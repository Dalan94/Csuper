Csuper
======
![Csuper logo](Images/Logo_200.png)


Csuper (Universal points counter allowing a dispense with reflection)

Informations
------------

This software allow you to count points easily with a computer.  
We can also save these games in a .csu file. (see documentation).

If you have any remarks or question on the use of csuper or on the code, don't hesitate to report to me.

You can find a lot of informations on [my website](https://www.dalan.fr).

Documentation
-------------

The documentation is made by Doxygen.  
You can create the documentation using the script `utility.sh`.
Run it without argument to see the help. You need latex installed to create the PDF doc.  
A file explain the .csu files.

Installation
------------

### Executables

Executables are available on [my website](https://www.dalan.fr).

### Compilation

To compile csuper you need a C++14 compliant compiler and the following libraries:

 * libxml++ ≥ 2.6
 * glibmm
 * giomm
 * muparser
 * PoDoFo
 * cairo
 * gettext
 * cmake
 * ncurses (for the CLI Unix version)
 * gtkmm3 ≥ 3.18 (for the GTK version)

If you are under Windows, you can use [MSYS2](https://msys2.github.io/) to get all the libraries.  
Then use:

```
git clone --recursive https://git.framasoft.org/Dalan94/Csuper.git
cd Csuper
cmake .
make
```

The followings arguments can be use with cmake:


```
-DLIB=FALSE				# To not compile libcsuper
-DCLI=FALSE				# To not compile csuper-cli
-DGTK=FALSE				# To not compile csuper-gtk
-DPORTABLE=TRUE			# To compile csuper as portable app
-DLIBXMLPP=3.0			# To compile with the 3.0 version of libxml++
-DALL_UPDATES=FALSE		# To not launch the update script which need pandoc, clang-format and gettext utilities
```

There are codeblocks or codelite project to managed the sources if you want but please use cmake first to configure the sources.  
If you are under Linux you can run the softwares with the scripts `run-csuper-cli.sh` and `run-csuper-gtk.sh`. On Windows just use the csuper-cli.exe and csuper-gtk.exe  
The software can also be run on a web browser through broadway using the `run-csuper-html.sh` and `csuper-html.bat` script.  

You can use the `utility.sh` script to:

* Updates the man pages
* Update and compile the translation
* Format the code
* Generate the documentation
* Install and uninstall

Launch it without arguments to see how to use it.


The compilation should work under Linux, Mac et Windows.

Additional Information
----------------------

If you have any remark, do not hesitate to open an issue.