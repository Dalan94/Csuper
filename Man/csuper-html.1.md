% CSUPER-HTML(1) Csuper-html User Manuals
% Rémi BERTHO
% November 5, 2017

# NAME

csuper-html - Universal points counter allowing a dispense with reflection

# SYNOPSIS

csuper-gtk [*input-file*]

# DESCRIPTION

This software allow you to count points easily with a computer. It is a GTK GUI of csuper opened with broadway.

The software can only open csu file.

# LOG

You can set the log level by setting to environnment variable CSUPER_LOG_LEVEL to WARNING (default), LOG or DEBUG.

# SEE ALSO

`csuper-cli` (1)
`csuper-gtk` (1)

The source code of in available on <https://git.framasoft.org/Dalan94/Csuper>.

The executable in available on <https://www.dalan.fr>