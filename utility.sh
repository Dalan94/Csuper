#!/bin/bash

print_help()
{
	echo "Csuper utility script"
	echo
	echo "Use:"
	echo "  bash utility.sh OPTION [PATH_TO_CSUPER_SRC_DIR]"
	echo
	echo "Update options:"
	echo "  -m, --man                  Update the man pages"
	echo "  -t, --translation          Update the translations"
	echo "  -c, --compile_translation  Compile the translations"
	echo "  -f, --format               Format the code"
	echo "  -a, --all                  Launch all update and compile function"
	echo
	echo "Documentation options:"
	echo "  -e, --generate_doc         Generate the documentation"
	echo "  -p, --pdf_doc              Compile the PDF documentation"
	echo "  -d, --doc                  Create the documentation"
	echo
	echo "Install options:"
	echo "  -l, --lib                  Install libcsuper"
	echo "  -o, --cli                  Install csuper-cli"
	echo "  -g, --gtk                  Install csuper-gtk"
	echo "  -i, --install              Install libcsuper, csuper-cli and csuper-gtk"
	echo
	echo "Uninstall options:"
	echo "  -ul, --unlib               Uninstall libcsuper"
	echo "  -uo, --uncli               Uninstall csuper-cli"
	echo "  -ug, --ungtk               Uninstall csuper-gtk"
	echo "  -u, --uninstall            Uinnstall libcsuper, csuper-cli and csuper-gtk"
}

update_man()
{
	echo "Update man pages"
	cd "Man" || return
	pandoc -s -t man csuper-cli.1.md -o csuper-cli.1
	gzip -n -f -9n csuper-cli.1
	pandoc -s -t man csuper-gtk.1.md -o csuper-gtk.1
	gzip -n -f -9n csuper-gtk.1
	pandoc -s -t man csuper-html.1.md -o csuper-html.1
	gzip -n -f -9n csuper-html.1
	cd .. || return
}

update_translation()
{
	echo "Update translations"

	xgettext --sort-by-file --add-comments=TRANSLATORS: --keyword=_ -o Translations/libcsuper/messages.pot Sources/libcsuper/*.cpp
	xgettext --sort-by-file --add-comments=TRANSLATORS: --keyword=_ -o Translations/csuper-cli/messages.pot Sources/csuper-cli/*.cpp
	xgettext --sort-by-file --add-comments=TRANSLATORS: --keyword=translatable --keyword=_ -o Translations/csuper-gtk/messages.pot Sources/csuper-gtk/*.cpp UI/csuper-gtk.glade

	for i in Translations/libcsuper/*.po
	do
		msgmerge -U $i "Translations/libcsuper/messages.pot"
	done

	for i in Translations/csuper-cli/*.po
	do
		msgmerge -U $i "Translations/csuper-cli/messages.pot"
	done

	for i in Translations/csuper-gtk/*.po
	do
		msgmerge -U $i "Translations/csuper-gtk/messages.pot"
	done
}

compile_translation()
{
	echo "Compile translations"
	mkdir -p Locales/fr_FR/LC_MESSAGES
	msgfmt "Translations/libcsuper/fr.po" -o "Locales/fr_FR/LC_MESSAGES/libcsuper.mo"
	msgfmt "Translations/csuper-cli/fr.po" -o "Locales/fr_FR/LC_MESSAGES/csuper-cli.mo"
	msgfmt "Translations/csuper-gtk/fr.po" -o "Locales/fr_FR/LC_MESSAGES/csuper-gtk.mo"
}

generate_documentation()
{
	echo "Generate documentation"
	cd "Documentation" || return
	
	doxygen libcsuper_doxyfile &> /dev/null
	doxygen csuper-cli_doxyfile &> /dev/null
	doxygen csuper-gtk_doxyfile &> /dev/null
	
	cd .. || return
}

compile_pdf_doc()
{
	echo "Compile PDF documentation"
	cd "Documentation/libcsuper/latex" || return
	make &> /dev/null
	cd ../../csuper-cli/latex || return
	make &> /dev/null
	cd ../../csuper-gtk/latex || return
	make &> /dev/null
	
	cd ../../.. || return
}

format_code()
{
	echo "Format code"
	clang-format -style=file Sources/libcsuper/*.cpp -i
	clang-format -style=file Sources/libcsuper/*.h -i
	clang-format -style=file Sources/csuper-cli/*.cpp -i
	clang-format -style=file Sources/csuper-cli/*.h -i
	clang-format -style=file Sources/csuper-gtk/*.cpp -i
	clang-format -style=file Sources/csuper-gtk/*.h -i
}

install_libcsuper()
{
	echo "Install libcsuper"
	sudo mkdir -p /usr/share/csuper/Locales/fr_FR/LC_MESSAGES
	sudo cp libcsuper.so /usr/lib
	sudo cp Locales/fr_FR/LC_MESSAGES/libcsuper.mo /usr/share/csuper/Locales/fr_FR/LC_MESSAGES/libcsuper.mo
	sudo mkdir -p /usr/include/libcsuper
	sudo cp Sources/libcsuper/*.h /usr/include/libcsuper
	sudo cp -R Images /usr/share/csuper
	sudo update-mime-database usr/share/mime &> /dev/null
	sudo cp Installation/Mime/csuper.xml /usr/share/mime/packages/
	sudo cp Installation/Mime/application-csu.png /usr/share/icons/hicolor/48x48/mimetypes
	sudo ldconfig 
}

install_csuper_cli()
{
	echo "Install csuper-cli"
	sudo mkdir -p /usr/share/csuper/Locales/fr_FR/LC_MESSAGES
	sudo cp csuper-cli /usr/share/csuper
	sudo cp -R Locales/fr_FR/LC_MESSAGES/csuper-cli.mo /usr/share/csuper/Locales/fr_FR/LC_MESSAGES
	sudo cp Installation/csuper-cli /usr/bin/csuper-cli
	sudo cp Man/csuper-cli.1.gz /usr/share/man/man1
	sudo chmod -R a+rx /usr/share/csuper
}

install_csuper_gtk()
{
	echo "Install csuper-gtk"
	sudo mkdir -p /usr/share/csuper/Locales/fr_FR/LC_MESSAGES
	sudo cp csuper-gtk /usr/share/csuper
	sudo cp -R UI /usr/share/csuper
	sudo cp -R Locales/fr_FR/LC_MESSAGES/csuper-gtk.mo /usr/share/csuper/Locales/fr_FR/LC_MESSAGES
	sudo cp Installation/csuper-gtk /usr/bin/csuper-gtk
	sudo cp Installation/csuper-html /usr/bin/csuper-html
	sudo cp Installation/csuper-html-autostart /usr/bin/csuper-html-autostart
	sudo cp Man/csuper-gtk.1.gz /usr/share/man/man1
	sudo cp Man/csuper-html.1.gz /usr/share/man/man1
	sudo cp Installation/fr.dalan.csuper-gtk.desktop /usr/share/applications/
	sudo cp Installation/fr.dalan.csuper-html.desktop /usr/share/applications/
	sudo cp Installation/csuper.service /etc/systemd/system/
	sudo chmod -R a+rx /usr/share/csuper
	[[ -x usr/bin/update-desktop-database ]] && update-desktop-database -q || true
}

uninstall_libcsuper()
{
	echo "Uninstall libcsuper"
	sudo rm -R /usr/include/libcsuper
	sudo rm /usr/lib/libcsuper.so
	sudo rm /usr/share/csuper/Locales/fr_FR/LC_MESSAGES/libcsuper.mo
	sudo rm -R /usr/share/csuper/Images
	sudo rm /usr/share/mime/packages/csuper.xml
	sudo rm /usr/share/icons/hicolor/48x48/mimetypes/application-csu.png
	sudo update-mime-database usr/share/mime &> /dev/null
	sudo ldconfig
}

uninstall_csuper_cli()
{
	echo "Uninstall csuper-cli"
	sudo rm /usr/bin/csuper-cli
	sudo rm /usr/share/man/man1/csuper-cli.1.gz
	sudo rm /usr/share/csuper/csuper-cli
	sudo rm /usr/share/csuper/Locales/fr_FR/LC_MESSAGES/csuper-cli.mo
}

uninstall_csuper_gtk()
{
	echo "Uninstall csuper-gtk"
	sudo rm /usr/share/csuper/csuper-gtk
	sudo rm -R /usr/share/csuper/UI
	sudo rm /usr/bin/csuper-gtk
	sudo rm /usr/bin/csuper-html
	sudo rm /usr/bin/csuper-html-autostart
	sudo rm /usr/share/man/man1/csuper-gtk.1.gz
	sudo rm /usr/share/man/man1/csuper-html.1.gz
	sudo rm /usr/share/csuper/Locales/fr_FR/LC_MESSAGES/csuper-gtk.mo
	sudo rm /usr/share/applications/fr.dalan.csuper-gtk.desktop
	sudo rm /usr/share/applications/fr.dalan.csuper-html.desktop
	sudo rm /etc/systemd/system/csuper.service 
}

cd "$(dirname "${BASH_SOURCE[0]}")" || exit

case "$1" in
	"-h" | "--help")
		print_help
		;;
	"-m" | "--man")
		update_man
		;;
	"-t" | "--translation")
		update_translation
		;;
	"-c" | "--compile_translation")
		compile_translation
		;;
	"-f" | "--format")
		format_code
		;;
	"-a" | "--all")
		update_man
		update_translation
		compile_translation
		format_code
		;;
	"-e" | "--generate_doc")
		generate_documentation
		;;
	"-p" | "--pdf_doc")
		compile_pdf_doc
		;;
	"-d" | "--doc")
		generate_documentation
		compile_pdf_doc
		;;
	"-l" | "--lib")
		install_libcsuper
		;;
	"-o" | "--cli")
		install_csuper_cli
		;;
	"-g" | "--gtk")
		install_csuper_gtk
		;;
	"-i" | "--install")
		install_libcsuper
		install_csuper_cli
		install_csuper_gtk
		;;
	"-ul" | "--unlib")
		uninstall_libcsuper
		;;
	"-uo" | "--uncli")
		uninstall_csuper_cli
		;;
	"-ug" | "--ungtk")
		uninstall_csuper_gtk
		;;
	"-u" | "--uninstall")
		uninstall_libcsuper
		uninstall_csuper_cli
		uninstall_csuper_gtk
		;;
	*)
		print_help
		;;
esac
