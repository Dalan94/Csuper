#Libcsuper

4.4.1

* Fixed migration from version 4.2.x
* Fixed infinite loop when using . as decimal point

4.4.0

* All 4.3.x changes

4.3.2

* Add save and export file asynchronous
* Improve XML
* Add export to markdown
* Add logs
* Upgrade slope

4.3.1

* Improve preferences
* Add libxml++3.0  compatibility

4.3.0

* Re-write in C++
* Add number of turn per distributor
* Add signals

4.2.0

* Add the possibility to choose which game configuration will be import/export
* New function changeDistributor 
* Add export to pdf and csv
* New function : totalPointsAtTurn and rankingAtTurn
* Add a calculator function
* Change the file format to XML
* Change all the preferences file into one XML file
* Change the game configuration files into one XML file
* Add export to gnuplot file
* Add export to octave/matlab file
* Add statisticals function

4.0.1

* Fix a bug on windows in the function getSimpleFilenameFromFullFilename
* New function checkFilename

4.0.0

* Add a function to transform integer to yes or no
* Add a function which calculate the index of the array from the position in the game
* The function changeSystemPath check if the new path is valid
* New function checkPath
* New function getFolderFromFilename
* New function differentsPlayerName
* Fix a bug with a game not in turn by turn
* New function getSimpleFilenameFromFullFilename
* New function copyCsuStruct
* New source file : preferences

2.4.0

* Add a initial turn (turn 0) with the begin score
* Add the possibility to open a file without the .csu extension
* Add the possibility to import and export game configurations


#Csuper-cli

4.4.0

* All 4.3.x changes

4.3.2

* Add save file asynchronous
* Add export to markdown
* Add logs

4.3.0

* Re-write in C++
* Add number of turn per distributor

4.2.0

* Add the possibility to choose which game configuration will be import/export
* You can now change the distributore during the game
* You export to pdf and csv
* Add a calculator function
* Add export to gnuplot file
* Add export to octave/matlab file

4.0.1

* Security update

4.0.0

* Change the display of a game configuration with yes or no instead of 1 and 0
* Correction of a bug which display the distributor when there is no distributor
* A few optimization

2.4.0

* Add a initial turn (turn 0) with the begin score
* Add the possibility to import and export game configurations


#Csuper-gtk

4.4.0

* All 4.3.x changes

4.3.2

* Add save and export file asynchronous
* Add export to markdown
* Add logs
* Merge statistics and charts windows
* Upgrade slope
* Improve HTML version

4.3.0

* Re-write in C++
* Change menu design
* Change calculator
* Add player names always on top
* Add broadway (HTML5) backend
* Add number of turn per distributor
* Edit game configuration of a game
* Import a game configuration from a game

4.2.0

* Add a delete file button
* Add the possibility to display only the image in the toolbar
* Add the possibility to choose which game configuration will be import/export
* When there is a new turn, the point window scroll at the bottom
* You can now change the distributore during the game
* Add shortcuts
* Add a podium at the end of the game
* Add the possibility to open recent file
* Add the possibilty to display difference between players
* Add export to pdf and csv
* Add the possibility to display the ranking and the total points in each turn
* Add a calculator function
* Add the possibility to display or not the elements on the left of the main window
* Add a delete and an edit turn button
* Add export to gnuplot file
* Add export to octave/matlab file
* Add a chart view
* Add statisticals view

4.0.2

* Add an error message when there is an error when loading the file with the main arguments

4.0.1

* Fixed the bug when the filename is too long
* We can use csuper-gui.exe with "open with" in windows explorer
* Fixed a bug with the distributor
* Update the translations
* Check the filename of the new file
* Open a error dialog when there is a error when saving the file

4.0.0

* Initial release