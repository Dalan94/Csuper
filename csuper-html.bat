@ECHO OFF
set port=52721
set GDK_BACKEND=broadway
for /f "delims=[] tokens=2" %%a in ('ping %computername% -n 1 -4 ^| findstr "["') do (set ip=%%a)

echo Csuper is available from a browser in your local network at the address http://%ip%:%port%/

start /b broadwayd --port=%port%
start http://%ip%:%port%/
csuper-gtk.exe
taskkill /F /IM broadwayd.exe
