#!/bin/bash
browser=`xdg-settings get default-web-browser | sed "s/.desktop//"`
ip=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')
port=52721

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)

echo ""Csuper is available from a browser in your local network at the address http://$ip:$port/""

broadwayd --port=$port &
nohup $browser http://$ip:$port/ &> /dev/null &
GDK_BACKEND=broadway ./csuper-gtk
killall broadwayd
