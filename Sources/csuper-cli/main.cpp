/*!
 * \file    main.cpp
 * \brief   Begin csuper
 * \author  Remi BERTHO
 * \date    25/05/15
 * \version 4.3.0
 */

/*
 * main.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-cli.
 *
 * Csuper-cli is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-cli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "share.h"

#include <clocale>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <map>
#include <glibmm/i18n.h>

#include "command_line_option.h"
#include "game_cli.h"
#include "menu.h"
#include "play_game.h"


using namespace csuper;
using namespace std;
using namespace Glib;

/*!
 *  Print an error message on error terminate
 */
void terminateFunction()
{
	g_error(_("Please report the bug to my git repository (https://git.framasoft.org/Dalan94/Csuper)."));
}

#ifdef GLIB_AVAILABLE_IN_2_50
GLogWriterOutput logWriterFunction(GLogLevelFlags log_level, const GLogField* fields, gsize n_fields, gpointer user_data)
#else
void logWriterFunction(const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data)
#endif
{
	map<ustring, ustring> str_fields;
	DateTime			  current_time = DateTime::create_now_local();
	ostringstream		  log_message;
	ofstream			  log_file;
	ustring				  log_filename;
	auto				  print_log_level = static_cast<CommandLineOption::LogLevel>(reinterpret_cast<uintptr_t>(user_data));

	// Check if log need to be written
	if (((print_log_level == CommandLineOption::WARNING) && (log_level > G_LOG_LEVEL_WARNING)) ||
			((print_log_level == CommandLineOption::INFO) && (log_level > G_LOG_LEVEL_INFO)))
	{
#ifdef GLIB_AVAILABLE_IN_2_50
		return G_LOG_WRITER_HANDLED;
#else
		return;
#endif
	}

	// Convert to map
#ifdef GLIB_AVAILABLE_IN_2_50
	for (guint i = 0; i < n_fields; i++)
		str_fields[fields[i].key] = (char*)fields[i].value;
#else
	str_fields["MESSAGE"]	  = message;
	str_fields["GLIB_DOMAIN"] = log_domain;
#endif

	// Print date
	log_message << "[" << current_time.format("%X.") << setw(3) << setfill('0') << current_time.get_microsecond() / 1000 << "] ";

	// Print severity
	switch (log_level)
	{
	case G_LOG_FLAG_RECURSION:
		log_message << "Emergency ";
		break;
	case G_LOG_FLAG_FATAL:
		log_message << "Fatal     ";
		break;
	case G_LOG_LEVEL_ERROR:
		log_message << "Error     ";
		break;
	case G_LOG_LEVEL_CRITICAL:
		log_message << "Critical  ";
		break;
	case G_LOG_LEVEL_WARNING:
		log_message << "Warning   ";
		break;
	case G_LOG_LEVEL_MESSAGE:
		log_message << "Message   ";
		break;
	case G_LOG_LEVEL_INFO:
		log_message << "Info      ";
		break;
	case G_LOG_LEVEL_DEBUG:
		log_message << "Debug     ";
		break;
	default:
		break;
	}

	// Print domain
	log_message << str_fields["GLIB_DOMAIN"];

	// Print file
	if (str_fields.count("CODE_FILE") == 1)
		log_message << " - " << ustring(path_get_basename(str_fields["CODE_FILE"])) << ":" << str_fields["CODE_LINE"];

	// Print function
	if (str_fields.count("CODE_FUNC") == 1)
		log_message << " - " << str_fields["CODE_FUNC"] << " ";

	// Print message
	log_message << ": " << removeCharacterInUstring(str_fields["MESSAGE"], '\n');


	// Print the message to the stream
	if (log_level <= G_LOG_LEVEL_ERROR)
		cerr << log_message.str() << endl;

// Create file
#ifdef PORTABLE
	log_filename = build_filename(get_current_dir(), "csuper-cli.log");
#else
	log_filename			  = build_filename(get_tmp_dir(), "csuper-cli.log");
#endif
	log_file.exceptions(ofstream::failbit | ofstream::badbit);
	try
	{
		log_file.open(locale_from_utf8(log_filename), ofstream::out | ofstream::app);
	}
	catch (ios_base::failure& e)
	{
		cerr << "Error when writting log file \"" << log_filename << "\"" << endl;
		if (log_level > G_LOG_LEVEL_ERROR)
			cerr << log_message.str() << endl;
	}
	log_file << log_message.str() << endl;
	log_file.close();

#ifdef GLIB_AVAILABLE_IN_2_50
	return G_LOG_WRITER_HANDLED;
#endif
}

/*!
 * \fn int main(int argc, char *argv[])
 *  Begin csuper.
 * \param[in] argc the number of argument.
 * \param[in] argv the array of argument.
 * \return 0 if everything is OK
 */
int main(int argc, char* argv[])
{
	// Set locals
	locale::global(locale(""));
	setlocale(LC_ALL, "");
	cout.imbue(locale(""));
	cerr.imbue(locale(""));
	cin.imbue(locale(""));
	bindtextdomain("csuper-cli", "Locales");
	bind_textdomain_codeset("csuper-cli", "UTF-8");
	textdomain("csuper-cli");
	Gio::init();

	// Set programme name
	set_prgname("csuper-cli");
	set_application_name(_("Csuper"));

	// Parse commande line
	CommandLineOption clo;
	clo.parse(argc, argv);

	// Set error and log functions
	set_terminate(terminateFunction);
#ifdef GLIB_AVAILABLE_IN_2_50
	g_log_set_writer_func(&logWriterFunction, reinterpret_cast<gpointer>(clo.getLogLevel()), NULL);
	g_log_structured(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "MESSAGE", "Csuper-cli start");
#else
	g_log_set_handler("libcsuper",
			(GLogLevelFlags)(G_LOG_LEVEL_DEBUG | G_LOG_LEVEL_INFO | G_LOG_LEVEL_WARNING | G_LOG_LEVEL_ERROR | G_LOG_FLAG_FATAL |
							 G_LOG_FLAG_RECURSION),
			&logWriterFunction,
			reinterpret_cast<gpointer>(clo.getLogLevel()));
	g_log_set_handler("csuper-cli",
			(GLogLevelFlags)(G_LOG_LEVEL_DEBUG | G_LOG_LEVEL_INFO | G_LOG_LEVEL_WARNING | G_LOG_LEVEL_ERROR | G_LOG_FLAG_FATAL |
							 G_LOG_FLAG_RECURSION),
			&logWriterFunction,
			reinterpret_cast<gpointer>(clo.getLogLevel()));
	g_debug("Csuper-cli start");
#endif

	// Initialization of csuper
	try
	{
#ifdef PORTABLE
		csuperInitialize(true);
#else
		csuperInitialize(false);
#endif	  // PORTABLE
	}
	catch (std::exception& e)
	{
		cout << e.what() << endl;
		cout << _("Error when initializing csuper") << endl;
		exit(EXIT_FAILURE);
	}
	catch (Glib::Exception& e)
	{
		cout << e.what() << endl;
		cout << _("Error when initializing csuper") << endl;
		exit(EXIT_FAILURE);
	}

	switch (clo.getInstruction())
	{
	case CommandLineOption::READ_FILE:
		try
		{
			GameCliPtr game = GameCli::create(clo.input());
			cout << *game;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::OPEN_FILE:
		try
		{
			GameCliPtr game = GameCli::create(clo.input());
			cout << *game << endl;
			systemPause();
			if (!(game->exceedMaxNumber()))
			{
				PlayGame play_game(game, clo.input());
				play_game.play();
			}
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::EXPORT_TO_PDF:
		try
		{
			PreferencesPtr pref = Preferences::get();
			GamePtr		   game = Game::create(clo.input());
			game->exportToPdf(clo.output(), pref->exportPdf(), pref->chartExportation());
			cout << ustring::compose(_("The file %1 was well exported to %2"), clo.input(), clo.output()) << endl;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::EXPORT_TO_PNG:
		try
		{
			PreferencesPtr pref = Preferences::get();
			GamePtr		   game = Game::create(clo.input());
			game->exportToPng(clo.output(), pref->chartExportation());
			cout << ustring::compose(_("The file %1 was well exported to %2"), clo.input(), clo.output()) << endl;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::EXPORT_TO_SVG:
		try
		{
			PreferencesPtr pref = Preferences::get();
			GamePtr		   game = Game::create(clo.input());
			game->exportToSvg(clo.output(), pref->chartExportation());
			cout << ustring::compose(_("The file %1 was well exported to %2"), clo.input(), clo.output()) << endl;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::EXPORT_TO_M:
		try
		{
			GamePtr game = Game::create(clo.input());
			game->exportToM(clo.output());
			cout << ustring::compose(_("The file %1 was well exported to %2"), clo.input(), clo.output()) << endl;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::EXPORT_TO_CSV:
		try
		{
			GamePtr game = Game::create(clo.input());
			game->exportToCsv(clo.output());
			cout << ustring::compose(_("The file %1 was well exported to %2"), clo.input(), clo.output()) << endl;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::EXPORT_TO_MD:
		try
		{
			GamePtr game = Game::create(clo.input());
			game->exportToMarkdown(clo.output());
			cout << ustring::compose(_("The file %1 was well exported to %2"), clo.input(), clo.output()) << endl;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::EXPORT_TO_GNUPLOT:
		try
		{
			GamePtr game = Game::create(clo.input());
			game->exportToGnuplot(clo.output());
			cout << ustring::compose(_("The file %1 was well exported to %2"), clo.input(), clo.output()) << endl;
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;

	case CommandLineOption::RUN:
		try
		{
			Menu menu;
			menu.main();
		}
		catch (std::exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		catch (Glib::Exception& e)
		{
			cout << e.what() << endl;
			exit(EXIT_FAILURE);
		}
		break;
	}

#ifdef GLIB_AVAILABLE_IN_2_50
	g_log_structured(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "MESSAGE", "Csuper-cli end");
#else
	g_debug("Csuper-cli end");
#endif

	return EXIT_SUCCESS;
}
