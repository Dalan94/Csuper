/*!
 * \file    command_line_option.cpp
 * \brief   Begin csuper
 * \author  Remi BERTHO
 * \date    14/06/15
 * \version 4.3.0
 */

/*
 * command_line_option.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-cli.
 *
 * Csuper-cli is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-cli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "share.h"

#include <glibmm/i18n.h>
#include "command_line_option.h"
#include <iostream>

using namespace Glib;
using namespace std;

CommandLineOption::CommandLineOption()
		: context_(""), filename_group_("filename", _("The input and output filename")),
		  instruction_group_("instruction", _("The instruction")), optional_group_("optional", _("Optional arguments"))
{
	open_entry_.set_long_name("open");
	open_entry_.set_short_name('e');
	open_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	open_entry_.set_description(_("Open the input file"));

	read_entry_.set_long_name("read");
	read_entry_.set_short_name('r');
	read_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	read_entry_.set_description(_("Read the input file"));

	pdf_entry_.set_long_name("to-pdf");
	pdf_entry_.set_short_name('p');
	pdf_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	pdf_entry_.set_description(_("Export the input file into into a PDF output file"));

	csv_entry_.set_long_name("to-csv");
	csv_entry_.set_short_name('c');
	csv_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	csv_entry_.set_description(_("Export the input file into into a CSV output file"));

	md_entry_.set_long_name("to-md");
	md_entry_.set_short_name('d');
	md_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	md_entry_.set_description(_("Export the input file into into a Markdown output file"));

	m_entry_.set_long_name("to-m");
	m_entry_.set_short_name('m');
	m_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	m_entry_.set_description(_("Export the input file into into a m (Octave/Matlab) output file"));

	gnuplot_entry_.set_long_name("to-gnuplot");
	gnuplot_entry_.set_short_name('g');
	gnuplot_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	gnuplot_entry_.set_description(_("Export the input file into into Gnuplot output files"));

	png_entry_.set_long_name("to-png");
	png_entry_.set_short_name('n');
	png_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	png_entry_.set_description(_("Export the input file into into a PNG output file"));

	svg_entry_.set_long_name("to-svg");
	svg_entry_.set_short_name('s');
	svg_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	svg_entry_.set_description(_("Export the input file into into a SVG output file"));

	instruction_group_.add_entry(open_entry_, open_);
	instruction_group_.add_entry(read_entry_, read_);
	instruction_group_.add_entry(pdf_entry_, pdf_);
	instruction_group_.add_entry(csv_entry_, csv_);
	instruction_group_.add_entry(md_entry_, md_);
	instruction_group_.add_entry(m_entry_, m_);
	instruction_group_.add_entry(gnuplot_entry_, gnuplot_);
	instruction_group_.add_entry(png_entry_, png_);
	instruction_group_.add_entry(svg_entry_, svg_);

	context_.add_group(instruction_group_);


	input_entry_.set_long_name("input");
	input_entry_.set_short_name('i');
	input_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	input_entry_.set_description(_("The input filename"));
	input_entry_.set_arg_description(_("filename"));

	output_entry_.set_long_name("output");
	output_entry_.set_short_name('o');
	output_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	output_entry_.set_description(_("The output filename"));
	output_entry_.set_arg_description(_("filename"));

	filename_group_.add_entry_filename(input_entry_, input_);
	filename_group_.add_entry_filename(output_entry_, output_);

	context_.add_group(filename_group_);


	log_entry_.set_long_name("log");
	log_entry_.set_short_name('l');
	log_entry_.set_flags(OptionEntry::FLAG_IN_MAIN);
	log_entry_.set_description(_("Log level"));
	log_entry_.set_arg_description(_("\"warning\" (default), \"info\", \"debug\""));

	optional_group_.add_entry(log_entry_, log_);

	context_.add_group(optional_group_);


	context_.set_help_enabled(true);
	context_.set_summary(_("If an instruction is set, a input filename is needed.\n"
						   "If a \"to\" instruction is set, an output filename is needed.\n"
						   "You can use only one instruction at once."));
}


void CommandLineOption::parse(int& argc, char**& argv)
{
	ustring error_msg;

	try
	{
		context_.parse(argc, argv);
	}
	catch (Glib::OptionError& e)
	{
		cout << e.what() << endl;
		cout << _("Use -h or --help for help.") << endl;
		exit(EXIT_FAILURE);
	}

	if (!open_ && !read_ && !pdf_ && !csv_ && !m_ && !gnuplot_ && !png_ && !svg_ && !md_)
	{
		if (!(input_.empty()) || !(output_.empty()))
			error_msg += _("You need an instruction.\n");
		else
			ins_ = RUN;
	}
	else
	{
		if (!singleInstruction())
			error_msg += _("You have to use only one instruction.\n");

		if (open_ || read_)
		{
			if (input_.empty())
				error_msg += _("You need an input file to open or read a file.\n");
			else
			{
				if (open_)
					ins_ = OPEN_FILE;
				else
					ins_ = READ_FILE;
			}

			if (!(output_.empty()))
				error_msg += _("You don't need an output file to open or read a file.\n");
		}
		else
		{
			if (input_.empty())
				error_msg += _("You need an input file to export a file.\n");
			if (output_.empty())
				error_msg += _("You need an output file to export a file.\n");

			if (!(output_.empty()) && !(input_.empty()))
			{
				if (pdf_)
					ins_ = EXPORT_TO_PDF;
				else if (csv_)
					ins_ = EXPORT_TO_CSV;
				else if (md_)
					ins_ = EXPORT_TO_MD;
				else if (gnuplot_)
					ins_ = EXPORT_TO_GNUPLOT;
				else if (png_)
					ins_ = EXPORT_TO_PNG;
				else if (svg_)
					ins_ = EXPORT_TO_SVG;
				else
					ins_ = EXPORT_TO_M;
			}
		}
	}


	if (!(error_msg.empty()))
	{
		cout << error_msg;
		cout << _("Use -h or --help for help.") << endl;
		exit(EXIT_FAILURE);
	}
}

bool CommandLineOption::singleInstruction() const
{
	return ((open_ + read_ + pdf_ + csv_ + md_ + m_ + png_ + svg_ + gnuplot_) == 1);
}
