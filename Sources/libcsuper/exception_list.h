/*!
 * \file    exception_list.h
 * \brief   Thread safe exception list
 * \author  Remi BERTHO
 * \date    28/08/16
 * \version 4.3.2
 */

/*
 * exception_list.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef EXCEPTION_LIST_H_INCLUDED
#define EXCEPTION_LIST_H_INCLUDED

#include <list>
#include <glibmm.h>
#include <atomic>
#include "exceptions.h"

namespace csuper
{

	/**
	 * @class ExceptionList
	 * @brief An thread safe Exception list. It can only be instantiate in the main thread.
	 */
	class ExceptionList
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		std::list<ExceptionPtr>	 list_;					   /** The list of exception */
		mutable std::atomic_flag lock_ = ATOMIC_FLAG_INIT; /** The lock */


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		Glib::Dispatcher signal_added_; /*!< The signal when an exception is added */


	public:
		/*!
		 *  \brief Connect to the added signal. It must be connected only once.
		 *  \param slot the slot function
		 *  \return the connection
		 */
		inline sigc::connection connectSignalAdded(const sigc::slot<void>& slot)
		{
			return signal_added_.connect(slot);
		}

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/**
		 * @brief Default constructor
		 */
		ExceptionList();

		/**
		 * @brief Destructor
		 */
		virtual ~ExceptionList();

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/**
		 * @brief Lock the list
		 */
		void lock() const;

		/**
		 * @brief Unlock the list
		 */
		void unlock() const;

	public:
		/**
		 * @brief Check if the list is empty
		 * @return true if the lest is empty, false otherwise
		 */
		bool empty() const;

		/**
		 * @brief Add an exception in the list
		 * @param exception the new exception
		 */
		void add(const csuper::Exception& exception);

		/**
		 * @brief Get the oldest exception in the list
		 * @return the exception
		 */
		ExceptionPtr get();
	};
}	 // namespace csuper

#endif	  // EXCEPTION_LIST_H_INCLUDED
