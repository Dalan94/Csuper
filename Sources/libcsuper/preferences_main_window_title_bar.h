/*!
 * \file    preferences_main_window_title_bar.h
 * \author  Remi BERTHO
 * \date    11/02/16
 * \version 4.3.1
 */

/*
* preferences_main_window_title_bar.h
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef PREFERENCES_MAIN_WINDOW_TITLE_BAR_H_INCLUDED
#define PREFERENCES_MAIN_WINDOW_TITLE_BAR_H_INCLUDED

#include "share.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class MainWindowTitleBarPreferences
	 *   \brief This class indicate where and how then title bar will be set
	 * This class contain:
	 *  - Ask the window manager to not decorated the main window
	 *  -  Print the file is the title bar ?
	 */
	class MainWindowTitleBarPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* DISABLE_WINDOW_MANAGER_DECORATION_LABEL = "disable_window_manager_decoration";
		static constexpr const char* PRINT_TITLE_LABEL						 = "print_title";

	public:
		static constexpr const char* LABEL = "main_window_title_bar";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		MainWindowTitleBarPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param disable_window_manager_decoration
		 *  \param print_title
		 */
		MainWindowTitleBarPreferences(const bool disable_window_manager_decoration, const bool print_title);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit MainWindowTitleBarPreferences(const xmlpp::Node* xml_node);



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the disable_window_manager_decoration
		 *  \param the disable_window_manager_decoration
		 */
		inline void setDisableWindowManagerDecoration(const bool disable_window_manager_decoration)
		{
			setBool(DISABLE_WINDOW_MANAGER_DECORATION_LABEL, disable_window_manager_decoration);
		}

		/*!
		 *  \brief Set the print_title
		 *  \param the print_title
		 */
		inline void setPrintTitle(const bool print_title)
		{
			setBool(PRINT_TITLE_LABEL, print_title);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the disable_window_manager_decoration
		 *  \return the disable_window_manager_decoration
		 */
		inline bool disableWindowManagerDecoration() const
		{
			return getAsBool(DISABLE_WINDOW_MANAGER_DECORATION_LABEL);
		}

		/*!
		 *  \brief Return the print_title
		 *  \return the print_title
		 */
		inline bool printTitle() const
		{
			return getAsBool(PRINT_TITLE_LABEL);
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the disable_window_manager_decoration in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring disableWindowManagerDecorationUstring() const
		{
			return boolToUstring(disableWindowManagerDecoration());
		}

		/*!
		 *  \brief Return the print_title in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring printTitleUstring() const
		{
			return boolToUstring(printTitle());
		}
	};
}	 // namespace csuper



#endif	  // PREFERENCES_MAIN_WINDOW_TITLE_BAR_H_INCLUDED
