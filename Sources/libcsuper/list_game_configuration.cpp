/*!
 * \file    list_game_configuration.cpp
 * \author  Remi BERTHO
 * \date    16/12/15
 * \version 4.3.0
 */

/*
* list_game_configuration.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#include "config.h"

#include "list_game_configuration.h"
#include "exceptions.h"
#include <iostream>

using namespace std;
using namespace Glib;
using namespace Gio;
using namespace xmlpp;

namespace csuper
{
	double ListGameConfiguration::version_(1.1);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	ListGameConfiguration::ListGameConfiguration()
	{
		connectSignal();
	}

	ListGameConfiguration::ListGameConfiguration(const ustring& filename)
	{
		DomParser	   parser;
		const Element* tmp_element;
		Node*		   root_node;

		try
		{
			parser.parse_file(filename);
		}
		catch (xmlpp::exception& e)
		{
			g_info("%s", e.what());
			throw XmlError(ustring::compose(_("Cannot open the file %1"), filename));
		}

		root_node = parser.get_document()->get_root_node();
		if (root_node->get_name() != "csu_game_configuration")
			throw XmlError(ustring::compose(_("This file is not a CSU game configuration file, it's a %1 file."), root_node->get_name()));


		// Version
		tmp_element			= getChildElement(root_node, VERSION_LABEL);
		double file_version = ustringToDouble(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());
		if (file_version > version_)
			throw XmlError(ustring::compose(
					_("This version of Csuper only support game configuration file version less than or equal to %1."), version_));
		double game_config_version;
		if (file_version == 1.0)
			game_config_version = 1.0;
		else
			game_config_version = 1.1;

		// Get all game configuration
		auto game_configuration_node_list = root_node->get_children(GameConfiguration::LABEL);
		for (auto& game_configuration_node : game_configuration_node_list)
			game_configuration_list_.push_back(new GameConfiguration(game_configuration_node, game_config_version));

		connectSignal();

		g_debug("List game configuration %s opened", filename.c_str());
	}

	ListGameConfiguration::ListGameConfiguration(const RefPtr<File>& file) : ListGameConfiguration(filename_to_utf8(file->get_path()))
	{
	}


	ListGameConfiguration::ListGameConfiguration(const ListGameConfiguration& list_game_config)
	{
		for (auto& it : list_game_config.game_configuration_list_)
			game_configuration_list_.push_back(new GameConfiguration(*it));

		connectSignal();

		g_debug("List game configuration copied");
	}


	ListGameConfiguration::~ListGameConfiguration()
	{
		for (auto& it : game_configuration_list_)
			delete it;

		game_configuration_list_.clear();

		g_debug("List game configuration destroyed");
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter and setter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	const GameConfiguration& ListGameConfiguration::operator[](unsigned int i) const
	{
		if (i >= size())
			throw OutOfRange(ustring::compose(_("Cannot access to the %1th element, there is only %2 elements."), i + 1, size()));
		return *game_configuration_list_[i];
	}

	GameConfiguration& ListGameConfiguration::operator[](unsigned int i)
	{
		if (i >= size())
			throw OutOfRange(ustring::compose(_("Cannot access to the %1th element, there is only %2 elements."), i + 1, size()));
		return *game_configuration_list_[i];
	}

	unsigned int ListGameConfiguration::size() const
	{
		return game_configuration_list_.size();
	}


	ListGameConfiguration& ListGameConfiguration::operator=(const ListGameConfiguration& list_game_config)
	{
		if (this == &list_game_config)
			return *this;


		for (auto& it : game_configuration_list_)
			delete it;
		game_configuration_list_.clear();


		for (auto& it : list_game_config.game_configuration_list_)
			game_configuration_list_.push_back(new GameConfiguration(*it));

		connectSignal();

		signal_changed_.emit();

		return *this;
	}


	void ListGameConfiguration::add(GameConfiguration* game_config)
	{
		for (auto& it : game_configuration_list_)
		{
			if (*game_config == *it)
				throw AlreadyExist(game_config->name());
		}
		game_configuration_list_.push_back(game_config);

		game_config->signalChanged().connect(mem_fun(signal_changed_, &type_signal_changed::emit));
		signal_changed_.emit();
	}

	void ListGameConfiguration::add(const GameConfiguration& game_config)
	{
		for (auto& it : game_configuration_list_)
		{
			if (game_config == *it)
				throw AlreadyExist(game_config.name());
		}
		GameConfiguration* new_game_config = new GameConfiguration(game_config);
		game_configuration_list_.push_back(new_game_config);

		new_game_config->signalChanged().connect(mem_fun(signal_changed_, &type_signal_changed::emit));
		signal_changed_.emit();
	}

	void ListGameConfiguration::add(const ListGameConfiguration& list_game_config)
	{
		for (auto& it : list_game_config.game_configuration_list_)
		{
			GameConfiguration* tmp_game_config = new GameConfiguration(*it);
			try
			{
				add(tmp_game_config);
			}
			catch (AlreadyExist& e)
			{
				g_info("%s", e.what().c_str());
				delete tmp_game_config;
			}
		}
	}

	void ListGameConfiguration::add(const ListGameConfiguration& list_game_config, const vector<unsigned int>& indexes)
	{
		unsigned int i = 0;

		for (auto& it : list_game_config.game_configuration_list_)
		{
			bool found = false;
			for (auto& it_index : indexes)
			{
				if (it_index == i)
				{
					found = true;
					break;
				}
			}

			if (found)
			{
				GameConfiguration* tmp_game_config = new GameConfiguration(*it);
				try
				{
					add(tmp_game_config);
				}
				catch (AlreadyExist& e)
				{
					g_info("%s", e.what().c_str());
					delete tmp_game_config;
				}
			}

			i++;
		}
	}


	void ListGameConfiguration::remove(const unsigned int i)
	{
		if (i >= size())
			throw OutOfRange(ustring::compose(_("Cannot remove the %1th element, there is only %2 elements"), i + 1, size()));

		delete game_configuration_list_[i];
		game_configuration_list_.erase(game_configuration_list_.begin() + i);
		signal_changed_.emit();
	}

	void ListGameConfiguration::remove(const GameConfiguration& game_config)
	{
		for (auto it = game_configuration_list_.begin(); it != game_configuration_list_.end(); it++)
		{
			if (**it == game_config)
			{
				delete *it;
				game_configuration_list_.erase(it);
				signal_changed_.emit();
				return;
			}
		}
		throw NotFound(
				ustring::compose(_("The game configuration %1 was not found in the list of game configuration"), game_config.name()));
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void ListGameConfiguration::connectSignal()
	{
		for (auto& it : game_configuration_list_)
			it->signalChanged().connect(mem_fun(signal_changed_, &type_signal_changed::emit));
	}


	ustring ListGameConfiguration::toUstring() const
	{
		ustring str("");

		for (auto& it : game_configuration_list_)
			str += (it->toUstring() + "\n\n");

		return str;
	}

	ustring ListGameConfiguration::toUstringName() const
	{
		ustring		 str("");
		unsigned int i = 1;

		for (auto it = game_configuration_list_.cbegin(); it != game_configuration_list_.cend(); it++, i++)
			str += ustring::compose(" (%1) %2\n", i, (*it)->nameUstring());

		return str;
	}

	ostream& operator<<(ostream& os, const ListGameConfiguration& list_game_config)
	{
		os << list_game_config.toUstring() << endl;
		return os;
	}


	void ListGameConfiguration::writeToFile(const ustring filename) const
	{
		Document doc;
		Element* root = doc.create_root_node(LABEL);

		// Version
		Element* node_version = root->LIBXMLXX_ADD_CHILD_ELEMENT(VERSION_LABEL);
		node_version->add_child_text(dtostr(version_));

		// Number of game configuration
		Element* node_nb = root->LIBXMLXX_ADD_CHILD_ELEMENT(VERSION_LABEL);
		node_nb->add_child_text(dtostr(size()));

		for (auto& it : game_configuration_list_)
			it->createXmlNode(root);

		try
		{
			doc.write_to_file_formatted(filename, "UTF-8");
		}
		catch (xmlpp::exception& e)
		{
			g_info("%s", e.what());
			throw FileError(ustring::compose(_("Error while writing the list of game configuration file %1"), filename));
		}

		g_debug("List game configuration written in %s", filename.c_str());
	}

	void ListGameConfiguration::writeToFile(const RefPtr<File> file) const
	{
		writeToFile(filename_to_utf8(file->get_path()));
	}

	void ListGameConfiguration::writeToFile(const ustring filename, const std::vector<unsigned int>& indexes) const
	{
		Document doc;
		Element* root = doc.create_root_node(LABEL);

		// Version
		Element* node_version = root->LIBXMLXX_ADD_CHILD_ELEMENT(VERSION_LABEL);
		node_version->add_child_text(dtostr(version_));

		// Number of game configuration
		Element* node_nb = root->LIBXMLXX_ADD_CHILD_ELEMENT(NB_GAME_CONFIG_LABEL);
		node_nb->add_child_text(dtostr(indexes.size()));

		unsigned int i = 0;
		for (auto& it : game_configuration_list_)
		{
			for (auto& it_index : indexes)
			{
				if (it_index == i)
				{
					it->createXmlNode(root);
					break;
				}
			}
			i++;
		}

		try
		{
			doc.write_to_file_formatted(filename, "UTF-8");
		}
		catch (xmlpp::exception& e)
		{
			g_info("%s", e.what());
			throw FileError(ustring::compose(_("Error while writing the list of game configuration file %1"), filename));
		}

		g_debug("List game configuration partially written in %s", filename.c_str());
	}


	void ListGameConfiguration::writeToFile(const RefPtr<File> file, const vector<unsigned int>& indexes) const
	{
		writeToFile(filename_to_utf8(file->get_path()), indexes);
	}

	void ListGameConfiguration::writeToFile() const
	{
		ustring filename;
		if (Portable::getPortable())
			filename = build_filename(DIRECTORY_NAME, GAME_CONFIGURATIONS_FILENAME);
		else
			filename = build_filename(locale_to_utf8(get_home_dir()), DIRECTORY_NAME, GAME_CONFIGURATIONS_FILENAME);

		writeToFile(filename);
	}

	ListGameConfigurationPtr ListGameConfiguration::getMainList()
	{
		string filename;
		if (Portable::getPortable())
			filename = build_filename(DIRECTORY_NAME, GAME_CONFIGURATIONS_FILENAME);
		else
			filename = build_filename(get_home_dir(), DIRECTORY_NAME, GAME_CONFIGURATIONS_FILENAME);

		return make_shared<ListGameConfiguration>(filename);
	}
}	 // namespace csuper
