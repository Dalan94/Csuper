/*!
 * \file    exceptions.h
 * \brief   Exceptions of Csuper
 * \author  Remi BERTHO
 * \date    10/02/14
 * \version 4.3.1
 */

/*
 * exceptions.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "exceptions.h"

using namespace std;
using namespace Glib;

namespace csuper
{
	Exception::Exception(const ustring msg) : Glib::Exception(), msg_(msg)
	{
	}

	ustring Exception::what() const
	{
		return msg_;
	}




	XmlError::XmlError(const ustring& msg) : ExceptionCRTP<XmlError>(_("XML Error: ") + msg)
	{
	}



	AlreadyExist::AlreadyExist(const ustring& msg) : ExceptionCRTP<AlreadyExist>(_("Already exist: ") + msg)
	{
	}



	NotFound::NotFound(const ustring& msg) : ExceptionCRTP<NotFound>(_("Not found: ") + msg)
	{
	}



	FileError::FileError(const ustring& msg) : ExceptionCRTP<FileError>(_("File error: ") + msg)
	{
	}



	PdfError::PdfError(const ustring& msg) : ExceptionCRTP<PdfError>(_("PDF error: ") + msg)
	{
	}



	WrongUse::WrongUse(const ustring& msg) : ExceptionCRTP<WrongUse>(_("Wrong use: ") + msg)
	{
	}



	CalculatorError::CalculatorError(const ustring& msg) : ExceptionCRTP<CalculatorError>(_("Calculator error: ") + msg)
	{
	}



	OutOfRange::OutOfRange(const ustring& msg) : ExceptionCRTP<OutOfRange>(_("Out of range: ") + msg)
	{
	}



	UndoRedoError::UndoRedoError(const ustring& msg) : ExceptionCRTP<UndoRedoError>(_("Undo redo error: ") + msg)
	{
	}



	NotAvailable::NotAvailable(const ustring& msg) : ExceptionCRTP<NotAvailable>(_("Not available: ") + msg)
	{
	}



	WrongValue::WrongValue(const ustring& msg) : ExceptionCRTP<WrongValue>(_("Wrong value: ") + msg)
	{
	}



	BadCast::BadCast(const ustring& msg) : ExceptionCRTP<BadCast>(_("Bad cast: ") + msg)
	{
	}



	InternetError::InternetError(const ustring& msg) : ExceptionCRTP<InternetError>(_("Internet error: ") + msg)
	{
	}
}	 // namespace csuper
