/*!
 * \file    preferences_difference_between_player.cpp
 * \author  Remi BERTHO
 * \date    13/02/16
 * \version 4.3.1
 */

/*
 * preferences_difference_between_player.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "config.h"

#include "preferences_difference_between_player.h"

using namespace Glib;
using namespace std;
using namespace xmlpp;

namespace csuper
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	DifferenceBetweenPlayerPreferences::DifferenceBetweenPlayerPreferences() : DifferenceBetweenPlayerPreferences(false, false, false)
	{
	}

	DifferenceBetweenPlayerPreferences::DifferenceBetweenPlayerPreferences(const bool consecutive, const bool first, const bool last)
			: AbstractPreference(LABEL,
					  AbstractPreference::BOOL,
					  CONSECUTIVE_LABEL,
					  AbstractPreference::BOOL,
					  FIRST_LABEL,
					  AbstractPreference::BOOL,
					  LAST_LABEL)
	{
		setConsecutive(consecutive);
		setFirst(first);
		setLast(last);
	}

	DifferenceBetweenPlayerPreferences::DifferenceBetweenPlayerPreferences(const Node* xml_node)
			: AbstractPreference(xml_node,
					  AbstractPreference::BOOL,
					  CONSECUTIVE_LABEL,
					  AbstractPreference::BOOL,
					  FIRST_LABEL,
					  AbstractPreference::BOOL,
					  LAST_LABEL)
	{
	}
}	 // namespace csuper
