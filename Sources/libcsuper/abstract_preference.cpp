/*!
 * \file    abstract_preference.h
 * \author  Remi BERTHO
 * \date    14/02/16
 * \version 4.3.1
 */

/*
 * abstract_preference.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <iostream>

#include "config.h"

#include "abstract_preference.h"
#include "exceptions.h"

using namespace Glib;
using namespace std;
using namespace xmlpp;

namespace csuper
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	AbstractPreference::AbstractPreference(ustring name,
			const Type							   type1,
			const ustring						   key1,
			const Type							   type2,
			const ustring						   key2,
			const Type							   type3,
			const ustring						   key3,
			const Type							   type4,
			const ustring						   key4,
			const Type							   type5,
			const ustring						   key5,
			const Type							   type6,
			const ustring						   key6,
			const Type							   type7,
			const ustring						   key7,
			const Type							   type8,
			const ustring						   key8,
			const Type							   type9,
			const ustring						   key9,
			const Type							   type10,
			const ustring						   key10)
			: name_(name)
	{
		for (int i = 1; i <= 10; i++)
		{
			Type	tmp_type;
			ustring tmp_key;

			switch (i)
			{
			case 1:
				tmp_type = type1;
				tmp_key	 = key1;
				break;
			case 2:
				tmp_type = type2;
				tmp_key	 = key2;
				break;
			case 3:
				tmp_type = type3;
				tmp_key	 = key3;
				break;
			case 4:
				tmp_type = type4;
				tmp_key	 = key4;
				break;
			case 5:
				tmp_type = type5;
				tmp_key	 = key5;
				break;
			case 6:
				tmp_type = type6;
				tmp_key	 = key6;
				break;
			case 7:
				tmp_type = type7;
				tmp_key	 = key7;
				break;
			case 8:
				tmp_type = type8;
				tmp_key	 = key8;
				break;
			case 9:
				tmp_type = type9;
				tmp_key	 = key9;
				break;
			case 10:
				tmp_type = type10;
				tmp_key	 = key10;
				break;
			default:
				break;
			}

			switch (tmp_type)
			{
			case Type::NONE:
				break;
			case Type::INT64:
				pref_[tmp_key] = Variant<gint64>::create(0);
				break;
			case Type::UINT64:
				pref_[tmp_key] = Variant<guint64>::create(0);
				break;
			case Type::INT32:
				pref_[tmp_key] = Variant<gint32>::create(0);
				break;
			case Type::UINT32:
				pref_[tmp_key] = Variant<guint32>::create(0);
				break;
			case Type::DOUBLE:
				pref_[tmp_key] = Variant<double>::create(0);
				break;
			case Type::USTRING:
				pref_[tmp_key] = Variant<ustring>::create("");
				break;
			case Type::BOOL:
				pref_[tmp_key] = Variant<bool>::create(false);
				break;
			default:
				break;
			}
		}
	}


	AbstractPreference::AbstractPreference(const Node* xml_node,
			const Type								   type1,
			const ustring							   key1,
			const Type								   type2,
			const ustring							   key2,
			const Type								   type3,
			const ustring							   key3,
			const Type								   type4,
			const ustring							   key4,
			const Type								   type5,
			const ustring							   key5,
			const Type								   type6,
			const ustring							   key6,
			const Type								   type7,
			const ustring							   key7,
			const Type								   type8,
			const ustring							   key8,
			const Type								   type9,
			const ustring							   key9,
			const Type								   type10,
			const ustring							   key10)
			: AbstractPreference("",
					  type1,
					  key1,
					  type2,
					  key2,
					  type3,
					  key3,
					  type4,
					  key4,
					  type5,
					  key5,
					  type6,
					  key6,
					  type7,
					  key7,
					  type8,
					  key8,
					  type9,
					  key9,
					  type10,
					  key10)
	{
		name_ = xml_node->get_name();
		for (auto& p : pref_)
		{
			try
			{
				ustring		   string_value;
				const Element* tmp_element = getChildElement(xml_node, p.first);
				if (tmp_element->has_child_text())
					string_value = tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content();
				VariantType type = p.second.get_type();
				VariantBase value;

				if (type.equal(Variant<bool>::variant_type()))
					value = Variant<bool>::create(ustringToBool(string_value));

				if (type.equal(Variant<gint64>::variant_type()))
					value = Variant<gint64>::create(ustringToInt(string_value));

				if (type.equal(Variant<gint32>::variant_type()))
					value = Variant<gint32>::create(ustringToInt(string_value));

				if (type.equal(Variant<guint64>::variant_type()))
					value = Variant<guint64>::create(ustringToInt(string_value));

				if (type.equal(Variant<guint32>::variant_type()))
					value = Variant<guint32>::create(ustringToInt(string_value));

				if (type.equal(Variant<double>::variant_type()))
					value = Variant<double>::create(ustringToDouble(string_value));

				if (type.equal(Variant<ustring>::variant_type()))
					value = Variant<ustring>::create(string_value);

				p.second = value;
			}
			catch (XmlError& e)
			{
				g_warning("%s", e.what().c_str());
			}
		}
	}

	AbstractPreference::AbstractPreference(const AbstractPreference& ap) : pref_(ap.pref_), name_(ap.name_)
	{
	}


	AbstractPreference::~AbstractPreference()
	{
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool AbstractPreference::operator==(const AbstractPreference& ap) const
	{
		if (name_ != ap.name_)
			return false;

		if (ap.pref_.size() != pref_.size())
			return false;

		auto it1 = pref_.cbegin();
		auto it2 = ap.pref_.cbegin();
		for (; it1 != pref_.cend(); it1++, it2++)
		{
			if (it1->first != it2->first || (!it1->second.equal(it2->second)))
				return false;
		}
		return true;
	}

	AbstractPreference& AbstractPreference::operator=(const AbstractPreference& ap)
	{
		pref_ = ap.pref_;
		name_ = ap.name_;

		signal_changed_.emit();
		return *this;
	}

	bool AbstractPreference::operator!=(const AbstractPreference& ap) const
	{
		return !(*this == ap);
	}

	ustring AbstractPreference::toUstring() const
	{
		ustring res = ustring::compose(_("%1:\n"), name_);

		for (auto& p : pref_)
			res += ustring::compose(_(" - %1: %2\n"), p.first, p.second.print());

		return res;
	}

	ostream& operator<<(ostream& os, const AbstractPreference& ap)
	{
		os << ap.toUstring();
		return os;
	}

	void AbstractPreference::createXmlNode(Element* parent_node) const
	{
		Element* node = parent_node->LIBXMLXX_ADD_CHILD_ELEMENT(name_);
		for (auto& p : pref_)
		{
			Element* tmp_node = node->LIBXMLXX_ADD_CHILD_ELEMENT(p.first);

			VariantType type = p.second.get_type();
			ustring		value;


			if (type.equal(Variant<bool>::variant_type()))
				value = boolToUstring(variantToBool(p.second));

			if (type.equal(Variant<gint64>::variant_type()))
				value = csuper::dtostr(variantToInt64(p.second));

			if (type.equal(Variant<gint32>::variant_type()))
				value = csuper::dtostr(variantToInt32(p.second));

			if (type.equal(Variant<guint64>::variant_type()))
				value = csuper::dtostr(variantToUint64(p.second));

			if (type.equal(Variant<guint32>::variant_type()))
				value = csuper::dtostr(variantToUint32(p.second));

			if (type.equal(Variant<double>::variant_type()))
				value = csuper::dtostr(variantToDouble(p.second));

			if (type.equal(Variant<ustring>::variant_type()))
				value = variantToUstring(p.second);


			tmp_node->add_child_text(value);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter and getter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void AbstractPreference::set(const ustring key, const VariantBase value)
	{
		auto p = pref_.find(key);
		if (p == pref_.end())
			throw NotFound(ustring::compose(_("Cannot found the key %1 in %2."), key, name_));
		if (!value.get_type().equal(p->second.get_type()))
			throw WrongValue(ustring::compose(
					_("The key %1 in %2 has a type of %3, not %4."), key, name_, p->second.get_type_string(), value.get_type_string()));

		p->second = value;

		signal_changed_.emit();
	}

	VariantBase AbstractPreference::get(const ustring key) const
	{
		auto p = pref_.find(key);
		if (p == pref_.end())
			throw NotFound(ustring::compose(_("Cannot found the key %1 in %2."), key, name_));

		return p->second;
	}

	ustring AbstractPreference::getUstring(const ustring key) const
	{
		VariantBase var	 = get(key);
		VariantType type = var.get_type();
		ustring		value;

		if (type.equal(Variant<bool>::variant_type()))
			value = boolToUstring(variantToBool(var));

		if (type.equal(Variant<gint64>::variant_type()))
			value = intToUstring(variantToInt64(var));

		if (type.equal(Variant<gint32>::variant_type()))
			value = intToUstring(variantToInt32(var));

		if (type.equal(Variant<guint64>::variant_type()))
			value = intToUstring(variantToUint64(var));

		if (type.equal(Variant<guint32>::variant_type()))
			value = intToUstring(variantToUint32(var));

		if (type.equal(Variant<double>::variant_type()))
			value = doubleToUstring(variantToDouble(var));

		if (type.equal(Variant<ustring>::variant_type()))
			value = variantToUstring(var);

		return value;
	}
}	 // namespace csuper
