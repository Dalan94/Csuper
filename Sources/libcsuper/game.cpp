/*!
 * \file    game.cpp
 * \author  Remi BERTHO
 * \date    30/08/16
 * \version 4.3.2
 */

/*
* game.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#include "config.h"

#include "game.h"
#include "exceptions.h"
#include <algorithm>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <slope.h>
#include "pdf_exportation.h"



namespace csuper
{
	using namespace std;
	using namespace Glib;
	using namespace xmlpp;
	using namespace Gio;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Game::Game(const unsigned int nb_player, const GameConfiguration& game_config)
			: SpinlockLockable(), enable_shared_from_this<Game>(), nb_player_(nb_player)
	{
		date_.set_time_current();
		config_ = new GameConfiguration(game_config);

		for (unsigned int i = 0; i < nb_player; i++)
			players_.push_back(new Player(game_config));

		connectSignal();

		g_debug("Game created");
	}


	Game::Game(const Game& game) : SpinlockLockable(), enable_shared_from_this<Game>()
	{
		size_max_name_		 = game.size_max_name_;
		date_				 = game.date_;
		nb_player_			 = game.nb_player_;
		distributor_		 = game.distributor_;
		config_				 = new GameConfiguration(*(game.config_));
		nb_turn_distributor_ = game.nb_turn_distributor_;

		for (auto& p : game.players_)
			players_.push_back(new Player(*p));

		connectSignal();


		g_debug("Game copied");
	}


	Game::Game(const Glib::ustring filename) : SpinlockLockable(), enable_shared_from_this<Game>(), date_(1, Date::JANUARY, 2016)
	{
		DomParser	   parser;
		Node*		   root_node;
		const Element *tmp_element, *tmp_element_2;

		try
		{
			parser.parse_file(filename);
		}
		catch (xmlpp::exception& e)
		{
			g_info("%s", e.what());
			throw XmlError(ustring::compose(_("Cannot open the file %1"), filename));
		}

		root_node = parser.get_document()->get_root_node();
		if (root_node->get_name() != "csu")
			throw XmlError(ustring::compose(_("This file is not a CSU file, it's a %1 file."), root_node->get_name()));


		// Version
		tmp_element			= getChildElement(root_node, VERSION_LABEL);
		double file_version = ustringToDouble(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());
		if (file_version >= MAX_VERSION)
			throw XmlError(ustring::compose(
					_("This version of Csuper only support game file version less than %1"), doubleToUstring(MAX_VERSION, 1)));
		double game_config_version;
		if (file_version <= 1.4)
			game_config_version = 1.0;
		else
			game_config_version = 1.1;

		// Size max of a name
		tmp_element	   = getChildElement(root_node, SIZE_MAX_NAME_LABEL);
		size_max_name_ = ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		// Date
		tmp_element	  = getChildElement(root_node, DATE_LABEL);
		tmp_element_2 = getChildElement(tmp_element, YEAR_LABEL);
		date_.set_year(ustringToInt(tmp_element_2->LIBXMLXX_GET_CHILD_TEXT()->get_content()));
		tmp_element_2 = getChildElement(tmp_element, MONTH_LABEL);
		date_.add_months(ustringToInt(tmp_element_2->LIBXMLXX_GET_CHILD_TEXT()->get_content()) - 1);
		tmp_element_2 = getChildElement(tmp_element, DAY_LABEL);
		date_.set_day(ustringToInt(tmp_element_2->LIBXMLXX_GET_CHILD_TEXT()->get_content()));

		// Nb player
		tmp_element = getChildElement(root_node, NB_PLAYER_LABEL);
		nb_player_	= ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		// Distributor
		tmp_element	 = getChildElement(root_node, DISTRIBUTOR_LABEL);
		distributor_ = ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		if (file_version >= 1.5)
		{
			tmp_element			 = getChildElement(root_node, NB_TURN_DISTRIBUTOR_LABEL);
			nb_turn_distributor_ = ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());
		}
		else
			nb_turn_distributor_ = 1;

		// Game configuration
		tmp_element = getChildElement(root_node, GameConfiguration::LABEL);
		config_		= new GameConfiguration(tmp_element, game_config_version);

		// Players
		auto player_node_list = root_node->get_children(Player::LABEL);
		for (auto& player_node : player_node_list)
			players_.push_back(new Player(player_node));

		connectSignal();

		g_debug("File %s opened", filename.c_str());
	}

	Game::Game(const RefPtr<File>& file) : Game(filename_to_utf8(file->get_path()))
	{
	}


	//
	// Destuctor
	//
	Game::~Game()
	{
		delete config_;

		for (auto& it : players_)
			delete it;

		players_.clear();

		g_debug("Game destroyed");
	}


	//
	// Creator
	//
	GamePtr Game::create(const unsigned int nb_player, const GameConfiguration& game_config)
	{
		return shared_ptr<Game>(new Game(nb_player, game_config));
	}

	GamePtr Game::clone() const
	{
		return shared_ptr<Game>(new Game(*this));
	}

	GamePtr Game::create(const Glib::ustring filename)
	{
		return shared_ptr<Game>(new Game(filename));
	}

	GamePtr Game::create(const Glib::RefPtr<Gio::File>& file)
	{
		return shared_ptr<Game>(new Game(file));
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Signals ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Game::connectSignal()
	{
		signalPointsChanged().connect(sigc::hide<>(sigc::mem_fun(*this, &Game::rankingCalculation)));
		signalPointsChanged().connect(sigc::mem_fun(*this, &Game::onPointsChange));

		config_->signalChanged().connect(sigc::mem_fun(*this, &Game::onGameConfigurationChange));
	}

	void Game::onGameConfigurationChange()
	{
		vector<double> points;
		for (unsigned int i = 0; i < nbPlayer(); i++)
			points.push_back(config().initialScore());
		editTurn(0, points);


		g_debug("Game configurations changed");
	}


	void Game::onPointsChange(PointsChangedType type)
	{
		switch (type)
		{
		case ADD_POINTS:
			g_debug("Game points added");
			break;
		case DELETE_POINTS:
			g_debug("Game points deleted");
			break;
		case EDIT_POINTS:
			g_debug("Game points edited");
			break;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Operator //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Game& Game::operator=(const Game& game)
	{
		if (this == &game)
			return *this;


		delete config_;

		for (auto& it : players_)
			delete it;

		players_.clear();


		size_max_name_		 = game.size_max_name_;
		date_				 = game.date_;
		nb_player_			 = game.nb_player_;
		distributor_		 = game.distributor_;
		nb_turn_distributor_ = game.nb_turn_distributor_;
		config_				 = new GameConfiguration(*(game.config_));

		for (auto& it : game.players_)
			players_.push_back(new Player(*it));

		connectSignal();
		signal_changed_.emit();

		return *this;
	}

	ostream& operator<<(ostream& os, const Game& game)
	{
		os << game.toUstring();
		return os;
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter and getter /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	unsigned int Game::ranking(const unsigned int player_index, const int turn) const
	{
		if (turn == -1)
			return player(player_index).ranking();

		if (!(config().turnBased()))
			throw WrongUse(_("The ranking function should only be used in a turn based game when a specific turn is specify"));

		if (!(player(player_index).hasTurn(turn)))
			throw OutOfRange(
					ustring::compose(_("Cannot access to the %1th turn, there is only %2 turn"), turn, player(player_index).nbTurn()));

		vector<double> sort_points;

		for (auto& it_player : players_)
			sort_points.push_back(it_player->totalPoints(turn));

		// Sort the points base on the first way
		if (config().maxWinner())
			sort(sort_points.begin(), sort_points.end(), &compareDoubleDescending);
		else
			sort(sort_points.begin(), sort_points.end(), &compareDoubleAscending);

		auto   it_sort		 = sort_points.begin();
		double points_player = totalPoints(player_index, turn);
		for (unsigned int i = 0; i < nbPlayer(); i++, it_sort++)
		{
			if (*it_sort == points_player)
				return i + 1;
		}
		return 0;
	}

	Player& Game::player(const unsigned int index)
	{
		if (index >= nbPlayer())
			throw OutOfRange(ustring::compose(_("Cannot access to the %1th player, there is only %2 player"), index + 1, nbPlayer()));
		return *players_[index];
	}


	const Player& Game::player(const unsigned int index) const
	{
		if (index >= nbPlayer())
			throw OutOfRange(ustring::compose(_("Cannot access to the %1th player, there is only %2 player"), index + 1, nbPlayer()));
		return *players_[index];
	}

	void Game::setDistributor(const unsigned int distributor)
	{
		if (distributor > nbPlayer())
			throw OutOfRange(
					ustring::compose(_("Cannot set the %1th player distributor, there is only %2 player"), distributor + 1, nbPlayer()));
		distributor_ = distributor;
		signal_changed_.emit();
		signal_distributor_changed_.emit();
	}

	void Game::setPlayerName(const unsigned int index, const Glib::ustring& name)
	{
		if (index > nbPlayer())
			throw OutOfRange(ustring::compose(_("Cannot set the %1th player name, there is only %2 player"), index + 1, nbPlayer()));
		players_[index]->setName(name);
		signal_changed_.emit();
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// To Ustring ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ustring Game::toUstring() const
	{
		ustring		 str;
		unsigned int line_size = 1;

		str = toUstringHeader();
		str += config_->toUstring() + "\n";
		str += toUstringDistributor() + "\n";
		str += toUstringNames(line_size, true);
		str += toUstringLine(line_size);
		str += toUstringAllPoints();
		str += toUstringLine(line_size);
		str += toUstringTotalPoints();
		str += toUstringLine(line_size);
		str += toUstringRanking();

		return str;
	}

	ustring Game::toUstringPoints() const
	{
		ustring		 str;
		unsigned int line_size = 1;

		str = toUstringNames(line_size, true);
		str += toUstringLine(line_size);
		str += toUstringAllPoints();
		str += toUstringLine(line_size);
		str += toUstringNames(line_size);
		str += toUstringLine(line_size);
		str += toUstringTotalPoints();
		str += toUstringLine(line_size);
		str += toUstringRanking();
		str += toUstringDistributor();

		return str;
	}

	ustring Game::toUstringProperties() const
	{
		ustring str;

		str = toUstringHeader();
		str += config_->toUstring() + "\n";
		str += toUstringDistributor();

		return str;
	}

	ustring Game::toUstringHeader() const
	{
		ustring str = _("Csu file\nCreated on the ") + dateUstring() + _("\nFile's version: ") + doubleToUstring(VERSION) +
					  _("\nMaximum size of the names: ") + sizeMaxNameUstring() + _("\nNumber of players: ") + nbPlayerUstring() +
					  _("\nMaximum number of turns: ") + intToUstring(maxNbTurn()) + "\n";
		return str;
	}

	ustring Game::toUstringDistributor() const
	{
		ustring str;
		if (config_->useDistributor())
		{
			str = distributorNameUstring() + _(" is the distributor") + _(" for his ") + intToUstring(nbTurnDistributor() + 1) +
				  _("th turn\n");
		}
		return str;
	}

	ustring Game::toUstringNames(unsigned int& line_size, const bool change_line_size) const
	{
		ustring str;
		int		i;

		// TRANSLATORS:The number of characters before the | must be eight
		str = _("Names   | ");

		for (auto& it : players_)
		{
			ustring name(it->name());
			str += name;

			for (i = name.size(); i < 4; i++)
			{
				str += " ";
				if (change_line_size)
					line_size += 1;
			}

			str += " | ";

			if (change_line_size)
				line_size += name.size() + 3;
		}


		return str + "\n";
	}

	ustring Game::toUstringLine(const unsigned int line_size) const
	{
		ustring str = "\t";

		for (unsigned i = 0; i < line_size; i++)
			str += "-";

		return str + "\n";
	}

	ustring Game::toUstringAllPoints() const
	{
		ustring		 str;
		unsigned int i, j;

		for (i = 0; i <= maxNbTurn(); i++)
		{
			// TRANSLATORS:The number of characters before the | and without the %1 must be six
			str += ustring::compose(_("Turn %1 |"), ustring::format(setw(2), i));

			for (auto& it : players_)
			{
				if (it->hasTurn(i))
					str += it->pointsUstring(*config_, i, 6);
				else
					str += "      ";

				for (j = 4; j < it->name().size(); j++)
					str += " ";

				str += "|";
			}
			str += "\n";
		}

		return str;
	}

	ustring Game::toUstringTotalPoints() const
	{
		ustring		 str;
		unsigned int i;


		// TRANSLATORS:The number of characters before the | must be eight
		str += _("Total   |");

		for (auto& it : players_)
		{
			str += it->totalPointsUstring(*config_, -1, 6);

			for (i = 4; i < it->name().size(); i++)
				str += " ";

			str += "|";
		}

		return str + "\n";
	}

	ustring Game::toUstringRanking() const
	{
		ustring		 str;
		unsigned int i;


		// TRANSLATORS:The number of characters before the | must be eight
		str += _("Ranking |");

		for (auto& it : players_)
		{
			str += it->rankingUstring(6);

			for (i = 4; i < it->name().size(); i++)
				str += " ";

			str += "|";
		}

		return str + "\n";
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	unsigned int Game::maxNbTurn() const
	{
		unsigned int max_turn = 0;

		for (auto& it : players_)
			max_turn = max(max_turn, it->nbTurn());

		return max_turn;
	}

	bool Game::canBeTurnBased() const
	{
		if (config().turnBased())
			return true;

		unsigned int max_nb_turn = maxNbTurn();
		for (auto& p : players_)
		{
			if (p->nbTurn() != max_nb_turn)
				return false;
		}
		return true;
	}

	unsigned int Game::getPlayerIndex(const ustring& player_name) const
	{
		for (unsigned int i = 0; i < nb_player_; i++)
		{
			if (players_[i]->name().lowercase().compare(0, player_name.lowercase().size(), player_name.lowercase()) == 0)
				return i;
		}
		throw NotFound(ustring::compose(_("Player %1 is not found in the game."), player_name));
	}

	unsigned int Game::getPlayerIndex(const Player& player) const
	{
		for (unsigned int i = 0; i < nb_player_; i++)
		{
			if (*players_[i] == player)
				return i;
		}
		throw NotFound(ustring::compose(_("Player %1 is not found in the game."), player.name()));
	}

	void Game::rankingCalculation()
	{
		vector<double> sort_points;

		for (auto& it_player : players_)
			sort_points.push_back(it_player->totalPoints());

		// Sort the points base on the first way
		if (config().maxWinner())
			sort(sort_points.begin(), sort_points.end(), &compareDoubleDescending);
		else
			sort(sort_points.begin(), sort_points.end(), &compareDoubleAscending);

		auto it_sort = sort_points.rbegin();
		// Loop on the sort points from the smallest
		for (int i = nbPlayer() - 1; i >= 0; i--, it_sort++)
		{
			// Loop on the total points
			for (auto& it_player : players_)
			{
				if (*it_sort == it_player->totalPoints())
					it_player->setRanking(i + 1);
			}
		}
	}

	bool Game::exceedMaxNumber() const
	{
		for (auto& it : players_)
		{
			if (config().useMaximum())
			{
				if (it->totalPoints() + DBL_EPSILON >= config().nbMaxMin())
				{
					signal_exceed_max_number_.emit();
					return true;
				}
			}
			else
			{
				if (it->totalPoints() - DBL_EPSILON <= config().nbMaxMin())
				{
					signal_exceed_max_number_.emit();
					return true;
				}
			}
		}
		return false;
	}


	bool Game::newTurn(const unsigned int player_index, const double points)
	{
		if (config().turnBased())
			throw WrongUse(_("This new turn function should only be used in a non turn based game"));

		player(player_index).addPoints(points);

		increaseDistributor();

		signal_points_changed_.emit(ADD_POINTS);
		signal_changed_.emit();

		return exceedMaxNumber();
	}


	bool Game::newTurn(const vector<double>& points)
	{
		if (!(config().turnBased()))
			throw WrongUse(_("The new turn function should only be used in a turn based game when a vector of points is given"));

		if (points.size() != nbPlayer())
			throw OutOfRange(ustring::compose(_("There is %1 points and %2 player in the game"), intToUstring(points.size()), nbPlayer()));

		auto it_points = points.cbegin();
		auto it_player = players_.begin();
		for (; it_points != points.cend(); it_player++, it_points++)
			(*it_player)->addPoints(*it_points);

		increaseDistributor();

		signal_points_changed_.emit(ADD_POINTS);
		signal_changed_.emit();

		return exceedMaxNumber();
	}


	bool Game::differentsPlayerNames() const
	{
		unsigned int i = 0;
		for (auto it1 = players_.cbegin(); it1 != players_.cend(); it1++, i++)
		{
			for (auto it2 = players_.cbegin() + i + 1; it2 != players_.cend(); it2++)
			{
				if ((*it1)->name() == (*it2)->name())
					return false;
			}
		}
		return true;
	}


	unsigned int Game::lastRanking(const unsigned int turn) const
	{
		if (!(config().turnBased()))
			throw WrongUse(_("The last ranking function should only be used in a turn based game when a specific turn is specified"));

		if (!(player(0).hasTurn(turn)))
			throw OutOfRange(ustring::compose(_("Cannot access to the %1th turn, there is only %2 turn"), turn, player(0).nbTurn()));


		vector<double> sort_points;
		auto		   it_player = players_.cbegin();

		for (; it_player != players_.cend(); it_player++)
			sort_points.push_back((*it_player)->totalPoints(turn));


		// Sort the points base on the first way
		if (config().maxWinner())
			sort(sort_points.begin(), sort_points.end(), &compareDoubleDescending);
		else
			sort(sort_points.begin(), sort_points.end(), &compareDoubleAscending);


		auto						   it_sort = sort_points.rbegin();
		vector<unsigned int>		   ranking(nbPlayer());
		vector<unsigned int>::iterator it_rank;
		// Loop on the sort points from the smallest
		for (int i = nbPlayer() - 1; i >= 0; i--, it_sort++)
		{
			// Loop on the total points
			for (it_player = players_.begin(), it_rank = ranking.begin(); it_player != players_.end(); ++it_player, ++it_rank)
			{
				if (*it_sort == (*it_player)->totalPoints(turn))
					*it_rank = i + 1;
			}
		}

		return *max_element(ranking.begin(), ranking.end());
	}


	bool Game::deleteTurn(const unsigned int turn)
	{
		if (!(config().turnBased()))
			throw WrongUse(_("The delete turn function should only be used in a turn based game when a player is specified"));

		if (!(player(0).hasTurn(turn)))
			throw OutOfRange(ustring::compose(_("Cannot access to the %1th turn, there is only %2 turn"), turn, player(0).nbTurn()));

		for (auto& it : players_)
			it->deleteTurn(turn);

		decreaseDistributor();

		signal_points_changed_.emit(DELETE_POINTS);
		signal_changed_.emit();

		return exceedMaxNumber();
	}



	bool Game::deleteTurn(const unsigned int turn, const unsigned int player_index)
	{
		if (config().turnBased())
			throw WrongUse(_("The delete turn function should only be used in a non turn based game when a player is specified"));

		if (!(player(player_index).hasTurn(turn)))
			throw OutOfRange(
					ustring::compose(_("Cannot access to the %1th turn, there is only %2 turn"), turn, player(player_index).nbTurn()));

		player(player_index).deleteTurn(turn);

		decreaseDistributor();

		signal_points_changed_.emit(DELETE_POINTS);
		signal_changed_.emit();

		return exceedMaxNumber();
	}

	bool Game::editTurn(const unsigned int turn, const std::vector<double>& points)
	{
		if (points.size() != nbPlayer())
			throw OutOfRange(ustring::compose(_("There is %1 points and %2 player in the game"), intToUstring(points.size()), nbPlayer()));

		auto it_points = points.cbegin();
		auto it_player = players_.begin();
		for (; it_points != points.cend(); it_player++, it_points++)
		{
			if ((*it_player)->hasTurn(turn))
				(*it_player)->setPoints(turn, *it_points);
		}

		signal_points_changed_.emit(EDIT_POINTS);
		signal_changed_.emit();

		return exceedMaxNumber();
	}

	bool Game::editTurn(const unsigned int turn, const unsigned int player_index, const double points)
	{
		player(player_index).setPoints(turn, points);

		signal_points_changed_.emit(EDIT_POINTS);
		signal_changed_.emit();

		return exceedMaxNumber();
	}


	void Game::increaseDistributor()
	{
		nb_turn_distributor_++;
		if (nb_turn_distributor_ >= config().nbTurnDistributor())
		{
			if (distributor() == nbPlayer() - 1)
				distributor_ = 0;
			else
				distributor_++;

			nb_turn_distributor_ = 0;
		}

		signal_distributor_changed_.emit();
	}


	void Game::decreaseDistributor()
	{
		if (nb_turn_distributor_ == 0)
		{
			if (distributor() == 0)
				distributor_ = nbPlayer() - 1;
			else
				distributor_--;

			nb_turn_distributor_ = config().nbTurnDistributor() - 1;
		}
		else
			nb_turn_distributor_--;

		signal_distributor_changed_.emit();
	}

	vector<unsigned int> Game::playerIndexFromPosition() const
	{
		vector<unsigned int> index;

		for (unsigned int i = 1; i <= nbPlayer(); i++)
		{
			unsigned int j = 0;
			for (auto it = players_.cbegin(); it != players_.cend(); it++, j++)
			{
				if (i == (*it)->ranking())
					index.push_back(j);
			}
		}

		return index;
	}

	void Game::writeToFile(const ustring& filename) const
	{
		Document doc;
		Element* root = doc.create_root_node(LABEL);


		// Version
		Element* node_version = root->LIBXMLXX_ADD_CHILD_ELEMENT(VERSION_LABEL);
		node_version->add_child_text(dtostr(VERSION));

		// Size max of a name
		Element* node_size_max_name = root->LIBXMLXX_ADD_CHILD_ELEMENT(SIZE_MAX_NAME_LABEL);
		node_size_max_name->add_child_text(dtostr(sizeMaxName()));

		// Date
		Element* node_date = root->LIBXMLXX_ADD_CHILD_ELEMENT(DATE_LABEL);
		Element* node_year = node_date->LIBXMLXX_ADD_CHILD_ELEMENT(YEAR_LABEL);
		node_year->add_child_text(date().format_string("%Y"));
		Element* node_month = node_date->LIBXMLXX_ADD_CHILD_ELEMENT(MONTH_LABEL);
		node_month->add_child_text(date().format_string("%m"));
		Element* node_day = node_date->LIBXMLXX_ADD_CHILD_ELEMENT(DAY_LABEL);
		node_day->add_child_text(date().format_string("%d"));

		// Nb player
		Element* node_nb_player = root->LIBXMLXX_ADD_CHILD_ELEMENT(NB_PLAYER_LABEL);
		node_nb_player->add_child_text(dtostr(nbPlayer()));

		// Distributor
		Element* node_distributor = root->LIBXMLXX_ADD_CHILD_ELEMENT(DISTRIBUTOR_LABEL);
		node_distributor->add_child_text(dtostr(distributor()));

		// Nb turn Distributor
		Element* node_nb_turn_distributor = root->LIBXMLXX_ADD_CHILD_ELEMENT(NB_TURN_DISTRIBUTOR_LABEL);
		node_nb_turn_distributor->add_child_text(dtostr(nbTurnDistributor()));

		// Game config
		config().createXmlNode(root);

		// Players
		for (auto& it : players_)
			it->createXmlNode(root);

		try
		{
			doc.write_to_file_formatted(filename, "UTF-8");
		}
		catch (xmlpp::exception& e)
		{
			g_info("%s", e.what());
			throw FileError(ustring::compose(_("Error while writing the CSU file %1"), filename));
		}

		g_debug("Game written in %s", filename.c_str());
	}

	void Game::writeToFileAsync(
			const ustring& filename, function<void(void)> return_function, function<void(csuper::Exception&)> exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game, void (Game::*)(const ustring&) const, ustring>(
				return_function, exception_function, copy, &Game::writeToFile, filename);
	}

	void Game::writeToFile(const RefPtr<File>& file) const
	{
		writeToFile(filename_to_utf8(file->get_path()));
	}

	void Game::writeToFileAsync(
			const RefPtr<File>& file, function<void(void)> return_function, function<void(csuper::Exception&)> exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game, void (Game::*)(const RefPtr<File>&) const, RefPtr<File>>(
				return_function, exception_function, copy, &Game::writeToFile, file);
	}

	void Game::reWriteToFile(const ustring& filename) const
	{
		ustring tmp_filename = filename + ".tmp";
		try
		{
			writeToFile(tmp_filename);
			removeFile(filename);
			moveFile(tmp_filename, filename);
		}
		catch (Glib::Exception& e)
		{
			g_info("%s", e.what().c_str());
			throw FileError(ustring::compose(_("Error while rewriting the file %1"), filename));
		}
	}

	void Game::reWriteToFileAsync(
			const ustring& filename, function<void(void)> return_function, function<void(csuper::Exception&)> exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game, void (Game::*)(const ustring&) const, ustring>(
				return_function, exception_function, copy, &Game::reWriteToFile, filename);
	}

	void Game::reWriteToFile(const RefPtr<File>& file) const
	{
		reWriteToFile(filename_to_utf8(file->get_path()));
	}

	void Game::reWriteToFileAsync(
			const RefPtr<File>& file, function<void(void)> return_function, function<void(csuper::Exception&)> exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game, void (Game::*)(const RefPtr<File>&) const, RefPtr<File>>(
				return_function, exception_function, copy, &Game::reWriteToFile, file);
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Statistics ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	unsigned int Game::nbTurnBestWorst(const unsigned int player_index, const bool best) const
	{
		if (!(config().turnBased()))
			throw WrongUse(_("The nbTurnBestWorst function should only be used in a turn based game"));

		unsigned int nb = 0;
		double		 points;

		unsigned int i;
		for (i = 1; i <= nbTurn(0); i++)
		{
			if ((config().maxWinner() && best) || (!(config().maxWinner() || best)))
				points = -DBL_MAX;
			else
				points = DBL_MAX;

			for (auto& it : players_)
			{
				if ((config().maxWinner() && best) || (!(config().maxWinner() || best)))
					points = max(points, it->points(i));
				else
					points = min(points, it->points(i));
			}
			if (points == player(player_index).points(i))
				nb++;
		}

		return nb;
	}

	unsigned int Game::nbTurnFirstLast(const unsigned int player_index, const bool first) const
	{
		if (!(config().turnBased()))
			throw WrongUse(_("The nbTurnFirstLast function should only be used in a turn based game"));

		unsigned int nb = 0, i;

		for (i = 1; i <= nbTurn(0); i++)
		{
			unsigned int compare_rank;
			if (first)
				compare_rank = 1;
			else
				compare_rank = lastRanking(i);

			if (compare_rank == ranking(player_index, i))
				nb++;
		}

		return nb;
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Export ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Game::exportToCsv(const Glib::ustring& filename) const
	{
		ofstream file;
		file.exceptions(ofstream::failbit | ofstream::badbit);
		try
		{
			file.open(locale_from_utf8(filename), ofstream::out);
		}
		catch (ios_base::failure& e)
		{
			throw FileError(_("Error while exporting the game into a CSV file, bad filename: ") + filename);
		}

		unsigned int i, j;

		// Header
		file << _("Csu file;") << endl
			 << _("Created on the;") << dateUstring().raw() << ";" << endl
			 << _("File's version;") << VERSION << ";" << endl
			 << _("Maximum size of the names;") << sizeMaxName() << ";" << endl
			 << _("Number of players;") << nbPlayer() << ";" << endl
			 << _("Maximum number of turns;") << maxNbTurn() << ";" << endl;

		// Game configuration
		file << _("Name of the game configuration;") << config().name().raw() << ";" << endl
			 << _("Use of a maximum score;") << config().useMaximumUstring().raw() << ";" << endl
			 << _("Initial score;") << config().initialScore() << ";" << endl
			 << _("Number of decimals displayed;") << config().decimalPlace() << ";" << endl
			 << _("The first has the highest score;") << config().maxWinnerUstring().raw() << ";" << endl
			 << _("Turn-based game;") << config().turnBasedUstring().raw() << ";" << endl
			 << _("Use of a distributor;") << config().useDistributorUstring().raw() << ";" << endl
			 << _("Maximum/minimum number of points;") << config().nbMaxMin() << ";" << endl
			 << endl;


		// Statistics

		file << _("Names;");
		for (auto& it : players_)
			file << it->name().raw() << ";";

		file << endl << _("Mean points;");
		for (auto& it : players_)
			file << it->meanPoints() << ";";

		file << endl << _("Number of turn;");
		for (auto& it : players_)
			file << it->nbTurn() << ";";
		if (config().turnBased())
		{
			file << endl << _("Number of turn with the best score;");
			for (i = 0; i < nbPlayer(); i++)
				file << nbTurnBest(i) << ";";

			file << endl << _("Number of turn with the worst score;");
			for (i = 0; i < nbPlayer(); i++)
				file << nbTurnWorst(i) << ";";

			file << endl << _("Number of turn first;");
			for (i = 0; i < nbPlayer(); i++)
				file << nbTurnFirst(i) << ";";

			file << endl << _("Number of turn last;");
			for (i = 0; i < nbPlayer(); i++)
				file << nbTurnLast(i) << ";";
		}

		// Points
		file << endl << endl << _("Names");
		for (auto& it : players_)
			file << ";;" << it->name().raw() << ";";

		file << endl << _("Legend;");
		for (i = 0; i < nbPlayer(); i++)
			file << _("Points in the turn;Points;Ranking;");

		for (i = 0; i <= maxNbTurn(); i++)
		{
			file << endl << _("Turn ") << i << ";";
			for (j = 0; j < nbPlayer(); j++)
			{
				if (player(j).hasTurn(i))
				{
					file << points(j, i) << ";" << totalPoints(j, i) << ";";
					if (config().turnBased())
						file << ranking(j, i);
					file << ";";
				}
				else
					file << ";;;";
			}
		}


		// Conclusion


		file << endl << endl << _("Names;");
		for (auto& it : players_)
			file << it->name().raw() << ";";

		file << endl << _("Total points;");
		for (auto& it : players_)
			file << it->totalPoints() << ";";

		file << endl << _("Ranking;");
		for (auto& it : players_)
			file << it->ranking() << ";";


		file.close();

		g_debug("Game export to CSV in %s", filename.c_str());
	}

	void Game::exportToCsvAsync(
			const ustring& filename, function<void(void)> return_function, function<void(csuper::Exception&)> exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game>(
				return_function, exception_function, copy, &Game::exportToCsv, filename);
	}


	void Game::exportToMarkdown(const Glib::ustring& filename) const
	{
		ustring	 simple_filename;
		ofstream file;

		// Create file
		file.exceptions(ofstream::failbit | ofstream::badbit);
		try
		{
			file.open(locale_from_utf8(filename), ofstream::out);
		}
		catch (ios_base::failure& e)
		{
			throw FileError(_("Error while exporting the game into a CSV file, bad filename: ") + filename);
		}


		simple_filename = path_get_basename(filename);
		removeFileExtension(simple_filename);

		// Header
		file << simple_filename << endl
			 << "==========" << endl
			 << endl
			 << _("Game informations") << endl
			 << "----------" << endl
			 << endl
			 << ustring::compose(_("File created on the %1"), dateUstring()).raw() << "  " << endl
			 << ustring::compose(_("Number of players: %1"), nbPlayerUstring()).raw() << "  " << endl
			 << ustring::compose(_("Maximum number of turns: %1"), maxNbTurn()).raw() << "  " << endl
			 << ustring::compose(_("Name of the game configuration: %1"), config().nameUstring()).raw() << endl
			 << endl;

		enum TableExport
		{
			TotalPoints,
			Points,
			Ranking,
			EndResult,
			Statistics
		};

		// Table function
		auto table_function = [&](TableExport table_config)
		{
			unsigned int i, j;

			// Title
			switch (table_config)
			{
			case TableExport::TotalPoints:
				file << _("Total points") << endl;
				break;
			case TableExport::Points:
				file << _("Points") << endl;
				break;
			case TableExport::Ranking:
				file << _("Ranking") << endl;
				break;
			case TableExport::EndResult:
				file << _("End results") << endl;
				break;
			case TableExport::Statistics:
				file << _("Statistics") << endl;
				break;
			}
			file << "----------" << endl << endl;

			// Names
			// TRANSLATORS:The number of characters before the | must be nine
			file << "| " << _("Names    |");
			for (auto& p : players_)
			{
				file << " " << p->name().raw();
				for (i = p->name().size(); i < 6; i++)
					file << " ";
				file << " |";
			}
			file << endl;

			// Options
			file << "|:--------:|";
			for (auto& p : players_)
			{
				file << ":";
				for (i = 0; i < p->name().size(); i++)
					file << "-";
				for (i = p->name().size(); i < 6; i++)
					file << "-";
				file << ":|";
			}
			file << endl;

			// Content
			switch (table_config)
			{
			case TableExport::TotalPoints:
			case TableExport::Points:
			case TableExport::Ranking:
				for (i = 0; i <= maxNbTurn(); i++)
				{
					// TRANSLATORS:The number of characters must be four
					file << "| " << _("Turn") << " " << intToUstring(i, 3) << " |";
					for (auto& p : players_)
					{
						// Get name rize
						unsigned int name_size = p->name().size();
						if (name_size < 6)
							name_size = 6;

						// Insirt blank
						for (j = 6; j < name_size; j++)
							file << " ";

						if (p->hasTurn(i))
						{
							switch (table_config)
							{
							case TableExport::TotalPoints:
								file << " " << p->totalPointsUstring(config(), i, 6).raw() << " |";
								break;
							case TableExport::Points:
								file << " " << p->pointsUstring(config(), i, 6).raw() << " |";
								break;
							case TableExport::Ranking:
								file << " " << rankingUstring(p->name(), i, 6).raw() << " |";
								break;
							default:
								break;
							}
						}
						else
							file << "         |";
					}
					file << endl;
				}
				break;

			case TableExport::EndResult:
				// TRANSLATORS:The number of characters before the | must be nine
				file << "| " << _("Points   |");
				for (auto& p : players_)
				{
					unsigned int name_size = p->name().size();
					if (name_size < 6)
						name_size = 6;

					for (j = 6; j < name_size; j++)
						file << " ";

					file << " " << p->totalPointsUstring(config(), -1, 6).raw() << " |";
				}
				file << endl;

				// TRANSLATORS:The number of characters before the | must be nine
				file << "| " << _("Ranking  |");
				for (auto& p : players_)
				{
					unsigned int name_size = p->name().size();
					if (name_size < 6)
						name_size = 6;

					for (j = 6; j < name_size; j++)
						file << " ";

					file << " " << rankingUstring(p->name(), -1, 6).raw() << " |";
				}
				file << endl;
				break;

			case TableExport::Statistics:
				file << "| " << _("Nb turn") << " |";
				for (auto& p : players_)
				{
					unsigned int name_size = p->name().size();
					if (name_size < 6)
						name_size = 6;

					for (j = 6; j < name_size; j++)
						file << " ";

					file << " " << p->nbTurnUstring().raw() << " |";
				}
				file << endl;

				file << "| " << _("Mean points") << " |";
				for (i = 0; i < nbPlayer(); i++)
				{
					unsigned int name_size = player(i).name().size();
					if (name_size < 6)
						name_size = 6;

					for (j = 6; j < name_size; j++)
						file << " ";

					file << " " << meanPointsUstring(i).raw() << " |";
				}
				file << endl;

				if (config().turnBased())
				{
					file << "| " << _("Nb turn best") << " |";
					for (i = 0; i < nbPlayer(); i++)
					{
						unsigned int name_size = player(i).name().size();
						if (name_size < 6)
							name_size = 6;

						for (j = 6; j < name_size; j++)
							file << " ";

						file << " " << nbTurnBestUstring(i).raw() << " |";
					}
					file << endl;

					file << "| " << _("Nb turn worst") << " |";
					for (i = 0; i < nbPlayer(); i++)
					{
						unsigned int name_size = player(i).name().size();
						if (name_size < 6)
							name_size = 6;

						for (j = 6; j < name_size; j++)
							file << " ";

						file << " " << nbTurnWorstUstring(i).raw() << " |";
					}
					file << endl;

					file << "| " << _("Nb turn first") << " |";
					for (i = 0; i < nbPlayer(); i++)
					{
						unsigned int name_size = player(i).name().size();
						if (name_size < 6)
							name_size = 6;

						for (j = 6; j < name_size; j++)
							file << " ";

						file << " " << nbTurnFirstUstring(i).raw() << " |";
					}
					file << endl;

					file << "| " << _("Nb turn last") << " |";
					for (i = 0; i < nbPlayer(); i++)
					{
						unsigned int name_size = player(i).name().size();
						if (name_size < 6)
							name_size = 6;

						for (j = 6; j < name_size; j++)
							file << " ";

						file << " " << nbTurnLastUstring(i).raw() << " |";
					}
					file << endl;
				}
				break;
			}

			file << endl;
		};

		// Total Points
		table_function(TableExport::TotalPoints);

		// Points
		table_function(TableExport::Points);

		// Ranking
		if (config().turnBased())
			table_function(TableExport::Ranking);

		// End result
		table_function(TableExport::EndResult);

		// End result
		table_function(TableExport::Statistics);


		// Charts
		file << _("Charts") << endl
			 << "----------" << endl
			 << endl
			 << "![" << _("Total points") << "](" << simple_filename << "_total_points.svg)" << endl
			 << endl
			 << "![" << _("Points") << "](" << simple_filename << "_points.svg)" << endl;

		file.close();

		g_debug("Game export to Markdown in %s", filename.c_str());
	}

	void Game::exportToMarkdownAsync(
			const ustring& filename, function<void(void)> return_function, function<void(csuper::Exception&)> exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game>(
				return_function, exception_function, copy, &Game::exportToMarkdown, filename);
	}

	void Game::exportToM(const Glib::ustring& filename) const
	{
		ofstream file;
		file.exceptions(ofstream::failbit | ofstream::badbit);
		try
		{
			file.open(locale_from_utf8(filename), ofstream::out);
		}
		catch (ios_base::failure& e)
		{
			throw FileError(_("Error while exporting the game into a m file, bad filename: ") + filename);
		}

		vector<Player*>::const_iterator it;
		unsigned int					i, tabs_name;

		file << "clear all;" << endl << "close all;" << endl;


		// x for players
		for (it = players_.cbegin(), tabs_name = 0; it != players_.cend(); ++it, tabs_name++)
			file << endl << "turn_" << tabs_name << "=0:1:" << (*it)->nbTurn() << ";";
		file << endl;


		// Tabs of total points
		for (it = players_.cbegin(), tabs_name = 0; it != players_.cend(); ++it, tabs_name++)
		{
			file << endl << "total_points_" << tabs_name << "=[";
			for (i = 0; i <= maxNbTurn(); i++)
			{
				if ((*it)->hasTurn(i))
					file << replaceCharacterInUstring((*it)->totalPointsUstring(config(), i), ',', '.').raw() << ",";
				else
					break;
			}
			file << "];";
		}


		// Plot total points
		file << endl << "plot(";
		for (tabs_name = 0; tabs_name < nbPlayer(); tabs_name++)
		{
			file << "turn_" << tabs_name << ",total_points_" << tabs_name;
			if (tabs_name != nbPlayer() - 1)
				file << ",";
		}
		file << ");" << endl
			 << "title('" << _("Total points") << "');" << endl
			 << "xlabel('" << _("Turn") << "');" << endl
			 << "ylabel('" << _("Points") << "');" << endl;


		file << "legend(";
		for (it = players_.cbegin(), tabs_name = 0; tabs_name < nbPlayer(); tabs_name++, ++it)
		{
			file << "'" << (*it)->nameUstring().raw() << "'";
			if (tabs_name != nbPlayer() - 1)
				file << ",";
		}
		file << ");" << endl;


		// Tabs of points and mean points
		for (it = players_.cbegin(), tabs_name = 0; it != players_.cend(); ++it, tabs_name++)
		{
			file << endl << "points_" << tabs_name << "=[";
			for (i = 0; i <= maxNbTurn(); i++)
			{
				if ((*it)->hasTurn(i))
					file << replaceCharacterInUstring((*it)->pointsUstring(config(), i), ',', '.').raw() << ",";
				else
					break;
			}
			file << "];" << endl
				 << "fprintf('" << ustring::compose(_("Mean score of %1: "), (*it)->name()).raw() << "%f\\n',mean(points_" << tabs_name
				 << "));";
		}


		// Plot points
		file << endl << endl << "figure;" << endl << "plot(";
		for (tabs_name = 0; tabs_name < nbPlayer(); tabs_name++)
		{
			file << "turn_" << tabs_name << ",points_" << tabs_name;
			if (tabs_name != nbPlayer() - 1)
				file << ",";
		}
		file << ");" << endl
			 << "title('" << _("Points") << "');" << endl
			 << "xlabel('" << _("Turn") << "');" << endl
			 << "ylabel('" << _("Points") << "');" << endl;


		file << "legend(";
		for (it = players_.cbegin(), tabs_name = 0; tabs_name < nbPlayer(); tabs_name++, ++it)
		{
			file << "'" << (*it)->nameUstring().raw() << "'";
			if (tabs_name != nbPlayer() - 1)
				file << ",";
		}
		file << ");" << endl;


		file.close();

		g_debug("Game export to M in %s", filename.c_str());
	}

	void Game::exportToMAsync(const Glib::ustring&	filename,
			std::function<void(void)>				return_function,
			std::function<void(csuper::Exception&)> exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game>(
				return_function, exception_function, copy, &Game::exportToM, filename);
	}

	void Game::exportToGnuplotData(const Glib::ustring& filename) const
	{
		ofstream file;
		file.exceptions(ofstream::failbit | ofstream::badbit);
		try
		{
			file.open(locale_from_utf8(filename + ".dat"), ofstream::out);
		}
		catch (ios_base::failure& e)
		{
			throw FileError(_("Error while exporting the game into a m file, bad filename: ") + filename);
		}

		unsigned int i;

		file << "\"" << _("Players") << "\"";
		for (auto& it : players_)
			file << "\t\"" << it->name().raw() << "\"";

		for (i = 0; i <= maxNbTurn(); i++)
		{
			file << endl << i;
			for (auto& it : players_)
			{
				if (it->hasTurn(i))
					file << "\t" << replaceCharacterInUstring(it->totalPointsUstring(config(), i), ',', '.').raw();
				else
					file << "\t-";
			}
		}


		file.close();
	}

	void Game::exportToGnuplotScript(const Glib::ustring& filename) const
	{
		ofstream file;
		file.exceptions(ofstream::failbit | ofstream::badbit);
		try
		{
			file.open(locale_from_utf8(filename + ".plt"), ofstream::out);
		}
		catch (ios_base::failure& e)
		{
			throw FileError(_("Error while exporting the game into a m file, bad filename: ") + filename);
		}

		file << "set datafile missing '-'" << endl
			 << "set style data linespoints" << endl
			 << "set xlabel \"" << _("Number of turns") << "\"" << endl
			 << "set ylabel \"" << _("Points") << "\"" << endl
			 << "set title \"" << _("Points on ") << path_get_basename(filename) << "\"" << endl;

		file << "plot '" << path_get_basename(filename) << ".dat"
			 << "' using 2:xtic(1) title columnheader(2),"
			 << " for [i=3:" << (nbPlayer() + 1) << "] '' using i title columnheader(i)" << endl
			 << "pause -1";


		file.close();
	}

	void Game::exportToGnuplot(const ustring& filename) const
	{
		exportToGnuplotData(filename);
		exportToGnuplotScript(filename);

		g_debug("Game export to Gnuplot in %s", filename.c_str());
	}

	void Game::exportToGnuplotAsync(const Glib::ustring& filename,
			std::function<void(void)>					 return_function,
			std::function<void(csuper::Exception&)>		 exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game>(
				return_function, exception_function, copy, &Game::exportToGnuplot, filename);
	}

	void Game::exportToPdf(
			const ustring& filename, const ExportPdfPreferences& pdf_pref, const ChartExportationPreferences& chart_pref) const
	{
		// Save the temporary file
		try
		{
			PdfExportation::exportToPdf(this, pdf_pref, chart_pref, filename);
		}
		catch (Glib::Exception& e)
		{
			g_info("%s", e.what().c_str());

			throw PdfError(ustring::compose(_("The file %1 cannot be created"), filename));
		}

		g_debug("Game export to PDF in %s", filename.c_str());
	}

	void Game::exportToPdfAsync(const Glib::ustring& filename,
			const ExportPdfPreferences&				 pdf_pref,
			const ChartExportationPreferences&		 chart_pref,
			std::function<void(void)>				 return_function,
			std::function<void(csuper::Exception&)>	 exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game>(
				return_function, exception_function, copy, &Game::exportToPdf, filename, pdf_pref, chart_pref);
	}

	void Game::exportToChart(const Glib::ustring& filename,
			const ChartExportationPreferences&	  chart_pref,
			const ExportPdfPreferences&			  pdf_pref,
			const ChartExportationType			  type) const
	{
		ustring title = path_get_basename(filename);
		removeFileExtension(title);
		if (chart_pref.totalPoints())
			title = _("Total points on ") + title;
		else
			title = _("Points on ") + title;

		SlopeColor	 color = SLOPE_BLACK;
		unsigned int i, j;

		double*	 slope_turn	  = new double[maxNbTurn() + 1];
		double** slope_points = new double*[nbPlayer()];

		for (i = 0; i <= maxNbTurn(); i++)
			slope_turn[i] = i;

		SlopeScale* scale = slope_xyscale_new_axis(_("Turns"), _("Points"), title.c_str());
		SlopeItem*	axis  = slope_xyscale_get_axis(SLOPE_XYSCALE(scale), SLOPE_XYSCALE_AXIS_BOTTOM);
		slope_xyaxis_set_components(SLOPE_XYAXIS(axis), SLOPE_XYAXIS_DEFAULT_DOWN_GRID);
		axis = slope_xyscale_get_axis(SLOPE_XYSCALE(scale), SLOPE_XYSCALE_AXIS_LEFT);
		slope_xyaxis_set_components(SLOPE_XYAXIS(axis), SLOPE_XYAXIS_DEFAULT_DOWN_GRID);
		axis = slope_xyscale_get_axis(SLOPE_XYSCALE(scale), SLOPE_XYSCALE_AXIS_TOP);
		slope_xyaxis_set_components(SLOPE_XYAXIS(axis), SLOPE_XYAXIS_LINE | SLOPE_XYAXIS_TITLE);

		vector<SlopeItem*> items;

		// Create the slope chart
		vector<Player*>::const_iterator it;
		for (i = 0, it = players_.cbegin(); it != players_.cend(); ++it, i++)
		{
			if (!(chart_pref.totalPoints()))
			{
				slope_points[i] = (*it)->points_.data();
			}
			else
			{
				slope_points[i] = new double[(*it)->nbTurn() + 1];
				for (j = 0; j <= nbTurn(i); j++)
					slope_points[i][j] = (*it)->totalPoints(j);
			}

			SlopeItem* tmp_item = slope_xyseries_new_filled((*it)->name().c_str(),
					slope_turn,
					slope_points[i],
					(*it)->nbTurn() + 1,
					color,
					SLOPE_WHITE,
					static_cast<SlopeXySeriesMode>(SLOPE_SERIES_CIRCLES | SLOPE_SERIES_LINE));
			items.push_back(tmp_item);
			slope_scale_add_item(scale, tmp_item);


			// Change the color
			switch (color)
			{
			case SLOPE_BLACK:
				color = SLOPE_RED;
				break;
			case SLOPE_RED:
				color = SLOPE_BLUE;
				break;
			case SLOPE_BLUE:
				color = SLOPE_GREEN;
				break;
			case SLOPE_GREEN:
				color = SLOPE_BLUEVIOLET;
				break;
			case SLOPE_BLUEVIOLET:
				color = SLOPE_HOTPINK;
				break;
			case SLOPE_HOTPINK:
				color = SLOPE_TEAL;
				break;
			case SLOPE_TEAL:
				color = SLOPE_YELLOW;
				break;
			case SLOPE_YELLOW:
				color = SLOPE_DARKMAGENTA;
				break;
			case SLOPE_DARKMAGENTA:
				color = SLOPE_MAROON;
				break;
			case SLOPE_MAROON:
				color = SLOPE_CYAN;
				break;
			case SLOPE_CYAN:
				color = SLOPE_BLACK;
				break;
			}
		}
		SlopeFigure* slope_figure = slope_figure_new();
		slope_figure_add_scale(SLOPE_FIGURE(slope_figure), scale);
		SlopeItem* legend = slope_figure_get_legend(SLOPE_FIGURE(slope_figure));
		slope_legend_set_default_position(SLOPE_LEGEND(legend), SLOPE_LEGEND_BOTTOMRIGHT);

		// Save the chart
		int	   res = 0;
		int	   width;
		int	   height;
		string locale_filename = locale_from_utf8(filename);
		switch (static_cast<int>(type))
		{
		case SVG:
			res = slope_figure_write_to_svg(slope_figure, locale_filename.c_str(), chart_pref.width(), chart_pref.height());
			break;
		case PNG:
			res = slope_figure_write_to_png(slope_figure, locale_filename.c_str(), chart_pref.width(), chart_pref.height());
			break;
		case PDF:
			if (pdf_pref.pdfSizeForChart())
			{
				switch (static_cast<int>(pdf_pref.size()))
				{
				case ExportPdfPreferences::A3:
					width  = 842;
					height = 1190;
					break;
				case ExportPdfPreferences::A4:
					width  = 595;
					height = 842;
					break;
				case ExportPdfPreferences::A5:
					width  = 420;
					height = 595;
					break;
				default:
					width  = 0;
					height = 0;
					break;
				}
				if (pdf_pref.direction() == ExportPdfPreferences::LANDSCAPE)
				{
					int tmp = width;
					width	= height;
					height	= tmp;
				}
			}
			else
			{
				width  = chart_pref.width();
				height = chart_pref.height();
			}
			res = slope_figure_write_to_pdf(slope_figure, locale_filename.c_str(), width, height);
			break;
		}

		// Delete
		delete[] slope_turn;
		if (chart_pref.totalPoints())
		{
			for (i = 0; i < nbPlayer(); i++)
				delete[] slope_points[i];
		}
		delete[] slope_points;
		/*g_clear_object(&scale);
		g_clear_object(&slope_figure);
		for (auto& item : items)
			g_clear_object(&item);*/

		if (res != 0)
			throw FileError(ustring::compose(_("Error while exporting the game into chart file, %1 is a bad filename."), filename));

		g_debug("Game export to chart (type %d) in %s", type, filename.c_str());
	}

	void Game::exportToSvgAsync(const Glib::ustring& filename,
			const ChartExportationPreferences&		 pref,
			std::function<void(void)>				 return_function,
			std::function<void(csuper::Exception&)>	 exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game>(
				return_function, exception_function, copy, &Game::exportToSvg, filename, pref);
	}

	void Game::exportToPngAsync(const Glib::ustring& filename,
			const ChartExportationPreferences&		 pref,
			std::function<void(void)>				 return_function,
			std::function<void(csuper::Exception&)>	 exception_function) const
	{
		GamePtr copy = this->clone();
		copy->execVoidAsynchronouslyWithoutLock<csuper::Exception, Game>(
				return_function, exception_function, copy, &Game::exportToPng, filename, pref);
	}
}	 // namespace csuper
