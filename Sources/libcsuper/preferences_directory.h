/*!
 * \file    preferences_score_display.h
 * \author  Remi BERTHO
 * \date    13/02/16
 * \version 4.3.1
 */

/*
* preferences_score_display.h
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef DIRECTORY_H_INCLUDED
#define DIRECTORY_H_INCLUDED

#include "share.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class DirectoryPreferences
	 *   \brief This class indicate the directory preference
	 * This class contain:
	 *  - The directory for opening file
	 */
	class DirectoryPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* OPEN_LABEL = "open";

	public:
		static constexpr const char* LABEL = "directory";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		DirectoryPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param directory
		 */
		explicit DirectoryPreferences(const Glib::ustring& open);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit DirectoryPreferences(const xmlpp::Node* xml_node);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the open directory
		 *  \param the open directory
		 */
		inline void setOpen(const Glib::ustring& open)
		{
			setUstring(OPEN_LABEL, open);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the open directory
		 *  \return the open directory
		 */
		inline Glib::ustring open() const
		{
			return getUstring(OPEN_LABEL);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the open directory in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring openUstring() const
		{
			return open();
		}
	};
}	 // namespace csuper


#endif	  // DIRECTORY_H_INCLUDED
