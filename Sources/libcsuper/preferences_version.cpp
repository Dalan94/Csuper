/*!
 * \file    preferences_version.h
 * \author  Remi BERTHO
 * \date    10/02/16
 * \version 4.3.1
 */

/*
 * preferences_version.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "preferences_version.h"

using namespace Glib;
using namespace xmlpp;

namespace csuper
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	VersionPreferences::VersionPreferences() : VersionPreferences(Version())
	{
	}

	VersionPreferences::VersionPreferences(const Version& version)
			: AbstractPreference(LABEL, AbstractPreference::USTRING, LAST_VERSION_CHECK_LABEL)
	{
		setLastCheckVersion(version);
	}

	VersionPreferences::VersionPreferences(const Node* xml_node)
			: AbstractPreference(xml_node, AbstractPreference::USTRING, LAST_VERSION_CHECK_LABEL)
	{
	}
}	 // namespace csuper
