/*!
 * \file    share.cpp
 * \brief   Essential function of libcsuper
 * \author  Remi BERTHO
 * \date    10/02/15
 * \version 4.3.1
 */

/*
 * share.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "share.h"
#include "exceptions.h"
#include "list_game_configuration.h"
#include "preferences.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <clocale>
#include <cmath>
#include <fstream>
#include <sys/stat.h>
#include <glib/gstdio.h>
#include <giomm.h>

namespace csuper
{

	using namespace std;
	using namespace xmlpp;
	using namespace Glib;
	using namespace Gio;

	bool Portable::portable_(false);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// General ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	void csuperInitialize(const bool portable)
	{
		static bool initialized = false;
		if (initialized == false)
		{
			g_debug("Starting Initializing libcsuper with portable %s", boolToUstring(portable).c_str());

			bindtextdomain("libcsuper", "Locales");
			bind_textdomain_codeset("libcsuper", "UTF-8");
			initialized = true;

			Portable::setPortable(portable);

			ustring folder;
			ustring home = locale_to_utf8(get_home_dir());

			if (portable)
				folder = DIRECTORY_NAME;
			else
				folder = build_filename(home, DIRECTORY_NAME);
#ifdef G_OS_UNIX
			g_mkdir(folder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#elif _WIN32
			g_mkdir(folder.c_str(), 0);
#endif

			string pref		   = build_filename(folder, Preferences::PREFERENCES_FILENAME);
			string game_config = build_filename(folder, ListGameConfiguration::GAME_CONFIGURATIONS_FILENAME);

			if (!file_test(pref, FILE_TEST_EXISTS))
			{
				Preferences pref;
				pref.writeToFile();
				g_info(_("Creation of the preferences file."));
			}

			if (!file_test(game_config, FILE_TEST_EXISTS))
			{
				ListGameConfiguration list_config;
				list_config.writeToFile(game_config);
				g_warning(_("Creation of the game configuration file."));
			}

			g_debug("End Initializing libcsuper");
		}
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Conversion ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	ustring boolToYesNo(bool b)
	{
		if (b)
			return _("yes");
		else
			return _("no");
	}

	ustring boolToUstring(bool b)
	{
		if (b)
			return "yes";
		else
			return "no";
	}

	bool ustringToBool(const ustring& str)
	{
		return str == "yes";
	}

	ustring intToUstring(const int i, const unsigned int width)
	{
		return ustring::format(setw(width), i);
	}

	int ustringToInt(const ustring& str)
	{
		lconv* lc = localeconv();
		removeCharacterInUstring(str, *(lc->thousands_sep));
		return atoi(str.c_str());
	}


	ustring doubleToUstring(const double d, const int decimals, const unsigned int width)
	{
		if (decimals < 0)
		{
			if (width == 0)
				return ustring::format(d);
			else
				return ustring::format(fixed, setw(width), d);
		}
		else
		{
			if (width == 0)
				return ustring::format(fixed, setprecision(decimals), d);
			else
				return ustring::format(fixed, setprecision(decimals), setw(width), d);
		}
	}

	ustring dtostr(const double d)
	{
#ifdef G_OS_WIN32
		if (d == INFINITY)
			return "inf";
		else
#endif	  // G_OS_WIN32
			return Ascii::dtostr(d);
	}


	double ustringToDouble(const ustring& str)
	{
		ustring copy_str(str);
		lconv*	lc = localeconv();

		// Remove the thousands separator
		removeCharacterInUstring(copy_str, *(lc->thousands_sep));
		replaceCharacterInUstring(copy_str, *(lc->decimal_point), '.');

#ifdef _WIN32
		if (copy_str == "inf")
			return INFINITY;
		else
#endif	  // _WIN32

			return Ascii::strtod(copy_str);
	}

	Glib::ustring replaceCharacterInUstring(const Glib::ustring& str, const char old_character, const char new_character)
	{
		ustring ret(str);
		if (old_character != new_character)
		{
			ustring::size_type pos;
			while ((pos = ret.find(old_character)) != ustring::npos)
				ret.replace(pos, 1, ustring(1, new_character));
		}
		return ret;
	}

	Glib::ustring& replaceCharacterInUstring(Glib::ustring& str, const char old_character, const char new_character)
	{
		if (old_character != new_character)
		{
			ustring::size_type pos;
			while ((pos = str.find(old_character)) != ustring::npos)
				str.replace(pos, 1, ustring(1, new_character));
		}
		return str;
	}

	Glib::ustring removeCharacterInUstring(const Glib::ustring& str, const char character)
	{
		ustring::size_type pos;
		ustring			   ret(str);
		while ((pos = ret.find(character)) != ustring::npos)
			ret.erase(pos, 1);
		return ret;
	}

	Glib::ustring& removeCharacterInUstring(Glib::ustring& str, const char character)
	{
		ustring::size_type pos;
		while ((pos = str.find(character)) != ustring::npos)
			str.erase(pos, 1);
		return str;
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// XML ///////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	const Element* getChildElement(const Node* node, ustring name)
	{
		const Node* child_node;
		child_node = node->get_first_child(name);

		if (child_node == nullptr)
			throw XmlError(ustring::compose(_("No child node named %1 in %2."), name, node->get_name()));

		const Element* child_element = dynamic_cast<const Element*>(child_node);

		if (child_element == nullptr)
			throw XmlError(ustring::compose(_("No element node named %1."), name));

		return child_element;
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// File //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool checkFilename(const ustring& filename)
	{
		if (filename.empty())
			return false;

		string full_filename = build_filename(get_tmp_dir(), locale_from_utf8(filename));

		RefPtr<File> file = File::create_for_path(full_filename);
		try
		{
			file->create_file();
			file->remove();
			return true;
		}
		catch (...)
		{
			return false;
		}
	}


	bool checkFolder(const ustring& folder)
	{
		if (folder.empty())
			return false;

		string filename = build_filename(locale_from_utf8(folder), "test_csu_tmp");

		RefPtr<File> file = File::create_for_path(filename);
		try
		{
			file->create_file();
			file->remove();
			return true;
		}
		catch (...)
		{
			return false;
		}
	}

	void removeFile(const ustring& filename)
	{
		RefPtr<File> file = File::create_for_path(filename_from_utf8(filename));
		try
		{
			if (!(file->remove()))
				throw csuper::FileError(ustring::compose(_("Error when deleting %1."), filename));
		}
		catch (Glib::Error& e)
		{
			g_info("%s", e.what().c_str());
			throw csuper::FileError(ustring::compose(_("Error when deleting %1."), filename));
		}
	}

	void trashFile(const ustring& filename)
	{
		RefPtr<File> file = File::create_for_path(filename_from_utf8(filename));
		try
		{
			if (!(file->trash()))
				throw csuper::FileError(ustring::compose(_("Error when trashing %1."), filename));
		}
		catch (Glib::Error& e)
		{
			g_info("%s", e.what().c_str());
			throw csuper::FileError(ustring::compose(_("Error when trashing %1."), filename));
		}
	}

	void moveFile(const ustring& old_filename, const ustring& new_filename)
	{
		RefPtr<File> old_file = File::create_for_path(filename_from_utf8(old_filename));
		RefPtr<File> new_file = File::create_for_path(filename_from_utf8(new_filename));
		try
		{
			if (!(old_file->move(new_file)))
				throw csuper::FileError(ustring::compose(_("Error when moving %1 to %2."), old_filename, new_filename));
		}
		catch (Glib::Error& e)
		{
			g_info("%s", e.what().c_str());
			throw csuper::FileError(ustring::compose(_("Error when moving %1 to %2."), old_filename, new_filename));
		}
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Extension /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ustring addFileExtension(const ustring& filename, const ustring& file_extension)
	{
		if (filename.substr(filename.size() - file_extension.size()) == file_extension)
			return filename;
		else
			return filename + file_extension;
	}


	ustring& addFileExtension(ustring& filename, const ustring& file_extension)
	{
		if (filename.substr(filename.size() - file_extension.size()) != file_extension)
			filename += ("." + file_extension);

		return filename;
	}


	ustring removeFileExtension(const ustring& filename)
	{
		ustring new_filename(filename);
		for (int i = new_filename.size() - 2; i > 0; i--)
		{
			if (new_filename[i] == '.')
			{
				new_filename.resize(i);
				break;
			}
		}
		return new_filename;
	}


	ustring& removeFileExtension(ustring& filename)
	{
		for (int i = filename.size() - 2; i > 0; i--)
		{
			if (filename[i] == '.')
			{
				filename.resize(i);
				break;
			}
		}
		return filename;
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Variant functions /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool variantToBool(Glib::VariantBase var)
	{
		try
		{
			Variant<bool> derived = VariantBase::cast_dynamic<Variant<bool>>(var);
			return derived.get();
		}
		catch (bad_cast& e)
		{
			g_info("%s", e.what());
			throw BadCast(_("Error when casting a variant."));
		}
	}

	ustring variantToUstring(Glib::VariantBase var)
	{
		try
		{
			Variant<ustring> derived = VariantBase::cast_dynamic<Variant<ustring>>(var);
			return derived.get();
		}
		catch (bad_cast& e)
		{
			g_info("%s", e.what());
			throw BadCast(_("Error when casting a variant."));
		}
	}

	double variantToDouble(Glib::VariantBase var)
	{
		try
		{
			Variant<double> derived = VariantBase::cast_dynamic<Variant<double>>(var);
			return derived.get();
		}
		catch (bad_cast& e)
		{
			g_info("%s", e.what());
			throw BadCast(_("Error when casting a variant."));
		}
	}

	gint64 variantToInt64(Glib::VariantBase var)
	{
		try
		{
			Variant<gint64> derived = VariantBase::cast_dynamic<Variant<gint64>>(var);
			return derived.get();
		}
		catch (bad_cast& e)
		{
			g_info("%s", e.what());
			throw BadCast(_("Error when casting a variant."));
		}
	}

	gint32 variantToInt32(Glib::VariantBase var)
	{
		try
		{
			Variant<gint32> derived = VariantBase::cast_dynamic<Variant<gint32>>(var);
			return derived.get();
		}
		catch (bad_cast& e)
		{
			g_info("%s", e.what());
			throw BadCast(_("Error when casting a variant."));
		}
	}

	guint64 variantToUint64(Glib::VariantBase var)
	{
		try
		{
			Variant<guint64> derived = VariantBase::cast_dynamic<Variant<guint64>>(var);
			return derived.get();
		}
		catch (bad_cast& e)
		{
			g_info("%s", e.what());
			throw BadCast(_("Error when casting a variant."));
		}
	}

	guint32 variantToUint32(Glib::VariantBase var)
	{
		try
		{
			Variant<guint32> derived = VariantBase::cast_dynamic<Variant<guint32>>(var);
			return derived.get();
		}
		catch (bad_cast& e)
		{
			g_info("%s", e.what());
			throw BadCast(_("Error when casting a variant."));
		}
	}
}	 // namespace csuper
