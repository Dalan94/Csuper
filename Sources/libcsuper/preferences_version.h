/*!
 * \file    preferences_version.h
 * \author  Remi BERTHO
 * \date    10/02/16
 * \version 4.3.1
 */

/*
 * preferences_version.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PREFERENCES_VERSION_H_INCLUDED
#define PREFERENCES_VERSION_H_INCLUDED

#include "share.h"
#include "version.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class VersionPreferences
	 *   \brief This class indicate the version preference
	 * This class contain:
	 *  - The last version checked version
	 */
	class VersionPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* LAST_VERSION_CHECK_LABEL = "last_version_check";

	public:
		static constexpr const char* LABEL = "check_version";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		VersionPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param directory
		 */
		explicit VersionPreferences(const Version& version);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit VersionPreferences(const xmlpp::Node* xml_node);



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the last check version
		 *  \param version the Version
		 */
		inline void setLastCheckVersion(const Version& version)
		{
			setUstring(LAST_VERSION_CHECK_LABEL, version.toUstring());
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the last check version
		 *  \return the last check version
		 */
		inline Version lastCheckVersion() const
		{
			return Version(getAsUstring(LAST_VERSION_CHECK_LABEL));
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the last check version in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring lastCheckVersionUstring() const
		{
			return getAsUstring(LAST_VERSION_CHECK_LABEL);
		}
	};
}	 // namespace csuper

#endif	  // PREFERENCES_VERSION_H_INCLUDED
