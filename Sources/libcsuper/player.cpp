/*!
 * \file    player.cpp
 * \author  Remi BERTHO
 * \date    03/06/15
 * \version 4.3.0
 */

/*
* player.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/


#include "config.h"

#include "player.h"
#include "exceptions.h"

namespace csuper
{
	using namespace std;
	using namespace Glib;
	using namespace xmlpp;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Player::Player()
	{
	}

	Player::Player(const ustring& name, const unsigned int ranking, const vector<double>& points)
			: name_(name), points_(points), ranking_(ranking), nb_turn_(points.size() - 1)
	{
		double total = 0;
		for (auto& points : points_)
			total += points;
		total_points_ = total;
	}


	Player::Player(const double initial_points, const Glib::ustring& name) : name_(name)
	{
		points_.push_back(initial_points);
		total_points_ = initial_points;
	}

	Player::Player(const GameConfiguration& game_config, const Glib::ustring& name) : name_(name)
	{
		points_.push_back(game_config.initialScore());
		total_points_ = game_config.initialScore();
	}

	Player::Player(const Node* xml_node)
	{
		const Element* tmp_element;

		tmp_element = getChildElement(xml_node, PLAYER_NAME_LABEL);
		name_		= tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content();

		tmp_element	  = getChildElement(xml_node, TOTAL_POINTS_LABEL);
		total_points_ = ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element = getChildElement(xml_node, RANK_LABEL);
		ranking_	= ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element = getChildElement(xml_node, NUMBER_OF_TURN_LABEL);
		nb_turn_	= ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content()) - 1;

		// Get all points
		tmp_element			= getChildElement(xml_node, POINTS_LABEL);
		auto turn_node_list = tmp_element->get_children(TURN_LABEL);
		for (auto& turn_node : turn_node_list)
			points_.push_back(ustringToDouble(static_cast<const Element*>(turn_node)->LIBXMLXX_GET_CHILD_TEXT()->get_content()));
	}


	Player::Player(const Player& player)
			: name_(player.name_), total_points_(player.total_points_), points_(player.points_), ranking_(player.ranking_),
			  nb_turn_(player.nb_turn_)
	{
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Operators /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	bool Player::operator==(const Player& player) const
	{
		return (name_ == player.name_);
	}

	bool Player::operator==(const ustring& name) const
	{
		return (name_ == name);
	}

	Player& Player::operator=(const Player& player)
	{
		name_		  = player.name_;
		total_points_ = player.total_points_;
		points_		  = player.points_;
		ranking_	  = player.ranking_;
		nb_turn_	  = player.nb_turn_;

		signal_changed_.emit();

		return *this;
	}

	ostream& operator<<(ostream& os, const Player& player)
	{
		os << player.toUstring();
		return os;
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ustring Player::toUstring() const
	{
		ustring res = _("Name of the player: ") + nameUstring() + "\n" + _("Number of turn: ") + nbTurnUstring() + "\n" + _("Ranking: ") +
					  rankingUstring() + "\n" + _("\tPoints\tTotal points") + "\n";

		for (unsigned int i = 0; i < nbTurn() + 1; i++)
		{
			res += _("Turn ") + intToUstring(i) + "\t" + pointsUstring(i) + "\t" + totalPointsUstring(i) + "\n";
		}

		return res;
	}

	ustring Player::toUstring(const GameConfiguration& game_config) const
	{
		ustring res = _("Name of the player: ") + nameUstring() + "\n" + _("Number of turn: ") + nbTurnUstring() + "\n" + _("Ranking: ") +
					  rankingUstring() + "\n" + _("\tPoints\tTotal points") + "\n";

		for (unsigned int i = 0; i < nbTurn() + 1; i++)
		{
			res += _("\nTurn ") + intToUstring(i) + "\t" + pointsUstring(game_config, i) + "\t" + totalPointsUstring(game_config, i);
		}

		return res;
	}

	double Player::meanPoints() const
	{
		return total_points_ / (nb_turn_);
	}


	void Player::createXmlNode(Element* parent_node) const
	{
		Element* node = parent_node->LIBXMLXX_ADD_CHILD_ELEMENT(LABEL);

		Element* node_player_name = node->LIBXMLXX_ADD_CHILD_ELEMENT(PLAYER_NAME_LABEL);
		node_player_name->add_child_text(nameUstring());

		Element* node_total_points = node->LIBXMLXX_ADD_CHILD_ELEMENT(TOTAL_POINTS_LABEL);
		node_total_points->add_child_text(dtostr(totalPoints()));

		Element* node_rank = node->LIBXMLXX_ADD_CHILD_ELEMENT(RANK_LABEL);
		node_rank->add_child_text(dtostr(ranking()));

		Element* node_number_of_turn = node->LIBXMLXX_ADD_CHILD_ELEMENT(NUMBER_OF_TURN_LABEL);
		node_number_of_turn->add_child_text(dtostr(nb_turn_ + 1));

		Element* node_points = node->LIBXMLXX_ADD_CHILD_ELEMENT(POINTS_LABEL);
		for (unsigned int i = 0; i < nbTurn() + 1; i++)
		{
			Element* node_tmp_points = node_points->LIBXMLXX_ADD_CHILD_ELEMENT(TURN_LABEL);
			node_tmp_points->add_child_text(dtostr(points(i)));
			node_tmp_points->set_attribute(NUM_LABEL, dtostr(i));
		}
	}

	bool Player::hasTurn(const unsigned int turn) const
	{
		return (turn <= nb_turn_);
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void Player::addPoints(const double points)
	{
		points_.push_back(points);
		total_points_ += points;
		nb_turn_++;
		signal_changed_.emit();
	}

	void Player::setPoints(const unsigned int turn, const double point)
	{
		if (turn > nb_turn_)
			throw OutOfRange(ustring::compose(_("Cannot access to the %1th turn, there is only %2 turn"), turn, nbTurn()));

		double points_diff = points(turn) - point;
		points_[turn]	   = point;

		total_points_ -= points_diff;

		signal_changed_.emit();
	}

	void Player::deleteTurn(const unsigned int turn)
	{
		if (turn > nb_turn_)
			throw OutOfRange(ustring::compose(_("Cannot delete the %1th turn, there is only %2 turn"), turn, nbTurn()));

		total_points_ -= points(turn);
		points_.erase(points_.begin() + turn);
		nb_turn_--;
		signal_changed_.emit();
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	double Player::totalPoints(const int turn) const
	{
		if (turn == -1)
			return total_points_;

		double total = 0;
		for (int i = 0; i < turn + 1; i++)
			total += points(i);
		return total;
	}
}	 // namespace csuper
