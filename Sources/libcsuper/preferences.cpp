/*!
 * \file    preferences.cpp
 * \author  Remi BERTHO
 * \date    14/02/16
 * \version 4.3.1
 */

/*
 * preferences.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "preferences.h"
#include "exceptions.h"
#include <iostream>

using namespace std;
using namespace Glib;
using namespace xmlpp;

namespace csuper
{
	double Preferences::VERSION(1.3);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Preferences::Preferences()
	{
		prefs_.push_back(new MainWindowSizePreferences());
		prefs_.push_back(new DifferenceBetweenPlayerPreferences());
		prefs_.push_back(new ScoreDisplayPreferences());
		prefs_.push_back(new MainWindowDisplayPreferences());
		prefs_.push_back(new ExportPdfPreferences());
		prefs_.push_back(new ChartExportationPreferences());
		prefs_.push_back(new DirectoryPreferences());
		prefs_.push_back(new MainWindowTitleBarPreferences());
		prefs_.push_back(new VersionPreferences());

		connectSignal();

		g_debug("Preferences created");
	}


	Preferences::Preferences(const Preferences& pref)
	{
		prefs_.push_back(new MainWindowSizePreferences(pref.mainWindowSize()));
		prefs_.push_back(new DifferenceBetweenPlayerPreferences(pref.differenceBetweenPlayer()));
		prefs_.push_back(new ScoreDisplayPreferences(pref.scoreDisplay()));
		prefs_.push_back(new MainWindowDisplayPreferences(pref.mainWindowDisplay()));
		prefs_.push_back(new ExportPdfPreferences(pref.exportPdf()));
		prefs_.push_back(new ChartExportationPreferences(pref.chartExportation()));
		prefs_.push_back(new DirectoryPreferences(pref.directory()));
		prefs_.push_back(new MainWindowTitleBarPreferences(pref.mainWindowTitleBar()));
		prefs_.push_back(new VersionPreferences(pref.version()));

		connectSignal();

		g_debug("Preferences copied");
	}


	Preferences::Preferences(const ustring& filename)
	{
		DomParser	   parser;
		Node*		   root_node;
		const Element* tmp_element;

		try
		{
			parser.parse_file(filename);
		}
		catch (xmlpp::exception& e)
		{
			g_warning("%s", e.what());
			throw XmlError(ustring::compose(_("Cannot open the file %1"), filename));
		}

		root_node = parser.get_document()->get_root_node();
		if (root_node->get_name().compare(LABEL) != 0)
			throw XmlError(ustring::compose(_("This file is not a CSU preferences file, it's a %1 file."), root_node->get_name()));

		tmp_element = getChildElement(root_node, VERSION_LABEL);

		// Version
		double file_version = ustringToDouble(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());
		if (file_version > VERSION)
			throw XmlError(
					ustring::compose(_("This version of Csuper only support preferences file version less than or equal to %1"), VERSION));

		// main_window_size
		tmp_element = getChildElement(root_node, MainWindowSizePreferences::LABEL);
		prefs_.push_back(new MainWindowSizePreferences(tmp_element));

		// difference_between_player
		tmp_element = getChildElement(root_node, DifferenceBetweenPlayerPreferences::LABEL);
		prefs_.push_back(new DifferenceBetweenPlayerPreferences(tmp_element));

		// score_display
		tmp_element = getChildElement(root_node, ScoreDisplayPreferences::LABEL);
		prefs_.push_back(new ScoreDisplayPreferences(tmp_element));

		// main_window_display
		if (file_version > 1)
		{
			tmp_element = getChildElement(root_node, MainWindowDisplayPreferences::LABEL);
			prefs_.push_back(new MainWindowDisplayPreferences(tmp_element));
		}
		else
			prefs_.push_back(new MainWindowDisplayPreferences());

		// export_pdf
		tmp_element = getChildElement(root_node, ExportPdfPreferences::LABEL);
		prefs_.push_back(new ExportPdfPreferences(tmp_element, file_version));

		// chart
		tmp_element = getChildElement(root_node, ChartExportationPreferences::LABEL);
		prefs_.push_back(new ChartExportationPreferences(tmp_element));

		// directory and title bar
		if (file_version > 1)
		{
			tmp_element = getChildElement(root_node, DirectoryPreferences::LABEL);
			prefs_.push_back(new DirectoryPreferences(tmp_element));

			tmp_element = getChildElement(root_node, MainWindowTitleBarPreferences::LABEL);
			prefs_.push_back(new MainWindowTitleBarPreferences(tmp_element));

			tmp_element = getChildElement(root_node, VersionPreferences::LABEL);
			prefs_.push_back(new VersionPreferences(tmp_element));
		}
		else
		{
			prefs_.push_back(new DirectoryPreferences());
			prefs_.push_back(new MainWindowTitleBarPreferences());
			prefs_.push_back(new VersionPreferences());
		}

		connectSignal();

		g_debug("Preferences %s opened", filename.c_str());
	}


	Preferences::~Preferences()
	{
		for (auto& p : prefs_)
			delete p;

		g_debug("Preferences destroyed");
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	Preferences& Preferences::operator=(const Preferences& pref)
	{
		if (this == &pref)
			return *this;

		for (auto& p : prefs_)
			delete p;

		prefs_.push_back(new MainWindowSizePreferences(pref.mainWindowSize()));
		prefs_.push_back(new DifferenceBetweenPlayerPreferences(pref.differenceBetweenPlayer()));
		prefs_.push_back(new ScoreDisplayPreferences(pref.scoreDisplay()));
		prefs_.push_back(new MainWindowDisplayPreferences(pref.mainWindowDisplay()));
		prefs_.push_back(new ExportPdfPreferences(pref.exportPdf()));
		prefs_.push_back(new ChartExportationPreferences(pref.chartExportation()));
		prefs_.push_back(new DirectoryPreferences(pref.directory()));
		prefs_.push_back(new MainWindowTitleBarPreferences(pref.mainWindowTitleBar()));
		prefs_.push_back(new VersionPreferences(pref.version()));

		connectSignal();

		return *this;
	}

	void Preferences::connectSignal()
	{
		for (auto& p : prefs_)
			p->signalChanged().connect(mem_fun(signal_changed_, &type_signal_changed::emit));
	}

	ustring Preferences::toUstring() const
	{
		ustring str;
		for (auto& p : prefs_)
			str += p->toUstring() + "\n";
		return str;
	}

	ostream& operator<<(ostream& os, const Preferences& pref)
	{
		os << pref.toUstring() << endl;
		return os;
	}


	PreferencesPtr Preferences::get()
	{
		string filename;
		if (Portable::getPortable())
			filename = build_filename(DIRECTORY_NAME, PREFERENCES_FILENAME);
		else
			filename = build_filename(get_home_dir(), DIRECTORY_NAME, PREFERENCES_FILENAME);

		return PreferencesPtr(new Preferences(filename));
	}

	void Preferences::writeToFile() const
	{
		string filename;
		if (Portable::getPortable())
			filename = build_filename(DIRECTORY_NAME, PREFERENCES_FILENAME);
		else
			filename = build_filename(get_home_dir(), DIRECTORY_NAME, PREFERENCES_FILENAME);

		Document doc;
		Element* root = doc.create_root_node(LABEL);

		// Version
		Element* node_version = root->LIBXMLXX_ADD_CHILD_ELEMENT(VERSION_LABEL);
		node_version->add_child_text(dtostr(VERSION));

		for (auto& p : prefs_)
			p->createXmlNode(root);

		try
		{
			doc.write_to_file_formatted(filename, "UTF-8");
		}
		catch (xmlpp::exception& e)
		{
			g_warning("%s", e.what());
			throw FileError(ustring::compose(_("Error while writing the CSU preferences file %1"), filename));
		}

		g_debug("Preferences written");
	}
}	 // namespace csuper
