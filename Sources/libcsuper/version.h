/*!
 * \file    version.h
 * \brief   Header for the essential function of libcsuper
 * \author  Remi BERTHO
 * \date    16/01/18
 * \version 4.4.1
 */

/*
* version.h
*
* Copyright 2014-2018
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef VERSION_H_INCLUDED
#define VERSION_H_INCLUDED

#include <glibmm.h>
#include <giomm.h>
#include <AsynchronousExecution.h>
#include "exceptions.h"
// minor and major are macro defined in sys/types.h so I need to undef them
#undef minor
#undef major

namespace csuper
{

	/*! \class Version
	 *   \brief This class define a version of Csuper
	 */
	class Version
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		unsigned int major_ = 4; /*!< The major version */
		unsigned int minor_ = 4; /*!< The minor version */
		unsigned int micro_ = 3; /*!< The micro version */


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		Version();


		/*!
		 *  \brief Constructor with the version in string
		 *  \param version the version
		 */
		explicit Version(const Glib::ustring& version);


		/**
		 * @brief Get the last version available of Csuper on the internet
		 * @return the Version
		 * @exception csuper::Exception if internet or the website is not available
		 */
		static Version getLast();

		/**
		 * @brief Get the last version available of Csuper on the internet asynchronously
		 * @param return_function the function that will receive the version
		 * @param exception_function the function that will receive the exception
		 * @exception Glib::Exception if internet or the website is not available
		 */
		static void getLastAsynchronously(
				std::function<void(Version&)> return_function, std::function<void(csuper::Exception&)> exception_function);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Get the major version
		 *  \return the major version
		 */
		inline unsigned int major() const
		{
			return major_;
		}

		/*!
		 *  \brief Get the minor version
		 *  \return the minor version
		 */
		inline unsigned int minor() const
		{
			return minor_;
		}

		/*!
		 *  \brief Get the micro version
		 *  \return the micro version
		 */
		inline unsigned int micro() const
		{
			return micro_;
		}

		/*!
		 *  \brief Convert to a ustring
		 *  \return the ustring
		 */
		Glib::ustring toUstring() const;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Operators /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Operator ==
		 *  \param version an other version
		 *  \return true if equal, false otherwise
		 */
		bool operator==(const Version& version);

		/*!
		 *  \brief Operator !=
		 *  \param version an other version
		 *  \return true if different, false otherwise
		 */
		bool operator!=(const Version& version);

		/*!
		 *  \brief Operator >
		 *  \param version an other version
		 *  \return true if superior, false otherwise
		 */
		bool operator>(const Version& version);

		/*!
		 *  \brief Operator <
		 *  \param version an other version
		 *  \return true if inferior, false otherwise
		 */
		bool operator<(const Version& version);

		/*!
		 *  \brief Operator >=
		 *  \param version an other version
		 *  \return true if superior or equal, false otherwise
		 */
		bool operator>=(const Version& version);

		/*!
		 *  \brief Operator <=
		 *  \param version an other version
		 *  \return true if inferior or equal, false otherwise
		 */
		bool operator<=(const Version& version);
	};
}	 // namespace csuper


#endif	  // VERSION_H_INCLUDED
