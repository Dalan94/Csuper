/*!
 * \file    share.h
 * \brief   Header for the essential function of libcsuper
 * \author  Remi BERTHO
 * \date    10/02/15
 * \version 4.3.1
 */

/*
* share.h
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef SHARE_H_INCLUDED
#define SHARE_H_INCLUDED

#include <glibmm.h>
#include <libxml++/libxml++.h>


/*! \namespace csuper
 *
 * Namespace of Csuper
 */
namespace csuper
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constants /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*!< Define the directory of csuper */
	static constexpr const char* DIRECTORY_NAME = ".csuper";


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// General ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	/*!
	 *  Initialize libcsuper with gettext.
	 *  \exception Glib::exception
	 */
	void csuperInitialize(const bool portable);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Conversion ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*!
	 *  Convert a Boolean into a ustring with yes or no which can be translated
	 *  \param b
	 *  \return the ustring
	 */
	Glib::ustring boolToYesNo(const bool b);

	/*!
	 *  Convert a Boolean into a ustring
	 *  \param b
	 *  \return the ustring
	 */
	Glib::ustring boolToUstring(const bool b);

	/*!
	 *  Convert a ustring to a bool
	 *  \param str the ustring
	 *  \return the bool
	 */
	bool ustringToBool(const Glib::ustring& str);

	/*!
	 *  Convert a double into a ustring
	 *  \param d the double
	 *  \param decimals the number of decimals
	 *  \return the ustring
	 */
	Glib::ustring doubleToUstring(const double d, const int decimals = -1, const unsigned int width = 0);

	/*!
	 *  Convert a double into a ustring without locales
	 *  \return the ustring
	 */
	Glib::ustring dtostr(const double d);

	/*!
	 *  Convert a ustring into a double
	 *  \param str the ustring
	 *  \return the double
	 */
	double ustringToDouble(const Glib::ustring& str);

	/*!
	 *  Convert a int into a ustring
	 *  \param str the ustring
	 *  \return the int
	 */
	int ustringToInt(const Glib::ustring& str);

	/*!
	 *  Convert an int into a ustring
	 *  \param i the int
	 *  \return the ustring
	 */
	Glib::ustring intToUstring(const int i, const unsigned int width = 0);

	/*!
	 *  Change the decimal place of a double in a ustring
	 *  \param str the ustring
	 *  \param old_character the old character
	 *  \param new_character the new character
	 *  \return the ustring
	 */
	Glib::ustring replaceCharacterInUstring(const Glib::ustring& str, const char old_character, const char new_character);

	/*!
	 *  Change the decimal place of a double in a ustring
	 *  \param str the ustring
	 *  \param old_character the character
	 *  \param new_character the character
	 *  \return the ustring
	 */
	Glib::ustring& replaceCharacterInUstring(Glib::ustring& str, const char old_character, const char new_character);

	/*!
	 *  Remove a character in a ustring
	 *  \param str the ustring
	 *  \param character
	 *  \return the ustring
	 */
	Glib::ustring removeCharacterInUstring(const Glib::ustring& str, const char character);

	/*!
	 *  Remove a character in a ustring
	 *  \param str the ustring
	 *  \param character
	 *  \return the ustring
	 */
	Glib::ustring& removeCharacterInUstring(Glib::ustring& str, const char character);



///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// XML ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
#if LIBXMLXX_MAJOR_VERSION == 3
#define LIBXMLXX_GET_CHILD_TEXT get_first_child_text
#define LIBXMLXX_ADD_CHILD_ELEMENT add_child_element
#else
#define LIBXMLXX_GET_CHILD_TEXT get_child_text
#define LIBXMLXX_ADD_CHILD_ELEMENT add_child
#endif

	/*!
	 *  Get a child element with the certain name
	 *  \param node the xmlpp node
	 *  \param name the name of the element searched
	 *  \return the node
	 *  \exception csuper::XmlError if no child element
	 */
	const xmlpp::Element* getChildElement(const xmlpp::Node* node, Glib::ustring name);



	//
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// File //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*!
	 *  Test if the filename is valid
	 * \param filename the filename
	 * \return true if the filename is valid OK, false otherwise
	 */
	bool checkFilename(const Glib::ustring& filename);

	/*!
	 *  Test if the folder is valid
	 * \param filename the filename
	 * \return true if the filename is valid OK, false otherwise
	 */
	bool checkFolder(const Glib::ustring& folder);

	/*!
	 *  Remove a file
	 * \param filename the filename
	 * \exception csuper::FileError if bad filename or IO error
	 */
	void removeFile(const Glib::ustring& filename);

	/*!
	 *  Thrash a file
	 * \param filename the filename
	 * \exception csuper::FileError if bad filename or IO error
	 */
	void trashFile(const Glib::ustring& filename);

	/*!
	 *  Move a file
	 * \param old_filename the old filename
	 * \param new_filename the new filename
	 * \exception csuper::FileError if bad filename or IO error
	 */
	void moveFile(const Glib::ustring& old_filename, const Glib::ustring& new_filename);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Extension /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*!
	 *  Add a file extension to a filename
	 * \param filename the filename
	 * \param file_extension the file extension
	 * \return the filename with the file extension
	 */
	Glib::ustring addFileExtension(const Glib::ustring& filename, const Glib::ustring& file_extension);

	/*!
	 *  Add a file extension to a filename
	 * \param filename the filename
	 * \param file_extension the file extension
	 * \return the filename with the file extension
	 */
	Glib::ustring& addFileExtension(Glib::ustring& filename, const Glib::ustring& file_extension);

	/*!
	 *  Remove the file extension from a filename
	 * \param[in] filename the filename
	 * \return the filename without the file extension
	 */
	Glib::ustring removeFileExtension(const Glib::ustring& filename);

	/*!
	 *  Remove the file extension from a filename
	 * \param[in] filename the filename
	 * \return the filename without the file extension
	 */
	Glib::ustring& removeFileExtension(Glib::ustring& filename);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Variant functions /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	/*!
	 *  Convert a Variant to a bool if it is possible
	 * \param[in] var the variant
	 * \exception csuper::BadCast if the variant is not a bool
	 * \return the bool
	 */
	bool variantToBool(Glib::VariantBase var);

	/*!
	 *  Convert a Variant to a ustring if it is possible
	 * \param[in] var the variant
	 * \exception csuper::BadCast if the variant is not a ustring
	 * \return the ustring
	 */
	Glib::ustring variantToUstring(Glib::VariantBase var);

	/*!
	 *  Convert a Variant to a double if it is possible
	 * \param[in] var the variant
	 * \exception csuper::BadCast if the variant is not a double
	 * \return the double
	 */
	double variantToDouble(Glib::VariantBase var);

	/*!
	 *  Convert a Variant to a int64 if it is possible
	 * \param[in] var the variant
	 * \exception csuper::BadCast if the variant is not a int64
	 * \return the int64
	 */
	gint64 variantToInt64(Glib::VariantBase var);

	/*!
	 *  Convert a Variant to a int32 if it is possible
	 * \param[in] var the variant
	 * \exception csuper::BadCast if the variant is not a int32
	 * \return the int32
	 */
	gint32 variantToInt32(Glib::VariantBase var);

	/*!
	 *  Convert a Variant to a uint64 if it is possible
	 * \param[in] var the variant
	 * \exception csuper::BadCast if the variant is not a uint64
	 * \return the uint64
	 */
	guint64 variantToUint64(Glib::VariantBase var);

	/*!
	 *  Convert a Variant to a uint32 if it is possible
	 * \param[in] var the variant
	 * \exception csuper::BadCast if the variant is not a uint32
	 * \return the uint32
	 */
	guint32 variantToUint32(Glib::VariantBase var);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Class ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	/*! \class Portable
	 *   \brief This class define is we use Csuper as a portable application or not
	 */
	class Portable
	{
	private:
		static bool portable_; /*!< Define if it is portable */

	public:
		/*!
		 *  \brief Set the portable variable
		 *  \param portable
		 */
		inline static void setPortable(const bool portable)
		{
			portable_ = portable;
		}

		/*!
		 *  \brief Get the portable variable
		 *  \return the portable value
		 */
		inline static bool getPortable()
		{
			return portable_;
		}
	};


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Sort //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/*!
	 *  Compare two double
	 * \param[in] a a double
	 * \param[in] b a double
	 * \return the comparison
	 */
	inline bool compareDoubleAscending(const double a, const double b)
	{
		return a < b;
	}

	/*!
	 *  Compare two double
	 * \param[in] a a double
	 * \param[in] b a double
	 * \return the comparison
	 */
	inline bool compareDoubleDescending(const double a, const double b)
	{
		return a > b;
	}
}	 // namespace csuper

#endif
