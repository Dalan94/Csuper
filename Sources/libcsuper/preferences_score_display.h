/*!
 * \file    preferences_score_display.h
 * \author  Remi BERTHO
 * \date    11/02/16
 * \version 4.3.1
 */

/*
* preferences_score_display.h
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef SCORE_DISPLAY_H_INCLUDED
#define SCORE_DISPLAY_H_INCLUDED

#include "share.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class ScoreDisplayPreferences
	 *   \brief This class indicate what will be display in the left side of the main window
	 * This class contain:
	 *  - Display the total points in each turn
	 *  - Display the ranking in each turn
	 *  - Display the edit and delete turn in each turn
	 */
	class ScoreDisplayPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* TOTAL_POINTS_LABEL = "total_points";
		static constexpr const char* RANKING_LABEL		= "ranking";
		static constexpr const char* EDIT_SUPPR_LABEL	= "edit_suppr";

	public:
		static constexpr const char* LABEL = "score_display";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		ScoreDisplayPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param total_points
		 *  \param ranking
		 *  \param edit_suppr
		 */
		ScoreDisplayPreferences(const bool total_points, const bool ranking, const bool edit_suppr);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit ScoreDisplayPreferences(const xmlpp::Node* xml_node);




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the total_points
		 *  \param the total_points
		 */
		inline void setTotalPoints(const bool total_points)
		{
			setBool(TOTAL_POINTS_LABEL, total_points);
		}

		/*!
		 *  \brief Set the ranking
		 *  \param the ranking
		 */
		inline void setRanking(const bool ranking)
		{
			setBool(RANKING_LABEL, ranking);
		}

		/*!
		 *  \brief Set the edit_suppr
		 *  \param the edit_suppr
		 */
		inline void setEditSuppr(const bool edit_suppr)
		{
			setBool(EDIT_SUPPR_LABEL, edit_suppr);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the total_points
		 *  \return the total_points
		 */
		inline bool totalPoints() const
		{
			return getAsBool(TOTAL_POINTS_LABEL);
		}

		/*!
		 *  \brief Return the ranking
		 *  \return the ranking
		 */
		inline bool ranking() const
		{
			return getAsBool(RANKING_LABEL);
		}

		/*!
		 *  \brief Return the edit_suppr
		 *  \return the edit_suppr
		 */
		inline bool editSuppr() const
		{
			return getAsBool(EDIT_SUPPR_LABEL);
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the total_points in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring totalPointsUstring() const
		{
			return boolToUstring(totalPoints());
		}

		/*!
		 *  \brief Return the ranking in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring rankingUstring() const
		{
			return boolToUstring(ranking());
		}

		/*!
		 *  \brief Return the edit_suppr in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring editSupprUstring() const
		{
			return boolToUstring(editSuppr());
		}
	};
}	 // namespace csuper


#endif	  // SCORE_DISPLAY_H_INCLUDED
