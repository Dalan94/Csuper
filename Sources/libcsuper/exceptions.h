/*!
 * \file    exceptions.h
 * \brief   Exceptions of Csuper
 * \author  Remi BERTHO
 * \date    10/02/14
 * \version 4.3.1
 */

/*
 * exceptions.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef EXCEPTIONS_H_INCLUDED
#define EXCEPTIONS_H_INCLUDED

#include <exception>
#include <stdexcept>
#include <string>
#include <glibmm.h>

namespace csuper
{
	class Exception;

	/** Exception smart pointer */
	typedef std::shared_ptr<Exception> ExceptionPtr;


	/*! \class Exception
	 *   \brief This class represent a csuper Exception
	 */
	class Exception : public Glib::Exception
	{
	private:
		Glib::ustring msg_;

	public:
		/*!
		 *  \brief Constructor with the ustring parameter
		 *  \param msg the message
		 */
		explicit Exception(const Glib::ustring msg);

		/*!
		 *  \brief Return the message
		 *  \return msg the message
		 */
		virtual Glib::ustring what() const;

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		virtual ExceptionPtr clone() const = 0;
	};


	/**
	 * @class ExceptionCRTP
	 * @brief An excption with CRTP implementation of clone
	 */
	template <typename Derived> class ExceptionCRTP : public Exception
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit ExceptionCRTP(const Glib::ustring msg) : Exception(msg)
		{
		}

		/**
		 * @brief Clone the exception
		 * @return the clone in a shared_ptr
		 */
		virtual ExceptionPtr clone() const
		{
			return std::make_shared<Derived>(static_cast<Derived const&>(*this));
		}
	};



	/*! \class XmlError
	 *   \brief This class represent a xml error
	 */
	class XmlError : public ExceptionCRTP<XmlError>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit XmlError(const Glib::ustring& msg);
	};




	/*! \class AlreadyExist
	 *   \brief This class represent an already exist error
	 */
	class AlreadyExist : public ExceptionCRTP<AlreadyExist>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit AlreadyExist(const Glib::ustring& msg);
	};




	/*! \class NotFound
	 *   \brief This class represent a not found error
	 */
	class NotFound : public ExceptionCRTP<NotFound>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit NotFound(const Glib::ustring& msg);
	};




	/*! \class FileError
	 *   \brief This class represent a file error
	 */
	class FileError : public ExceptionCRTP<FileError>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit FileError(const Glib::ustring& msg);
	};




	/*! \class PdfError
	 *   \brief This class represent a pdf error
	 */
	class PdfError : public ExceptionCRTP<PdfError>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit PdfError(const Glib::ustring& msg);
	};




	/*! \class WrongUse
	 *   \brief This class represent a error when a function is called when it would'n be
	 */
	class WrongUse : public ExceptionCRTP<WrongUse>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit WrongUse(const Glib::ustring& msg);
	};




	/*! \class CalculatorError
	 *   \brief This class represent a error when calculating
	 */
	class CalculatorError : public ExceptionCRTP<CalculatorError>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit CalculatorError(const Glib::ustring& msg);
	};




	/*! \class OutOfRange
	 *   \brief This class represent a error when there is an out of range
	 */
	class OutOfRange : public ExceptionCRTP<OutOfRange>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit OutOfRange(const Glib::ustring& msg);
	};




	/*! \class UndoRedoError
	 *   \brief This class represent a error on the UndoRedoManager
	 */
	class UndoRedoError : public ExceptionCRTP<UndoRedoError>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit UndoRedoError(const Glib::ustring& msg);
	};




	/*! \class NotAvailable
	 *   \brief This class represent a error when something is not available
	 */
	class NotAvailable : public ExceptionCRTP<NotAvailable>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit NotAvailable(const Glib::ustring& msg);
	};




	/*! \class WrongValue
	 *   \brief This class represent a error when a value is wrong
	 */
	class WrongValue : public ExceptionCRTP<WrongValue>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit WrongValue(const Glib::ustring& msg);
	};




	/*! \class BadCast
	 *   \brief This class represent a error on a  cast occurred
	 */
	class BadCast : public ExceptionCRTP<BadCast>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit BadCast(const Glib::ustring& msg);
	};




	/*! \class InternetError
	 *   \brief This class represent a error during an internet connection
	 */
	class InternetError : public ExceptionCRTP<InternetError>
	{
	public:
		/*!
		 *  \brief Constructor with the string parameter
		 *  \param msg the string of the error
		 */
		explicit InternetError(const Glib::ustring& msg);
	};
}	 // namespace csuper


#endif	  // EXCEPTIONS_H_INCLUDED
