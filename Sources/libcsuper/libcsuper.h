/*!
 * \file    libcsuper.h
 * \brief   Inclusion of all header files of libcsuper
 * \author  Remi BERTHO
 * \date    25/05/14
 * \version 4.3.0
 */

/*
 * libcsuper.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef LIBCSUPER_H_INCLUDED
#define LIBCSUPER_H_INCLUDED

/*!
 * \def NOT_LIBCSUPER
 * Define that we don't compile libcsuper
 */
#define NOT_LIBCSUPER

#include "share.h"
#include "game_configuration.h"
#include "list_game_configuration.h"
#include "exceptions.h"
#include "player.h"
#include "game.h"
#include "calculator.h"
#include "preferences_difference_between_player.h"
#include "preferences_main_window_size.h"
#include "preferences_score_display.h"
#include "preferences_main_window_display.h"
#include "preferences_export_pdf.h"
#include "preferences_directory.h"
#include "preferences_chart_exportation.h"
#include "preferences_version.h"
#include "preferences.h"
#include "pdf_exportation.h"
#include "undo_redo_manager.h"
#include "version.h"
#include "abstract_preference.h"
#include "exception_list.h"

#endif
