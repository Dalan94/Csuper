/*!
 * \file    exception_list.cpp
 * \brief   Thread safe exception list
 * \author  Remi BERTHO
 * \date    28/08/16
 * \version 4.3.2
 */

/*
 * exception_list.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "exception_list.h"
#if defined __unix__ || defined __MINGW64__
#include <xmmintrin.h>
#endif

namespace csuper
{
	using namespace std;
	using namespace Glib;

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ExceptionList::ExceptionList()
	{
	}

	ExceptionList::~ExceptionList()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void ExceptionList::lock() const
	{
		while (lock_.test_and_set())
			_mm_pause();
	}

	void ExceptionList::unlock() const
	{
		lock_.clear();
	}

	bool ExceptionList::empty() const
	{
		bool res;

		lock();
		res = list_.empty();
		unlock();

		return res;
	}

	void ExceptionList::add(const csuper::Exception& exception)
	{
		lock();
		list_.push_back(exception.clone());
		unlock();
		g_debug("Exception added in list");
		signal_added_.emit();
	}

	ExceptionPtr ExceptionList::get()
	{
		ExceptionPtr res;

		lock();
		res = list_.front();
		list_.pop_front();
		unlock();
		g_debug("Exception removed from list");

		return res;
	}
}	 // namespace csuper
