/*!
 * \file    preferences_export_pdf.h
 * \author  Remi BERTHO
 * \date    19/12/15
 * \version 4.3.0
 */

/*
* preferences_export_pdf.h
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef PREFERENCES_EXPORT_PDF_H_INCLUDED
#define PREFERENCES_EXPORT_PDF_H_INCLUDED

#include "share.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class ExportPdfPreferences
	 *   \brief This class indicate the preferences for the pdf exportation
	 * This class contain:
	 *  - The classical font size
	 *  - The size of the pdf document
	 *  - The direction of the pdf document
	 *  - The character set of the pdf document
	 *  - The margin of the pdf document
	 *  - Indicate if we show the total points in each turn
	 *  - Indicate if we show the ranking in each turn
	 *  - Indicate if we use the pdf size for the chart
	 *  - Indicate if the font will be embedded
	 *  - The font name
	 */
	class ExportPdfPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* FONT_SIZE_LABEL		  = "font_size";
		static constexpr const char* SIZE_LABEL				  = "size";
		static constexpr const char* DIRECTION_LABEL		  = "direction";
		static constexpr const char* CHARSET_LABEL			  = "charset";
		static constexpr const char* MARGIN_LABEL			  = "margin";
		static constexpr const char* TOTAL_POINTS_LABEL		  = "total_points";
		static constexpr const char* RANKING_LABEL			  = "ranking";
		static constexpr const char* PDF_SIZE_FOR_CHART_LABEL = "pdf_size_for_chart";
		static constexpr const char* EMBEDDED_FONT_LABEL	  = "embedded_font";
		static constexpr const char* FONT_NAME_LABEL		  = "font_name";

	public:
		static constexpr const char* LABEL = "export_pdf_preferences";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Enum //////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		enum CharacterSet
		{
			UTF8		= 0,
			WINDOWS1252 = 1
		};

		enum PageSize
		{
			A3 = 2,
			A4 = 3,
			A5 = 4
		};

		enum PageDirection
		{
			PORTRAIT  = 0,
			LANDSCAPE = 1
		};


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		ExportPdfPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param font_size
		 *  \param size
		 *  \param direction
		 *  \param charset
		 *  \param margin
		 *  \param total_points
		 *  \param ranking
		 *  \param embedded_font
		 *  \param font_name
		 */
		ExportPdfPreferences(const unsigned int font_size,
				const PageSize					size,
				const PageDirection				direction,
				const CharacterSet				charset,
				const unsigned int				margin,
				const bool						total_points,
				const bool						ranking,
				const bool						pdf_size_for_chart,
				const bool						embedded_font,
				const Glib::ustring&			font_name);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \param version the version of the preferences
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		ExportPdfPreferences(const xmlpp::Node* xml_node, const double version);



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the font_size
		 *  \param the font_size
		 */
		inline void setFontSize(const gint32 font_size)
		{
			setUint32(FONT_SIZE_LABEL, font_size);
		}

		/*!
		 *  \brief Set the size
		 *  \param the size
		 */
		inline void setSize(const PageSize size)
		{
			setUint32(SIZE_LABEL, size);
		}

		/*!
		 *  \brief Set the direction
		 *  \param the direction
		 */
		inline void setDirection(const PageDirection direction)
		{
			setUint32(DIRECTION_LABEL, direction);
		}

		/*!
		 *  \brief Set the charset
		 *  \param the charset
		 */
		inline void setCharset(const CharacterSet charset)
		{
			setUint32(CHARSET_LABEL, charset);
		}

		/*!
		 *  \brief Set the margin
		 *  \param the margin
		 */
		inline void setMargin(const gint32 margin)
		{
			setUint32(MARGIN_LABEL, margin);
		}

		/*!
		 *  \brief Set the total_points
		 *  \param the total_points
		 */
		inline void setTotalPoints(const bool total_points)
		{
			setBool(TOTAL_POINTS_LABEL, total_points);
		}

		/*!
		 *  \brief Set the ranking
		 *  \param the ranking
		 */
		inline void setRanking(const bool ranking)
		{
			setBool(RANKING_LABEL, ranking);
		}

		/*!
		 *  \brief Set the pdf_size_for_chart_
		 *  \param the pdf_size_for_chart_
		 */
		inline void setPdfSizeForChart(const bool pdf_size_for_chart)
		{
			setBool(PDF_SIZE_FOR_CHART_LABEL, pdf_size_for_chart);
		}

		/*!
		 *  \brief Set the font name
		 *  \param the font name
		 */
		inline void setFontName(const Glib::ustring& font_name)
		{
			setUstring(FONT_NAME_LABEL, font_name);
		}

		/*!
		 *  \brief Set the embedded_font
		 *  \param the embedded_font
		 */
		inline void setEmbeddedFont(const bool embedded_font)
		{
			setBool(EMBEDDED_FONT_LABEL, embedded_font);
		}


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the font_size
		 *  \return the font_size
		 */
		inline gint32 fontSize() const
		{
			return getAsUint32(FONT_SIZE_LABEL);
		}

		/*!
		 *  \brief Return the size
		 *  \return the size
		 */
		inline PageSize size() const
		{
			return static_cast<PageSize>(getAsUint32(SIZE_LABEL));
		}

		/*!
		 *  \brief Return the direction
		 *  \return the direction
		 */
		inline PageDirection direction() const
		{
			return static_cast<PageDirection>(getAsUint32(DIRECTION_LABEL));
		}

		/*!
		 *  \brief Return the charset
		 *  \return the charset
		 */
		inline CharacterSet charset() const
		{
			return static_cast<CharacterSet>(getAsUint32(CHARSET_LABEL));
		}

		/*!
		 *  \brief Return the margin
		 *  \return the margin
		 */
		inline gint32 margin() const
		{
			return getAsUint32(MARGIN_LABEL);
		}

		/*!
		 *  \brief Return the total_points
		 *  \return the total_points
		 */
		inline bool totalPoints() const
		{
			return getAsBool(TOTAL_POINTS_LABEL);
		}

		/*!
		 *  \brief Return the ranking
		 *  \return the ranking
		 */
		inline bool ranking() const
		{
			return getAsBool(RANKING_LABEL);
		}

		/*!
		 *  \brief Return the pdf_size_for_chart
		 *  \return the pdf_size_for_chart
		 */
		inline bool pdfSizeForChart() const
		{
			return getAsBool(PDF_SIZE_FOR_CHART_LABEL);
		}

		/*!
		 *  \brief Return the font name
		 *  \return the font name
		 */
		inline Glib::ustring fontName() const
		{
			return getAsUstring(FONT_NAME_LABEL);
		}

		/*!
		 *  \brief Return the embedded_font
		 *  \return the embedded_font
		 */
		inline bool embeddedFont() const
		{
			return getAsBool(EMBEDDED_FONT_LABEL);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the font_size in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring fontSizeUstring() const
		{
			return intToUstring(fontSize());
		}

		/*!
		 *  \brief Return the size in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring sizeUstring() const
		{
			return intToUstring(size());
		}

		/*!
		 *  \brief Return the direction in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring directionUstring() const
		{
			return intToUstring(direction());
		}

		/*!
		 *  \brief Return the charset in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring charsetUstring() const
		{
			return intToUstring(charset());
		}

		/*!
		 *  \brief Return the margin in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring marginUstring() const
		{
			return intToUstring(margin());
		}

		/*!
		 *  \brief Return the total_oints in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring totalPointsUstring() const
		{
			return boolToUstring(totalPoints());
		}

		/*!
		 *  \brief Return the ranking in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring rankingUstring() const
		{
			return boolToUstring(ranking());
		}

		/*!
		 *  \brief Return the pdf_size_for_chart in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring pdfSizeForChartUstring() const
		{
			return boolToUstring(pdfSizeForChart());
		}

		/*!
		 *  \brief Return the embedded_font in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring embeddedFontUstring() const
		{
			return boolToUstring(embeddedFont());
		}


		/*!
		 *  \brief Return the font name in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring fontNameUstring() const
		{
			return fontName();
		}
	};
}	 // namespace csuper



#endif	  // PREFERENCES_EXPORT_PDF_H_INCLUDED
