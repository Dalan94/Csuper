/*!
 * \file    preferences_difference_between_player.h
 * \author  Remi BERTHO
 * \date    13/02/16
 * \version 4.3.1
 */

/*
 * preferences_difference_between_player.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef DIFFERENCES_BETWEEN_PLAYER_H_INCLUDED
#define DIFFERENCES_BETWEEN_PLAYER_H_INCLUDED

#include "share.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class DifferenceBetweenPlayerPreferences
	 *   \brief This class indicate which difference between player will be displayed in the ranking
	 * This class contain:
	 *  - Between two player consecutive
	 *  - Between the player and the first
	 *  - Between the player and the last one
	 */
	class DifferenceBetweenPlayerPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* CONSECUTIVE_LABEL = "consecutive";
		static constexpr const char* FIRST_LABEL	   = "first";
		static constexpr const char* LAST_LABEL		   = "last";

	public:
		static constexpr const char* LABEL = "difference_between_player";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		DifferenceBetweenPlayerPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param width
		 *  \param height
		 *  \param is_maximize
		 */
		DifferenceBetweenPlayerPreferences(const bool consecutive, const bool first, const bool last);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit DifferenceBetweenPlayerPreferences(const xmlpp::Node* xml_node);


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the consecutive
		 *  \param the consecutive
		 */
		inline void setConsecutive(const bool consecutive)
		{
			setBool(CONSECUTIVE_LABEL, consecutive);
		}

		/*!
		 *  \brief Set the first
		 *  \param the first
		 */
		inline void setFirst(const bool first)
		{
			setBool(FIRST_LABEL, first);
		}

		/*!
		 *  \brief Set the last
		 *  \param the last
		 */
		inline void setLast(const bool last)
		{
			setBool(LAST_LABEL, last);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the consecutive
		 *  \return the consecutive
		 */
		inline unsigned int consecutive() const
		{
			return getAsBool(CONSECUTIVE_LABEL);
		}

		/*!
		 *  \brief Return the first
		 *  \return the first
		 */
		inline unsigned int first() const
		{
			return getAsBool(FIRST_LABEL);
		}

		/*!
		 *  \brief Return the last
		 *  \return the last
		 */
		inline bool last() const
		{
			return getAsBool(LAST_LABEL);
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the consecutive in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring consecutiveUstring() const
		{
			return boolToUstring(consecutive());
		}

		/*!
		 *  \brief Return the first in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring firstUstring() const
		{
			return boolToUstring(first());
		}

		/*!
		 *  \brief Return the last in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring lastUstring() const
		{
			return boolToUstring(last());
		}
	};
}	 // namespace csuper


#endif	  // DIFFERENCES_BETWEEN_PLAYER_H_INCLUDED
