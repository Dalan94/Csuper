/*!
 * \file    preferences_export_pdf.cpp
 * \author  Remi BERTHO
 * \date    09/06/15
 * \version 4.3.0
 */

/*
* preferences_export_pdf.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#include "config.h"

#include "preferences_export_pdf.h"

using namespace Glib;
using namespace std;
using namespace xmlpp;

namespace csuper
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	ExportPdfPreferences::ExportPdfPreferences()
			: ExportPdfPreferences(12, A4, PORTRAIT, WINDOWS1252, 40, false, false, true, true, "Times-Roman")
	{
	}

	ExportPdfPreferences::ExportPdfPreferences(const unsigned int font_size,
			const PageSize										  size,
			const PageDirection									  direction,
			const CharacterSet									  charset,
			const unsigned int									  margin,
			const bool											  total_points,
			const bool											  ranking,
			const bool											  pdf_size_for_chart,
			const bool											  embedded_font,
			const ustring&										  font_name)
			: AbstractPreference(LABEL,
					  AbstractPreference::UINT32,
					  FONT_SIZE_LABEL,
					  AbstractPreference::UINT32,
					  SIZE_LABEL,
					  AbstractPreference::UINT32,
					  DIRECTION_LABEL,
					  AbstractPreference::UINT32,
					  CHARSET_LABEL,
					  AbstractPreference::UINT32,
					  MARGIN_LABEL,
					  AbstractPreference::BOOL,
					  TOTAL_POINTS_LABEL,
					  AbstractPreference::BOOL,
					  RANKING_LABEL,
					  AbstractPreference::BOOL,
					  PDF_SIZE_FOR_CHART_LABEL,
					  AbstractPreference::BOOL,
					  EMBEDDED_FONT_LABEL,
					  AbstractPreference::USTRING,
					  FONT_NAME_LABEL)
	{
		setFontSize(font_size);
		setSize(size);
		setDirection(direction);
		setCharset(charset);
		setMargin(margin);
		setTotalPoints(total_points);
		setRanking(ranking);
		setPdfSizeForChart(pdf_size_for_chart);
		setEmbeddedFont(embedded_font);
		setFontName(font_name);
	}

	ExportPdfPreferences::ExportPdfPreferences(const Node* xml_node, const double version)
			: AbstractPreference(xml_node,
					  AbstractPreference::UINT32,
					  FONT_SIZE_LABEL,
					  AbstractPreference::UINT32,
					  SIZE_LABEL,
					  AbstractPreference::UINT32,
					  DIRECTION_LABEL,
					  AbstractPreference::UINT32,
					  CHARSET_LABEL,
					  AbstractPreference::UINT32,
					  MARGIN_LABEL,
					  AbstractPreference::BOOL,
					  TOTAL_POINTS_LABEL,
					  AbstractPreference::BOOL,
					  RANKING_LABEL,
					  AbstractPreference::BOOL,
					  PDF_SIZE_FOR_CHART_LABEL,
					  AbstractPreference::BOOL,
					  EMBEDDED_FONT_LABEL,
					  AbstractPreference::USTRING,
					  FONT_NAME_LABEL)
	{
		if (version == 1)
		{
			setFontName("Times-Roman");
			setEmbeddedFont(true);
			setRanking(false);
			setTotalPoints(false);
		}
	}
}	 // namespace csuper
