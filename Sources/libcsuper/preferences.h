/*!
 * \file    preferences.h
 * \author  Remi BERTHO
 * \date    14/02/16
 * \version 4.3.1
 */

/*
 * preferences.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PREFERENCES_H_INCLUDED
#define PREFERENCES_H_INCLUDED

#include "preferences_difference_between_player.h"
#include "preferences_main_window_size.h"
#include "preferences_score_display.h"
#include "preferences_main_window_display.h"
#include "preferences_export_pdf.h"
#include "preferences_directory.h"
#include "preferences_chart_exportation.h"
#include "preferences_main_window_title_bar.h"
#include "preferences_version.h"

namespace csuper
{
	class Preferences;


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Smart pointers ////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	/** Preferences smart pointer */
	typedef std::shared_ptr<Preferences> PreferencesPtr;




	/*! \class Preferences
	 *   \brief This class represent all the preferences
	 * This class contain:
	 *  - The differences between player preferences
	 *  - The directory preferences
	 *  - The pdf exportation preferences
	 *  - The main window display preferences
	 *  - The main window size preferences
	 *  - The score display preferences
	 *  - The chart exportation preferences
	 *  - The main window title bar preferences
	 *  - The version preferences
	 */
	class Preferences
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const unsigned int SIZE_INDEX		= 0;
		static constexpr const unsigned int DIFF_INDEX		= 1;
		static constexpr const unsigned int SCORE_INDEX		= 2;
		static constexpr const unsigned int DISPLAY_INDEX	= 3;
		static constexpr const unsigned int PDF_INDEX		= 4;
		static constexpr const unsigned int CHART_INDEX		= 5;
		static constexpr const unsigned int DIR_INDEX		= 6;
		static constexpr const unsigned int TITLE_BAR_INDEX = 7;
		static constexpr const unsigned int VERSION_INDEX	= 8;

		static constexpr const char* VERSION_LABEL = "version";
		static constexpr const char* LABEL		   = "csu_preferences";

		/*!< The version */
		static double VERSION;

	public:
		/*!< Define the filename of the preferences file of csuper */
		static constexpr const char* PREFERENCES_FILENAME = "preferences.xml";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		std::vector<AbstractPreference*> prefs_; /*!< The preferences */


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef sigc::signal<void> type_signal_changed;

		/*!
		 *  \brief Return the signal changed
		 *  \return the signal changed
		 */
		inline type_signal_changed signalChanged()
		{
			return signal_changed_;
		}

	private:
		type_signal_changed signal_changed_; /*!< The signal when the object changed*/

		/*!
		 *  \brief Connect all the signal of the sub-preferences to the preference signal
		 */
		void connectSignal();



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		/*!
		 *  \brief Constructor from a filename
		 *  \exception csuper::XmlError if bad file
		 */
		Preferences(const Glib::ustring& filename);

	public:
		/*!
		 *  \brief Default constructor
		 */
		Preferences();

		/*!
		 *  \brief Copy constructor
		 */
		Preferences(const Preferences& pref);


		/*!
		 *  \brief Destructor
		 */
		~Preferences();




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief operator =
		 *  \param pref a Preferences
		 *  \return a reference to the object
		 */
		Preferences& operator=(const Preferences& pref);

		/*!
		 *  \brief Convert to a ustring
		 */
		Glib::ustring toUstring() const;

		/*!
		 *  \brief Operator <<
		 *  \param os the ostream
		 *  \param pref
		 *  \return the ostream
		 */
		friend std::ostream& operator<<(std::ostream& os, const Preferences& pref);

		/*!
		 *  \brief Get the preferences
		 *  \return the preferences
		 */
		static PreferencesPtr get();

		/*!
		 *  \brief Write to the file
		 *  \exception csuper::FileError
		 */
		void writeToFile() const;




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter and setter /////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief return the DifferenceBetweenPlayerPreferences
		 *  \return the DifferenceBetweenPlayerPreferences
		 */
		const DifferenceBetweenPlayerPreferences& differenceBetweenPlayer() const
		{
			return *static_cast<DifferenceBetweenPlayerPreferences*>(prefs_[DIFF_INDEX]);
		}

		/*!
		 *  \brief return the DifferenceBetweenPlayerPreferences
		 *  \return the DifferenceBetweenPlayerPreferences
		 */
		DifferenceBetweenPlayerPreferences& differenceBetweenPlayer()
		{
			return *static_cast<DifferenceBetweenPlayerPreferences*>(prefs_[DIFF_INDEX]);
		}

		/*!
		 *  \brief return the DirectoryPreferences
		 *  \return the DirectoryPreferences
		 */
		const DirectoryPreferences& directory() const
		{
			return *static_cast<DirectoryPreferences*>(prefs_[DIR_INDEX]);
		}

		/*!
		 *  \brief return the DirectoryPreferences
		 *  \return the DirectoryPreferences
		 */
		DirectoryPreferences& directory()
		{
			return *static_cast<DirectoryPreferences*>(prefs_[DIR_INDEX]);
		}

		/*!
		 *  \brief return the ExportPdfPreferences
		 *  \return the ExportPdfPreferences
		 */
		const ExportPdfPreferences& exportPdf() const
		{
			return *static_cast<ExportPdfPreferences*>(prefs_[PDF_INDEX]);
		}

		/*!
		 *  \brief return the ExportPdfPreferences
		 *  \return the ExportPdfPreferences
		 */
		ExportPdfPreferences& exportPdf()
		{
			return *static_cast<ExportPdfPreferences*>(prefs_[PDF_INDEX]);
		}

		/*!
		 *  \brief return the MainWindowDisplayPreferences
		 *  \return the MainWindowDisplayPreferences
		 */
		const MainWindowDisplayPreferences& mainWindowDisplay() const
		{
			return *static_cast<MainWindowDisplayPreferences*>(prefs_[DISPLAY_INDEX]);
		}

		/*!
		 *  \brief return the MainWindowDisplayPreferences
		 *  \return the MainWindowDisplayPreferences
		 */
		MainWindowDisplayPreferences& mainWindowDisplay()
		{
			return *static_cast<MainWindowDisplayPreferences*>(prefs_[DISPLAY_INDEX]);
		}

		/*!
		 *  \brief return the MainWindowSizePreferences
		 *  \return the MainWindowSizePreferences
		 */
		const MainWindowSizePreferences& mainWindowSize() const
		{
			return *static_cast<MainWindowSizePreferences*>(prefs_[SIZE_INDEX]);
		}

		/*!
		 *  \brief return the MainWindowSizePreferences
		 *  \return the MainWindowSizePreferences
		 */
		MainWindowSizePreferences& mainWindowSize()
		{
			return *static_cast<MainWindowSizePreferences*>(prefs_[SIZE_INDEX]);
		}

		/*!
		 *  \brief return the ScoreDisplayPreferences
		 *  \return the ScoreDisplayPreferences
		 */
		const ScoreDisplayPreferences& scoreDisplay() const
		{
			return *static_cast<ScoreDisplayPreferences*>(prefs_[SCORE_INDEX]);
		}

		/*!
		 *  \brief return the ScoreDisplayPreferences
		 *  \return the ScoreDisplayPreferences
		 */
		ScoreDisplayPreferences& scoreDisplay()
		{
			return *static_cast<ScoreDisplayPreferences*>(prefs_[SCORE_INDEX]);
		}


		/*!
		 *  \brief return the ChartExportationPreferences
		 *  \return the ChartExportationPreferences
		 */
		const ChartExportationPreferences& chartExportation() const
		{
			return *static_cast<ChartExportationPreferences*>(prefs_[CHART_INDEX]);
		}

		/*!
		 *  \brief return the ChartExportationPreferences
		 *  \return the ChartExportationPreferences
		 */
		ChartExportationPreferences& chartExportation()
		{
			return *static_cast<ChartExportationPreferences*>(prefs_[CHART_INDEX]);
		}

		/*!
		 *  \brief return the MainWindowTitleBarPreferences
		 *  \return the MainWindowTitleBarPreferences
		 */
		const MainWindowTitleBarPreferences& mainWindowTitleBar() const
		{
			return *static_cast<MainWindowTitleBarPreferences*>(prefs_[TITLE_BAR_INDEX]);
		}

		/*!
		 *  \brief return the MainWindowTitleBarPreferences
		 *  \return the MainWindowTitleBarPreferences
		 */
		MainWindowTitleBarPreferences& mainWindowTitleBar()
		{
			return *static_cast<MainWindowTitleBarPreferences*>(prefs_[TITLE_BAR_INDEX]);
		}

		/*!
		 *  \brief return the VersionPreferences
		 *  \return the VersionPreferences
		 */
		const VersionPreferences& version() const
		{
			return *static_cast<VersionPreferences*>(prefs_[VERSION_INDEX]);
		}

		/*!
		 *  \brief return the VersionPreferences
		 *  \return the VersionPreferences
		 */
		VersionPreferences& version()
		{
			return *static_cast<VersionPreferences*>(prefs_[VERSION_INDEX]);
		}
	};
}	 // namespace csuper



#endif	  // PREFERENCES_H_INCLUDED
