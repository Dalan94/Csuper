/*!
 * \file    preferences_main_window_display.h
 * \author  Remi BERTHO
 * \date    11/02/16
 * \version 4.3.1
 */

/*
 * preferences_main_window_display.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef MAIN_WINDOW_DISPLAY_H_INCLUDED
#define MAIN_WINDOW_DISPLAY_H_INCLUDED

#include "abstract_preference.h"
#include "share.h"

namespace csuper
{
	/*! \class MainWindowDisplayPreferences
	 *   \brief This class indicate what will be display in the main window
	 * This class contain:
	 *  - Display the ranking
	 *  - Display the game informations
	 */
	class MainWindowDisplayPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* GAME_INFORMATION_LABEL = "game_information";
		static constexpr const char* RANKING_LABEL			= "ranking";

	public:
		static constexpr const char* LABEL = "main_window_display";


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		MainWindowDisplayPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param calculator_
		 *  \param ranking
		 *  \param game_information
		 */
		MainWindowDisplayPreferences(const bool ranking, const bool game_information);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \param version the version of the preferences file
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit MainWindowDisplayPreferences(const xmlpp::Node* xml_node);




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the ranking
		 *  \param the ranking
		 */
		inline void setRanking(const bool ranking)
		{
			setBool(RANKING_LABEL, ranking);
		}

		/*!
		 *  \brief Set the game_information
		 *  \param the game_information
		 */
		inline void setGameInformation(const bool game_information)
		{
			setBool(GAME_INFORMATION_LABEL, game_information);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the ranking
		 *  \return the ranking
		 */
		inline bool ranking() const
		{
			return getAsBool(RANKING_LABEL);
		}

		/*!
		 *  \brief Return the game_information
		 *  \return the game_information
		 */
		inline bool gameInformation() const
		{
			return getAsBool(GAME_INFORMATION_LABEL);
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the ranking in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring rankingUstring() const
		{
			return boolToUstring(ranking());
		}

		/*!
		 *  \brief Return the game_information in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring gameInformationUstring() const
		{
			return boolToUstring(gameInformation());
		}
	};
}	 // namespace csuper


#endif	  // MAIN_WINDOW_DISPLAY_H_INCLUDED
