/*!
 * \file    game_configuration.cpp
 * \author  Remi BERTHO
 * \date    16/12/15
 * \version 4.3.0
 */

/*
* game_configuration.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#include "config.h"

#include "game_configuration.h"
#include <iostream>

using namespace std;
using namespace Glib;
using namespace xmlpp;

namespace csuper
{

	double GameConfiguration::version_(1.1);

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	GameConfiguration::GameConfiguration(const double nb_max_min,
			const bool								  use_maximum,
			const bool								  turn_based,
			const bool								  use_distributor,
			const int								  decimal_place,
			const bool								  max_winner,
			const Glib::ustring&					  name,
			const double							  initial_score,
			const unsigned int						  nb_turn_distributor)
			: nb_max_min_(nb_max_min), use_maximum_(use_maximum), turn_based_(turn_based), use_distributor_(use_distributor),
			  decimal_place_(decimal_place), max_winner_(max_winner), name_(name), initial_score_(initial_score),
			  nb_turn_distributor_(nb_turn_distributor)
	{
	}


	GameConfiguration::GameConfiguration(const Node* xml_node, const double version)
	{
		const Element* tmp_element;

		tmp_element = getChildElement(xml_node, NB_MAX_MIN_LABEL);
		nb_max_min_ = ustringToDouble(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element = getChildElement(xml_node, MAX_WINNER_LABEL);
		max_winner_ = ustringToBool(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element = getChildElement(xml_node, TURN_BY_TURN_LABEL);
		turn_based_ = ustringToBool(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element		 = getChildElement(xml_node, USE_DISTRIBUTOR_LABEL);
		use_distributor_ = ustringToBool(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element	   = getChildElement(xml_node, DECIMAL_PLACE_LABEL);
		decimal_place_ = ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element	 = getChildElement(xml_node, USE_MAXIMUM_LABEL);
		use_maximum_ = ustringToBool(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		tmp_element = getChildElement(xml_node, NAME_LABEL);
		name_		= tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content();

		tmp_element	   = getChildElement(xml_node, BEGIN_SCORE_LABEL);
		initial_score_ = ustringToDouble(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());

		if (version >= 1.1)
		{
			tmp_element			 = getChildElement(xml_node, NB_TURN_DISTRIBUTOR_LABEL);
			nb_turn_distributor_ = ustringToInt(tmp_element->LIBXMLXX_GET_CHILD_TEXT()->get_content());
		}
	}

	GameConfiguration::GameConfiguration(const GameConfiguration& gc)
			: nb_max_min_(gc.nb_max_min_), use_maximum_(gc.use_maximum_), turn_based_(gc.turn_based_),
			  use_distributor_(gc.use_distributor_), decimal_place_(gc.decimal_place_), max_winner_(gc.max_winner_), name_(gc.name_),
			  initial_score_(gc.initial_score_), nb_turn_distributor_(gc.nb_turn_distributor_)
	{
	}

	GameConfiguration::GameConfiguration()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Operators /////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	bool GameConfiguration::operator==(const GameConfiguration& game_config) const
	{
		return (nb_max_min_ == game_config.nb_max_min_) && (use_maximum_ == game_config.use_maximum_) &&
			   (turn_based_ == game_config.turn_based_) && (use_distributor_ == game_config.use_distributor_) &&
			   (decimal_place_ == game_config.decimal_place_) && (max_winner_ == game_config.max_winner_) && (name_ == game_config.name_) &&
			   (initial_score_ == game_config.initial_score_) && (nb_turn_distributor_ == game_config.nb_turn_distributor_);
	}

	ostream& operator<<(ostream& os, const GameConfiguration& game_config)
	{
		os << game_config.toUstring() << endl;
		return os;
	}

	GameConfiguration& GameConfiguration::operator=(const GameConfiguration& game_config)
	{
		nb_max_min_			 = game_config.nb_max_min_;
		use_maximum_		 = game_config.use_maximum_;
		turn_based_			 = game_config.turn_based_;
		use_distributor_	 = game_config.use_distributor_;
		decimal_place_		 = game_config.decimal_place_;
		max_winner_			 = game_config.max_winner_;
		name_				 = game_config.name_;
		initial_score_		 = game_config.initial_score_;
		nb_turn_distributor_ = game_config.nb_turn_distributor_;

		signal_changed_.emit();
		return *this;
	}


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	void GameConfiguration::createXmlNode(Element* parent_node) const
	{
		Element* node = parent_node->LIBXMLXX_ADD_CHILD_ELEMENT(LABEL);

		Element* node_nb_max = node->LIBXMLXX_ADD_CHILD_ELEMENT(NB_MAX_MIN_LABEL);
		node_nb_max->add_child_text(dtostr(nbMaxMin()));

		Element* node_max_winner = node->LIBXMLXX_ADD_CHILD_ELEMENT(MAX_WINNER_LABEL);
		node_max_winner->add_child_text(boolToUstring(max_winner_));

		Element* node_turn_by_turn = node->LIBXMLXX_ADD_CHILD_ELEMENT(TURN_BY_TURN_LABEL);
		node_turn_by_turn->add_child_text(boolToUstring(turn_based_));

		Element* node_use_distributor = node->LIBXMLXX_ADD_CHILD_ELEMENT(USE_DISTRIBUTOR_LABEL);
		node_use_distributor->add_child_text(boolToUstring(use_distributor_));

		Element* node_decimal_place = node->LIBXMLXX_ADD_CHILD_ELEMENT(DECIMAL_PLACE_LABEL);
		node_decimal_place->add_child_text(decimalPlaceUstring());

		Element* node_use_maximum = node->LIBXMLXX_ADD_CHILD_ELEMENT(USE_MAXIMUM_LABEL);
		node_use_maximum->add_child_text(boolToUstring(use_maximum_));

		Element* node_name = node->LIBXMLXX_ADD_CHILD_ELEMENT(NAME_LABEL);
		node_name->add_child_text(name_);

		Element* node_begin_score = node->LIBXMLXX_ADD_CHILD_ELEMENT(BEGIN_SCORE_LABEL);
		node_begin_score->add_child_text(dtostr(initialScore()));

		Element* node_nb_turn_distributor = node->LIBXMLXX_ADD_CHILD_ELEMENT(NB_TURN_DISTRIBUTOR_LABEL);
		node_nb_turn_distributor->add_child_text(nbTurnDistributorUstring());
	}

	ustring GameConfiguration::toUstring() const
	{
		ustring res = _("Name of the game configuration: ") + nameUstring() + "\n" + toUstringWithoutName();

		return res;
	}

	ustring GameConfiguration::toUstringWithoutName() const
	{
		ustring res = _("Use of a maximum score: ") + useMaximumUstring() + "\n" + _("Maximum/minimum number of points: ") +
					  nbMaxMinUstring() + "\n" + _("Initial score: ") + initialScoreUstring() + "\n" + _("Number of decimals displayed: ") +
					  decimalPlaceUstring() + "\n" + _("The first has the highest score: ") + maxWinnerUstring() + "\n" +
					  _("Turn-based game: ") + turnBasedUstring() + "\n" + _("Use of a distributor: ") + useDistributorUstring() + "\n" +
					  _("Number of turn per distributor: ") + nbTurnDistributorUstring();

		return res;
	}
}	 // namespace csuper
