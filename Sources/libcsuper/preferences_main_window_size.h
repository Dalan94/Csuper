/*!
 * \file    preferences_main_window_size.h
 * \author  Remi BERTHO
 * \date    11/02/16
 * \version 4.3.1
 */

/*
* preferences_main_window_size.h
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef MAIN_WINDOW_SIZE_H_INCLUDED
#define MAIN_WINDOW_SIZE_H_INCLUDED

#include "share.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class MainWindowSizePreferences
	 *   \brief This class represent a the preferences for the size of_the main window
	 * This class contain:
	 *  - The width of the main window
	 *  - he height of the main window
	 *  - Said if the main window is maximize or not
	 */
	class MainWindowSizePreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* WIDTH_LABEL	   = "width";
		static constexpr const char* HEIGHT_LABEL	   = "height";
		static constexpr const char* IS_MAXIMIZE_LABEL = "is_maximize";

	public:
		static constexpr const char* LABEL = "main_window_size";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		MainWindowSizePreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param width
		 *  \param height
		 *  \param is_maximize
		 */
		MainWindowSizePreferences(const guint32 width, const guint32 height, const bool is_maximize);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit MainWindowSizePreferences(const xmlpp::Node* xml_node);




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////
		/*!
		 *  \brief Set the width
		 *  \param the width
		 */
		inline void setWidth(const guint32 width)
		{
			setUint32(WIDTH_LABEL, width);
		}

		/*!
		 *  \brief Set the height
		 *  \param the height
		 */
		inline void setHeight(const guint32 height)
		{
			setUint32(HEIGHT_LABEL, height);
		}

		/*!
		 *  \brief Set the is_maximize
		 *  \param the is_maximize
		 */
		inline void setIsMaximize(const bool is_maximize)
		{
			setBool(IS_MAXIMIZE_LABEL, is_maximize);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		/*!
		 *  \brief return the width
		 *  \return the width
		 */
		inline guint32 width() const
		{
			return getAsUint32(WIDTH_LABEL);
		}

		/*!
		 *  \brief return the height
		 *  \return the height
		 */
		inline guint32 height() const
		{
			return getAsUint32(HEIGHT_LABEL);
		}

		/*!
		 *  \brief return the width
		 *  \return the width
		 */
		inline bool isMaximize() const
		{
			return getAsBool(IS_MAXIMIZE_LABEL);
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		/*!
		 *  \brief return the width in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring widthUstring() const
		{
			return intToUstring(width());
		}

		/*!
		 *  \brief return the height in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring heightUstring() const
		{
			return intToUstring(height());
		}

		/*!
		 *  \brief return the width in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring isMaximizeUstring() const
		{
			return boolToUstring(isMaximize());
		}
	};
}	 // namespace csuper


#endif	  // MAIN_WINDOW_SIZE_H_INCLUDED
