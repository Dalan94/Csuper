/*!
 * \file    abstract_preference.h
 * \author  Remi BERTHO
 * \date    14/02/16
 * \version 4.3.1
 */

/*
 * abstract_preference.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef ABSTRACT_PREFERENCE_H_INCLUDED
#define ABSTRACT_PREFERENCE_H_INCLUDED

#include <map>

#include "share.h"

namespace csuper
{
	/*! \class AbstractPreference
	 *   \brief This class represent an abstract preference
	 */
	class AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Enum //////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		enum Type
		{
			NONE,
			INT64,
			UINT64,
			INT32,
			UINT32,
			DOUBLE,
			USTRING,
			BOOL
		};
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Attributes ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		std::map<Glib::ustring, Glib::VariantBase> pref_;
		Glib::ustring							   name_;


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Signals ///////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		typedef sigc::signal<void> type_signal_changed;

		/*!
		 *  \brief Return the signal changed
		 *  \return the signal changed
		 */
		inline type_signal_changed signalChanged()
		{
			return signal_changed_;
		}

	private:
		type_signal_changed signal_changed_; /*!< The signal when the object changed*/

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		AbstractPreference(Glib::ustring name,
				const Type				 type1	= Type::NONE,
				const Glib::ustring		 key1	= "",
				const Type				 type2	= Type::NONE,
				const Glib::ustring		 key2	= "",
				const Type				 type3	= Type::NONE,
				const Glib::ustring		 key3	= "",
				const Type				 type4	= Type::NONE,
				const Glib::ustring		 key4	= "",
				const Type				 type5	= Type::NONE,
				const Glib::ustring		 key5	= "",
				const Type				 type6	= Type::NONE,
				const Glib::ustring		 key6	= "",
				const Type				 type7	= Type::NONE,
				const Glib::ustring		 key7	= "",
				const Type				 type8	= Type::NONE,
				const Glib::ustring		 key8	= "",
				const Type				 type9	= Type::NONE,
				const Glib::ustring		 key9	= "",
				const Type				 type10 = Type::NONE,
				const Glib::ustring		 key10	= "");

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \param version the version of the preferences
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		AbstractPreference(const xmlpp::Node* xml_node,
				const Type					  type1	 = Type::NONE,
				const Glib::ustring			  key1	 = "",
				const Type					  type2	 = Type::NONE,
				const Glib::ustring			  key2	 = "",
				const Type					  type3	 = Type::NONE,
				const Glib::ustring			  key3	 = "",
				const Type					  type4	 = Type::NONE,
				const Glib::ustring			  key4	 = "",
				const Type					  type5	 = Type::NONE,
				const Glib::ustring			  key5	 = "",
				const Type					  type6	 = Type::NONE,
				const Glib::ustring			  key6	 = "",
				const Type					  type7	 = Type::NONE,
				const Glib::ustring			  key7	 = "",
				const Type					  type8	 = Type::NONE,
				const Glib::ustring			  key8	 = "",
				const Type					  type9	 = Type::NONE,
				const Glib::ustring			  key9	 = "",
				const Type					  type10 = Type::NONE,
				const Glib::ustring			  key10	 = "");

		/**
		 * @brief Copy constructor
		 *
		 * @param ap another AbstractPreference
		 */
		AbstractPreference(const AbstractPreference& ap);

		/**
		 * @brief Destructor
		 *
		 */
		virtual ~AbstractPreference();


		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Function //////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Operator ==
		 *  \param ap another AbstractPreference
		 */
		bool operator==(const AbstractPreference& ap) const;

		/*!
		 *  \brief Operator =
		 *  \param ap another AbstractPreference
		 */
		AbstractPreference& operator=(const AbstractPreference& ap);

		/*!
		 *  \brief Operator !=
		 *  \param ap another AbstractPreference
		 */
		bool operator!=(const AbstractPreference& ap) const;

		/*!
		 *  \brief Convert to a ustring
		 *  \return the ustring
		 */
		virtual Glib::ustring toUstring() const;

		/*!
		 *  \brief Operator <<
		 *  \param os the ostream
		 *  \param ap the AbstractPreference
		 *  \return the ostream
		 */
		friend std::ostream& operator<<(std::ostream& os, const AbstractPreference& ap);

		/*!
		 *  \brief Add the AbstractPreference into a xmlpp element
		 *  \param parent_node the parent node
		 *  \exception xmlpp::internal_error if bad xmlpp node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		void createXmlNode(xmlpp::Element* parent_node) const;



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		void set(const Glib::ustring key, const Glib::VariantBase value);

	public:
		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		inline void setBool(const Glib::ustring key, const bool value)
		{
			set(key, Glib::Variant<bool>::create(value));
		}


		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		inline void setDouble(const Glib::ustring key, const double value)
		{
			set(key, Glib::Variant<double>::create(value));
		}


		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		inline void setUstring(const Glib::ustring key, const Glib::ustring value)
		{
			set(key, Glib::Variant<Glib::ustring>::create(value));
		}


		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		inline void setInt64(const Glib::ustring key, const gint64 value)
		{
			set(key, Glib::Variant<gint64>::create(value));
		}


		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		inline void setInt32(const Glib::ustring key, const gint32 value)
		{
			set(key, Glib::Variant<gint32>::create(value));
		}


		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		inline void setUint64(const Glib::ustring key, const guint64 value)
		{
			set(key, Glib::Variant<guint64>::create(value));
		}


		/*!
		 *  \brief Set a value in the preference
		 *  \param key the key
		 *  \param value the new value
		 *  \exception WrongValue if the value is wrong
		 *  \exception NotFound if the key in not found
		 */
		inline void setUint32(const Glib::ustring key, const guint32 value)
		{
			set(key, Glib::Variant<guint32>::create(value));
		}


		/*!
		 *  \brief Set the name
		 *  \param the name
		 */
		inline void setName(const Glib::ustring& name)
		{
			name_ = name;
			signal_changed_.emit();
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	protected:
		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 */
		Glib::VariantBase get(const Glib::ustring key) const;


	public:
		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 * \exception csuper::BadCast if the variant is not a bool
		 */
		inline bool getAsBool(const Glib::ustring key) const
		{
			return variantToBool(get(key));
		}

		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 * \exception csuper::BadCast if the variant is not a double
		 */
		inline double getAsDouble(const Glib::ustring key) const
		{
			return variantToDouble(get(key));
		}

		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 * \exception csuper::BadCast if the variant is not a ustring
		 */
		inline Glib::ustring getAsUstring(const Glib::ustring key) const
		{
			return variantToUstring(get(key));
		}

		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 * \exception csuper::BadCast if the variant is not a int64
		 */
		inline gint64 getAsInt64(const Glib::ustring key) const
		{
			return variantToInt64(get(key));
		}

		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 * \exception csuper::BadCast if the variant is not a int32
		 */
		inline gint32 getAsInt32(const Glib::ustring key) const
		{
			return variantToInt32(get(key));
		}

		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 * \exception csuper::BadCast if the variant is not a uint64
		 */
		inline guint64 getAsUint64(const Glib::ustring key) const
		{
			return variantToUint64(get(key));
		}

		/*!
		 *  \brief Return a value in the preference
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 * \exception csuper::BadCast if the variant is not a uint32
		 */
		inline guint32 getAsUint32(const Glib::ustring key) const
		{
			return variantToUint32(get(key));
		}

		/*!
		 *  \brief Return a value in the preference in a ustring
		 *  \param key the key
		 *  \return the value
		 *  \exception NotFound if the key in not found
		 */
		Glib::ustring getUstring(const Glib::ustring key) const;

		/*!
		 *  \brief Return the name
		 *  \return the name
		 */
		inline Glib::ustring name() const
		{
			return name_;
		}
	};
}	 // namespace csuper


#endif	  // ABSTRACT_PREFERENCE_H_INCLUDED
