/*!
 * \file    preferences_chart_exportation.h
 * \author  Remi BERTHO
 * \date    13/02/16
 * \version 4.3.1
 */

/*
 * preferences_chart_exportation.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of LibCsuper.
 *
 * LibCsuper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibCsuper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CHART_EXPORTATION_H_INCLUDED
#define CHART_EXPORTATION_H_INCLUDED

#include "share.h"
#include "abstract_preference.h"

namespace csuper
{
	/*! \class ChartExportationPreferences
	 *   \brief This class indicate the chart exportation preferences
	 * This class contain:
	 *  - The width of the chart
	 *  - The height of the chart
	 *  - Total points or points
	 */
	class ChartExportationPreferences : public AbstractPreference
	{
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constants /////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	private:
		static constexpr const char* WIDTH_LABEL		= "width";
		static constexpr const char* HEIGHT_LABEL		= "height";
		static constexpr const char* TOTAL_POINTS_LABEL = "total_points";

	public:
		static constexpr const char* LABEL = "chart_export_preferences";

		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Constructor ///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Default constructor
		 */
		ChartExportationPreferences();

		/*!
		 *  \brief Constructor with all intern component
		 *  \param width
		 *  \param height
		 *  \param total_points
		 */
		ChartExportationPreferences(const int width, const int height, const bool total_points);

		/*!
		 *  \brief Constructor with a xmlpp node
		 *  \param xml_node the xml node
		 *  \exception csuper::XmlError if bad xmlpp node
		 */
		explicit ChartExportationPreferences(const xmlpp::Node* xml_node);



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Setter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Set the width
		 *  \param the width
		 */
		inline void setWidth(const guint32 width)
		{
			setUint32(WIDTH_LABEL, width);
		}

		/*!
		 *  \brief Set the height
		 *  \param the height
		 */
		inline void setHeight(const guint32 height)
		{
			setUint32(HEIGHT_LABEL, height);
		}

		/*!
		 *  \brief Set the total_points
		 *  \param the total_points
		 */
		inline void setTotalPoints(const bool total_points)
		{
			setBool(TOTAL_POINTS_LABEL, total_points);
		}




		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the width
		 *  \return the width
		 */
		inline guint32 width() const
		{
			return getAsUint32(WIDTH_LABEL);
		}

		/*!
		 *  \brief Return the height
		 *  \return the height
		 */
		inline guint32 height() const
		{
			return getAsUint32(HEIGHT_LABEL);
		}

		/*!
		 *  \brief Return the total_points
		 *  \return the total_points
		 */
		inline bool totalPoints() const
		{
			return getAsBool(TOTAL_POINTS_LABEL);
		}



		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////// Getter ustring ////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
	public:
		/*!
		 *  \brief Return the width in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring widthUstring() const
		{
			return intToUstring(width());
		}

		/*!
		 *  \brief Return the height in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring heightUstring() const
		{
			return intToUstring(height());
		}

		/*!
		 *  \brief Return the total_points in a ustring
		 *  \return the ustring
		 */
		inline Glib::ustring totalPointsUstring() const
		{
			return boolToUstring(totalPoints());
		}
	};
}	 // namespace csuper


#endif	  // CHART_EXPORTATION_H_INCLUDED
