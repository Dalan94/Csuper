/*!
 * \file    preferences_score_display.cpp
 * \author  Remi BERTHO
 * \date    13/02/16
 * \version 4.3.1
 */

/*
* preferences_score_display.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/


#include "config.h"

#include "preferences_directory.h"

using namespace Glib;
using namespace std;
using namespace xmlpp;

namespace csuper
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	DirectoryPreferences::DirectoryPreferences() : DirectoryPreferences(".")
	{
		if (!Portable::getPortable())
			setOpen(get_home_dir());
	}

	DirectoryPreferences::DirectoryPreferences(const ustring& open) : AbstractPreference(LABEL, AbstractPreference::USTRING, OPEN_LABEL)
	{
		setOpen(open);
	}

	DirectoryPreferences::DirectoryPreferences(const Node* xml_node) : AbstractPreference(xml_node, AbstractPreference::USTRING, OPEN_LABEL)
	{
	}
}	 // namespace csuper
