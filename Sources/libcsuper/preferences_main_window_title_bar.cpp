/*!
 * \file    preferences_main_window_title_bar.cpp
 * \author  Remi BERTHO
 * \date    11/02/16
 * \version 4.3.1
 */

/*
* preferences_main_window_title_bar.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#include "config.h"

#include "preferences_main_window_title_bar.h"

using namespace Glib;
using namespace xmlpp;
using namespace std;

namespace csuper
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	MainWindowTitleBarPreferences::MainWindowTitleBarPreferences() : MainWindowTitleBarPreferences(false, false)
	{
#if defined G_OS_UNIX
		// Look for current desktop
		ustring current_desktop				  = "";
		ustring known_desktops_headerbar_ok[] = {"gnome", "unity", "mate", "kde", "xcfe", "lxde", "cinnamon"};

		current_desktop = getenv("XDG_CURRENT_DESKTOP");
		if (current_desktop.empty())
		{
			current_desktop = getenv("XDG_SESSION_DESKTOP");
		}

		if (!current_desktop.empty())
		{
			current_desktop = current_desktop.lowercase();

			for (auto& desktop : known_desktops_headerbar_ok)
			{
				if (current_desktop.compare(desktop))
				{
					setDisableWindowManagerDecoration(true);
					setPrintTitle(true);
					break;
				}
			}
		}

#elif defined G_OS_WIN32
		setDisableWindowManagerDecoration(true);
		setPrintTitle(true);
#endif
	}

	MainWindowTitleBarPreferences::MainWindowTitleBarPreferences(const bool disable_window_manager_decoration, const bool print_title)
			: AbstractPreference(
					  LABEL, AbstractPreference::BOOL, DISABLE_WINDOW_MANAGER_DECORATION_LABEL, AbstractPreference::BOOL, PRINT_TITLE_LABEL)
	{
		setDisableWindowManagerDecoration(disable_window_manager_decoration);
		setPrintTitle(print_title);
	}

	MainWindowTitleBarPreferences::MainWindowTitleBarPreferences(const Node* xml_node)
			: AbstractPreference(xml_node,
					  AbstractPreference::BOOL,
					  DISABLE_WINDOW_MANAGER_DECORATION_LABEL,
					  AbstractPreference::BOOL,
					  PRINT_TITLE_LABEL)
	{
	}
}	 // namespace csuper
