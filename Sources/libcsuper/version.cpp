/*!
 * \file    version.cpp
 * \brief   Header for the essential function of libcsuper
 * \author  Remi BERTHO
 * \date    26/08/15
 * \version 4.3.0
 */

/*
* version.cpp
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of LibCsuper.
*
* LibCsuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* LibCsuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#include "config.h"

#include "version.h"
#include "share.h"

namespace csuper
{
	using namespace Glib;
	using namespace std;
	using namespace AsynchronousExecution;

	//
	// Constructor
	//
	Version::Version()
	{
	}

	Version::Version(const ustring& version)
	{
		stringstream ss(version);
		char		 tmp_1, tmp_2;
		ss >> major_ >> tmp_1 >> minor_ >> tmp_2 >> micro_;
	}


	Version Version::getLast()
	{
		char* data;

		try
		{
			gsize			  length;
			RefPtr<Gio::File> file = Gio::File::create_for_uri("https://www.binaries.dalan.fr/Csuper/latest_stable/version.txt");
			file->load_contents(data, length);
		}
		catch (Glib::Exception& ex)
		{
			throw InternetError(_("Cannot access to the latest version file on the internet."));
		}

		return Version(data);
	}

	void Version::getLastAsynchronously(
			std::function<void(Version&)> return_function, std::function<void(csuper::Exception&)> exception_function)
	{
		execFunction<Version, csuper::Exception>(getLast, return_function, exception_function);
	}

	//
	// Function
	//
	ustring Version::toUstring() const
	{
		return intToUstring(major()) + "." + intToUstring(minor()) + "." + intToUstring(micro());
	}


	//
	// operator
	//
	bool Version::operator==(const Version& version)
	{
		return major() == version.major() && minor() == version.minor() && micro() == version.micro();
	}


	bool Version::operator!=(const Version& version)
	{
		return major() != version.major() || minor() != version.minor() || micro() != version.micro();
	}


	bool Version::operator>(const Version& version)
	{
		return (major() > version.major()) || (major() == version.major() && minor() > version.minor()) ||
			   (major() == version.major() && minor() == version.minor() && micro() > version.micro());
	}


	bool Version::operator<(const Version& version)
	{
		return (major() < version.major()) || (major() == version.major() && minor() < version.minor()) ||
			   (major() == version.major() && minor() == version.minor() && micro() < version.micro());
	}


	bool Version::operator>=(const Version& version)
	{
		return (*this == version || *this > version);
	}


	bool Version::operator<=(const Version& version)
	{
		return (*this == version || *this < version);
	}
}	 // namespace csuper
