/*!
 * \file    csu_application.cpp
 * \author  Remi BERTHO
 * \date    01/07/17
 * \version 4.3.2
 */

/*
 * csu_application.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include <iostream>

#include "csu_application.h"

#include "main_window.h"
#include "about.h"
#include "game_configuration_window.h"
#include "new_game_configuration_dialog.h"
#include "import_export_game_configuration_dialog.h"
#include "menu_file.h"
#include "menu_edit.h"
#include "menu_display.h"
#include "preferences_window.h"
#include "new_file_assistant.h"
#include "file_properties_dialog.h"
#include "game_over_dialog.h"
#include "statistics_grid.h"
#include "ranking_view.h"
#include "game_information_view.h"
#include "change_distributor_dialog.h"
#include "points_view.h"
#include "delete_turn_dialog.h"
#include "edit_turn_dialog.h"
#include "players_names_view.h"
#include "display_dialog.h"
#include "chart_total_points_grid.h"
#include "chart_points_grid.h"

using namespace Gtk;
using namespace Glib;
using namespace Gio;
using namespace std;
using namespace csuper;
using namespace sigc;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
CsuApplication::CsuApplication(int& argc, char**& argv)
		: Gtk::Application(argc, argv, "fr.dalan.csuper-gtk", APPLICATION_HANDLES_OPEN | APPLICATION_NON_UNIQUE)
{
	signal_startup().connect(mem_fun(*this, &CsuApplication::onStartup));

	// Initialization of csuper
	try
	{
#ifdef PORTABLE
		csuperInitialize(true);
#else
		csuperInitialize(false);
#endif	  // PORTABLE
	}
	catch (std::exception& e)
	{
		g_error("%s", e.what());
		exit(EXIT_FAILURE);
	}
	catch (Glib::Exception& e)
	{
		g_error("%s", e.what().c_str());
		exit(EXIT_FAILURE);
	}

	// Initialize Csuper
	try
	{
		pref_			  = csuper::Preferences::get();
		list_game_config_ = ListGameConfiguration::getMainList();
	}
	catch (Glib::Exception& e)
	{
		MessageDialog* error =
				new MessageDialog(_("Fatal error during initialization, please check your preferences an game configurations files."),
						false,
						MESSAGE_ERROR,
						BUTTONS_OK,
						true);
		error->run();
		error->hide();
		delete error;

		g_error("%s", e.what().c_str());
		exit(-1);
	}


	undoRedoManager().signalUndo().connect(mem_fun(*this, &CsuApplication::updateGame));
	undoRedoManager().signalRedo().connect(mem_fun(*this, &CsuApplication::updateGame));

	exception_list_.connectSignalAdded(mem_fun(*this, &CsuApplication::checkExceptionList));

	signal_check_for_update_.connect(mem_fun(*this, &CsuApplication::checkForUpdateSlot));

	signal_handle_local_options().connect(mem_fun(*this, &CsuApplication::localOption));

	// Check GDK backend
	string gdk_backend = Glib::getenv("GDK_BACKEND");
	if (gdk_backend == "broadway")
	{
		broadway_backend_ = true;
		g_info("Brodaway backend enable");
	}
	else
		broadway_backend_ = false;
}

CsuApplication::~CsuApplication()
{
	delete import_export_game_config_dialog_;
	delete new_file_assistant_;
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void CsuApplication::init(RefPtr<Builder>& builder)
{
	builder_ = builder;
	CsuWidget::setApp(this);

	main_accel_group_ = RefPtr<AccelGroup>::cast_dynamic(builder->get_object("main_accelgroup"));

	// builder_->get_widget_derived("chart_dialog", chart_);
	builder_->get_widget_derived("main_window_points_scrolled_window", points_view_);
	builder_->get_widget_derived("main_window_players_names_scrolled_window", players_names_);
	builder_->get_widget_derived("delete_turn_dialog", delete_turn_);
	builder_->get_widget_derived("edit_turn_dialog", edit_turn_);
	builder_->get_widget_derived("change_distributor_dialog", change_distributor_dialog_);
	builder_->get_widget_derived("main_window_side_ranking_box", rank_);
	builder_->get_widget_derived("main_window_side_game_information_box", game_info_);
	builder_->get_widget_derived("display_dialog", display_dialog_);
	builder_->get_widget_derived("menu_file", menu_file_);
	builder_->get_widget_derived("menu_edit", menu_edit_);
	builder_->get_widget_derived("menu_display", menu_display_);
	builder_->get_widget_derived("about_dialog", about_);
	builder_->get_widget_derived("game_configuration_window", game_config_window_);
	builder_->get_widget_derived("new_game_configuration_dialog", new_game_config_dialog_);
	builder_->get_widget_derived("preferences_window", pref_window_);
	new_file_assistant_ = new NewFileAssistant(builder);
	builder_->get_widget_derived("preferences_exportation_scrolledwindow", export_pref_window_);
	builder_->get_widget_derived("preferences_display_scrolledwindow", display_pref_window_);
	builder_->get_widget_derived("preferences_html_scrolledwindow", html_pref_window_);
	builder_->get_widget_derived("main_window", main_window_);
	builder_->get_widget_derived("file_properties_dialog", file_properties_dialog_);
	builder_->get_widget_derived("game_over_dialog", game_over_dialog_);
	builder_->get_widget_derived("display_dialog_statistics_main_grid", stat_grid_);
	builder_->get_widget_derived("display_dialog_total_points_chart_main_grid", chart_total_points_);
	builder_->get_widget_derived("display_dialog_points_chart_main_grid", chart_points_);
	import_export_game_config_dialog_ = new ImportExportGameConfigurationDialog();

	g_debug("Init done");
}


int CsuApplication::localOption(const RefPtr<VariantDict>& options)
{
	return -1;
}

void CsuApplication::checkExceptionList()
{
	while (!exceptionList().empty())
	{
		ExceptionPtr ex = exceptionList().get();

		MessageDialog* error = new MessageDialog(*mainWindow(), ex->what(), false, MESSAGE_ERROR, BUTTONS_OK, true);
		error->run();
		error->hide();
		delete error;
	}
}

int CsuApplication::launch()
{
	int res = this->run(*main_window_);
	menuEdit()->stopUpdate();
	mainWindow()->saveSize();
	return res;
}

void CsuApplication::onStartup()
{
	// Load the menu
	if (pref()->mainWindowTitleBar().disableWindowManagerDecoration())
	{
		add_action("about", mem_fun(*about_, &About::launch));
		add_action("quit", mem_fun(*this, &CsuApplication::onQuit));
		add_action("game_config", mem_fun(*game_config_window_, &GameConfigurationWindow::launch));
		add_action("preferences", mem_fun(*pref_window_, &PreferencesWindow::launch));
		add_action("update", bind<-1, const bool>(mem_fun(*this, &CsuApplication::checkForUpdate), false));
		set_accel_for_action("app.about", "<primary>a");
		set_accel_for_action("app.quit", "<primary>q");
		set_accel_for_action("app.game_config", "<primary>g");
		set_accel_for_action("app.preferences", "<primary>p");
		set_accel_for_action("app.update", "<primary>u");

		RefPtr<Gio::Menu> menu = Gio::Menu::create();
		menu->append(_("Preferences"), "app.preferences");
		menu->append(_("Game configuration"), "app.game_config");
		menu->append(_("About"), "app.about");
		menu->append(_("Check for update"), "app.update");
		menu->append(_("Quit"), "app.quit");
		set_app_menu(RefPtr<Gio::MenuModel>::cast_static(menu));
	}

	checkForUpdate(true);

#ifndef G_OS_WIN32
	if (broadwayBackend())
	{
		// Get local IP
		auto				ip_resolver	 = Resolver::get_default();
		auto				ip_addresses = ip_resolver->lookup_by_name(g_get_host_name());
		RefPtr<InetAddress> local_ip;
		for (const auto& ip : ip_addresses)
		{
			if (ip->get_family() == SocketFamily::SOCKET_FAMILY_IPV4)
			{
				local_ip = ip;
				break;
			}
		}

		auto notification = Notification::create(_("Opened"));
		notification->set_body(ustring::compose(_("Csuper-HTML is available at http://%1:52721"), local_ip->to_string()));
		notification->set_priority(NOTIFICATION_PRIORITY_NORMAL);
		send_notification("open", notification);
	}
#endif	  // G_OS_WIN32
}

void CsuApplication::on_open(const type_vec_files& files, const ustring& hint)
{
	RefPtr<File> file = files[0];
	if (file->query_exists())
	{
		try
		{
			ustring filename = filename_to_utf8(file->get_path());
			GamePtr tmp_game = Game::create(file);
			setGame(tmp_game);
			setFilename(filename);
		}
		catch (Glib::Exception& e)
		{
			g_info("%s", e.what().c_str());
			MessageDialog* error = new MessageDialog(*mainWindow(), e.what(), false, MESSAGE_ERROR, BUTTONS_OK, true);
			error->run();
			error->hide();
			delete error;
		}
	}
	activate();
}

void CsuApplication::onQuit()
{
	bool quit_csuper = true;

	if (broadwayBackend())
	{
		int result;

		MessageDialog* quitDialog = new MessageDialog(*mainWindow(),
				_("Do you really want to quit Csuper HTML ?\nYou will need to restart Csuper-HTML before running it again."),
				false,
				MESSAGE_QUESTION,
				BUTTONS_YES_NO,
				true);
		result					  = quitDialog->run();
		quitDialog->hide();
		delete quitDialog;

		if (result == RESPONSE_NO)
			quit_csuper = false;
	}

	if (quit_csuper)
	{
#ifndef G_OS_WIN32
		if (broadwayBackend())
		{
			withdraw_notification("open");

			auto notification = Notification::create(_("Closed"));
			notification->set_body(_("Csuper-HTML is closed"));
			notification->set_priority(NOTIFICATION_PRIORITY_NORMAL);
			send_notification("close", notification);
		}
#endif

		quit();
		about_->hide();
		new_game_config_dialog_->hide();
	}
}

void CsuApplication::checkForUpdate(const bool auto_check)
{
	Version::getLastAsynchronously(
			[=](Version& version)
			{
				if (auto_check && (version <= pref()->version().lastCheckVersion()))
					return;

				last_version_ = version;
				signal_check_for_update_.emit();
			},
			[=](csuper::Exception& e)
			{
				g_info("%s", e.what().c_str());
				if (!auto_check)
					this->exceptionList().add(e);
			});
}

void CsuApplication::checkForUpdateSlot()
{
	ustring msg;
	if (last_version_ > Version())
	{
		msg = ustring::compose(_("A update is available: you use the version %1 of Csuper whereas the version %2 is available.\n"
								 "You can download the new version on this website: https://www.dalan.fr/"),
				Version().toUstring(),
				last_version_.toUstring());
	}
	else
	{
		msg = ustring::compose(_("You use the version %1 of Csuper which is the latest version."), Version().toUstring());
	}

	MessageDialog* dial = new MessageDialog(*(mainWindow()), msg, false, MESSAGE_INFO, BUTTONS_OK, true);
	dial->run();
	dial->hide();
	delete dial;

	pref()->version().setLastCheckVersion(last_version_);
	pref()->writeToFile();
}

void CsuApplication::updateGame(csuper::GamePtr game)
{
	if (!game)
		undoRedoManager().add(game_);
	else
		game_ = game;

	this->game()->writeToFileAsync(filename(),
			nullptr,
			[=](auto& e)
			{
				g_info("%s", e.what().c_str());
				this->exceptionList().add(e);
			});

	if (game_->exceedMaxNumber())
	{
		gameOverDialog()->launch();
		mainWindow()->setButtonEndOfTurnSensitive(false);
	}
	else
		mainWindow()->setButtonEndOfTurnSensitive(true);

	pointsView()->update();
	ranking()->update();
	gameInformation()->updateDistributor();
	gameInformation()->updatePoints(0);
}


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Setter and getter /////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void CsuApplication::setGame(csuper::GamePtr game)
{
	if (!game_)
	{
		menuFile()->setSensitive();
		menuDisplay()->setSensitive();
	}
	if (game->exceedMaxNumber())
		mainWindow()->setButtonEndOfTurnSensitive(false);
	else
		mainWindow()->setButtonEndOfTurnSensitive(true);

	game_ = game;

	undoRedoManager().clear();
	undoRedoManager().add(game);
	pointsView()->update();
	ranking()->update();
	gameInformation()->updateDistributor();
	gameInformation()->updatePoints(0);
	game_->signalChanged().connect(bind<-1, GamePtr>(mem_fun(*this, &CsuApplication::updateGame), nullptr));
}

void CsuApplication::setFilename(const ustring& filename)
{
	filename_ = filename;
	mainWindow()->setFilename();

	pref()->directory().setOpen(path_get_dirname(filename));
	pref()->writeToFile();
}
