/*!
 * \file    delete_turn_dialog.cpp
 * \author  Remi BERTHO
 * \date    06/10/15
 * \version 4.3.0
 */

/*
 * delete_turn_dialog.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include "delete_turn_dialog.h"

#include "csu_application.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;


//
// Constructor and destructor
//
DeleteTurnDialog::DeleteTurnDialog(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), Dialog(cobject)
{
	refGlade->get_widget("delete_turn_validate_button", ok_button_);
	refGlade->get_widget("delete_turn_cancel_button", cancel_button_);
	refGlade->get_widget("delete_turn_label", title_);
	refGlade->get_widget("delete_turn_grid", main_grid_);
	refGlade->get_widget("delete_turn_scrolledwindow", window_);
	refGlade->get_widget("delete_turn_viewport", viewport_);

	ok_button_->set_image_from_icon_name("gtk-ok", ICON_SIZE_BUTTON);
	cancel_button_->set_image_from_icon_name("gtk-cancel", ICON_SIZE_BUTTON);
}

//
// Function
//
bool DeleteTurnDialog::launch(unsigned int turn)
{
	bool res = false;

	Grid* grid = new Grid();
	grid->set_column_spacing(5);
	grid->set_row_spacing(5);
	grid->set_margin_end(10);
	grid->set_margin_start(10);
	grid->set_margin_top(10);
	grid->set_margin_bottom(10);


	unsigned int nb = 0;
	for (unsigned int i = 0; i < app()->game()->nbPlayer(); i++)
	{
		if (app()->game()->hasTurn(i, turn))
		{
			CheckButton* tmp_button = manage(new CheckButton(app()->game()->playerName(i)));
			grid->attach(*tmp_button, 0, nb, 1, 1);
			nb++;
		}
	}

	viewport_->add(*grid);

	show_all();
	switch (run())
	{
	case RESPONSE_ACCEPT:
		res = true;
		unsigned int i;
		for (i = 0; i < nb; i++)
		{
			CheckButton* tmp_check_button = static_cast<CheckButton*>(grid->get_child_at(0, i));
			if (tmp_check_button->get_active())
				app()->game()->deleteTurn(turn, tmp_check_button->get_label());
		}
		break;
	case RESPONSE_CANCEL:
		break;
	default:
		break;
	}
	hide();

	delete grid;

	return res;
}
