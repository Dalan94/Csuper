/*!
 * \file    chart_total_points_widget.cpp
 * \author  Remi BERTHO
 * \date    04/12/15
 * \version 4.3.0
 */

/*
 * chart_total_points_widget.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include "chart_total_points_grid.h"

#include "csu_application.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;
using namespace std;

//
// Constructor and destructor
//
ChartTotalPointsGrid::ChartTotalPointsGrid(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : ChartGrid(cobject, refGlade)
{
	refGlade->get_widget("display_dialog_total_points_chart_grid", chart_grid_);
	refGlade->get_widget("display_dialog_total_points_chart_x_scrollbar", x_scrollbar_);
	refGlade->get_widget("display_dialog_total_points_chart_y_scrollbar", y_scrollbar_);
	refGlade->get_widget("display_dialog_total_points_chart_x_scale", x_scale_);
	refGlade->get_widget("display_dialog_total_points_chart_y_scale", y_scale_);
	refGlade->get_widget("display_dialog_total_points_chart_drawingarea", chart_);

	init();
}

ChartTotalPointsGrid::~ChartTotalPointsGrid()
{
}

void ChartTotalPointsGrid::update()
{
	ChartGrid::update(true);
}
