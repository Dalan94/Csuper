/*!
 * \file    players_names_view.h
 * \author  Remi BERTHO
 * \date    26/11/15
 * \version 4.3.0
 */

/*
 * players_names_view.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "players_names_view.h"

#include <glibmm/i18n.h>

#include "csu_application.h"
#include "points_view.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;
using namespace std;

//
// Constructor and destructor
//
PlayersNamesView::PlayersNamesView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), ScrolledWindow(cobject)
{
	refGlade->get_widget("main_window_players_names_viewport", viewport_);

	RefPtr<Adjustment> adjust = get_hadjustment();
	adjust->signal_value_changed().connect(sigc::mem_fun(*this, &PlayersNamesView::scrollChanged));
}


//
// Function
//
void PlayersNamesView::update()
{
	if (app()->game() == nullptr)
		return;

	if (main_grid_ != nullptr)
		delete main_grid_;

	unsigned int nb_player = app()->game()->nbPlayer();

	// Set the grid
	main_grid_ = manage(new Grid());
	main_grid_->set_column_spacing(5);
	main_grid_->set_row_spacing(5);
	main_grid_->set_margin_end(10);
	main_grid_->set_margin_start(10);
	main_grid_->set_margin_top(10);
	main_grid_->set_margin_bottom(10);
	main_grid_->set_valign(ALIGN_START);

	// Add separator
	for (unsigned int i = 0; i <= nb_player; i++)
	{
		main_grid_->attach(*manage(new Separator(ORIENTATION_VERTICAL)), 6 * i + 1, 0, 1, 1);
	}

	// Write the names of the players and set the expand
	Label* legend = manage(new Label(_("Name")));
	main_grid_->attach(*legend, 0, 0, 1, 1);
	main_grid_->get_child_at(0, 0)->set_hexpand(true);
	for (unsigned int i = 0; i < nb_player; i++)
	{
		Label* tmp_label;
		tmp_label = manage(new Label());
		tmp_label->set_markup("<span size=\"large\">" + app()->game()->playerNameUstring(i) + "</span>");
		tmp_label->set_hexpand(true);
		main_grid_->attach(*tmp_label, 6 * i + 2, 0, 5, 1);
	}

	if (app()->pref()->scoreDisplay().editSuppr())
	{
		main_grid_->attach(*(new Label()), 6 * nb_player + 2, 0, 1, 1);
	}

	main_grid_->show_all();
	viewport_->add(*main_grid_);
}

void PlayersNamesView::updateSize()
{
	if (app()->game() == nullptr)
		return;

	unsigned int nb_player = app()->game()->nbPlayer();
	Widget*		 tmp_widget;

	tmp_widget = main_grid_->get_child_at(0, 0);
	tmp_widget->set_size_request(app()->pointsView()->getLegendSize());

	for (unsigned int i = 0; i < nb_player; i++)
	{
		tmp_widget = main_grid_->get_child_at(6 * i + 2, 0);
		tmp_widget->set_size_request(app()->pointsView()->getPlayerSize(i));
	}

	if (app()->pref()->scoreDisplay().editSuppr())
	{
		tmp_widget = main_grid_->get_child_at(6 * nb_player + 2, 0);
		tmp_widget->set_size_request(app()->pointsView()->getEditSupprSize());
	}
}

void PlayersNamesView::setScroll(const double scroll_value)
{
	RefPtr<Adjustment> adjust = get_hadjustment();
	adjust->set_value(scroll_value);
}


void PlayersNamesView::scrollChanged()
{
	RefPtr<Adjustment> adjust = get_hadjustment();
	app()->pointsView()->setScroll(adjust->get_value());
}
