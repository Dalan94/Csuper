/*!
 * \file    chart_widget.h
 * \author  Remi BERTHO
 * \date    04/12/15
 * \version 4.3.0
 */

/*
 * chart_widget.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CHART_GRID_H_INCLUDED
#define CHART_GRID_H_INCLUDED

#include <gtkmm.h>
#include <slope.h>

#include "csu_widget.h"

/*! \class ChartWidget
 *   \brief This class represent the chart widget
 */
class ChartGrid : public CsuWidget, public Gtk::Grid
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	Gtk::Grid*		  chart_grid_;			   /*!< The chart grid */
	Gtk::Scrollbar*	  x_scrollbar_;			   /*!< The x scrollbar */
	Gtk::Scrollbar*	  y_scrollbar_;			   /*!< The y scrollbar */
	Gtk::Scale*		  x_scale_;				   /*!< The x scale */
	Gtk::Scale*		  y_scale_;				   /*!< The y scale */
	Gtk::DrawingArea* chart_;				   /*!< The chart */
	Gtk::Grid*		  players_grid_ = nullptr; /*!< The player grid */

	bool		 total_points_;			 /*!< True if we draw total points, false otherwise */
	SlopeFigure* slope_figure = nullptr; /*!< The slope figure */
	double*		 slope_turn	  = nullptr; /*!< The turns used by slope */
	double**	 slope_points = nullptr; /*!< The points used by slope */
	SlopeItem**	 slope_items  = nullptr; /*!< The slope items */
	SlopeScale*	 slope_scale  = nullptr; /*!< The slope scale */

	std::vector<Gtk::CheckButton*> check_buttons_; /*!< The check buttons */


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	ChartGrid(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);

	virtual ~ChartGrid();

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/*!
	 *  \brief Init the objects
	 */
	void init();

	/*!
	 *  \brief Called when the zoom change
	 */
	void changeZoom();

	/*!
	 *  \brief Called when the scrollbar change
	 */
	void changeScrollbar();

	/*!
	 *  \brief Called when the players selected change
	 */
	void changePlayers();

	/*!
	 *  \brief Draw the chart
	 */
	bool drawChart(const Cairo::RefPtr<Cairo::Context>& cr);

	/*!
	 *  \brief Free all slope object
	 */
	void freeSlope();

	/*!
	 *  \brief Show the widget properties
	 *  \param total_points set to true if we draw total points, false otherwise
	 */
	void update(const bool total_points);
};

#endif	  // CHART_GRID_H_INCLUDED
