/*!
 * \file    points_view.h
 * \author  Remi BERTHO
 * \date    18/12/15
 * \version 4.3.0
 */

/*
 * points_view.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef POINTS_VIEW_H_INCLUDED
#define POINTS_VIEW_H_INCLUDED

#include <gtkmm.h>

#include "csu_widget.h"
#include "libcsuper/libcsuper.h"

/*! \class PointsView
 *   \brief This class represent the points view
 */
class PointsView : public CsuWidget, public Gtk::ScrolledWindow
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	Gtk::Viewport* viewport_;			 /*!< The viewport */
	Gtk::Grid*	   main_grid_ = nullptr; /*!< The main grid */

	std::vector<Gtk::SpinButton*> new_points_; /*!< The new points spin button */

	csuper::Calculator calc_; /*!< The calculator */


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	PointsView(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	/*!
	 *  \brief Call when the points in a game changed
	 */
	void pointsChanged();


	/*!
	 *  \brief Calculate the new spin button value
	 *  \param new_value the new value
	 *  \param index the index of the spin button
	 *  \return true for a successful conversion, false if the input was not handled,
	 and Gtk::INPUT_ERROR if the conversion failed.
	 */
	int calculateSpinbuttonValue(double* new_value, const unsigned int index);


	/*!
	 *  \brief Delete a turn
	 *  \param turn the turn
	 */
	void deleteTurn(const unsigned int turn);


	/*!
	 *  \brief Edit a turn
	 *  \param turn the turn
	 */
	void editTurn(const unsigned int turn);


	/*!
	 *  \brief Set the names view
	 *  \param cr the cairo context, no used
	 *  \return false
	 */
	bool setNames(const Cairo::RefPtr<Cairo::Context>& cr);


	/*!
	 *  \brief Call when the scroll changed
	 */
	void scrollChanged();



public:
	/*!
	 *  \brief Update the points
	 */
	void update();


	/*!
	 *  \brief Set the points of a player
	 *  \param index_player the index of the player
	 *  \param points the points
	 */
	void setPoints(const unsigned int index_player, const double points);


	/*!
	 *  \brief Retreive the new points of all players
	 *  \return the vector of the new points
	 */
	std::vector<double> getNewPoints();


	/*!
	 *  \brief Scroll down
	 */
	void scrollDown();


	//
	// Get size
	//
	/*!
	 *  \brief Get the legend size
	 *  \return the legend size
	 */
	int getLegendSize();

	/*!
	 *  \brief Get the player size
	 *  \return the player size
	 */
	int getPlayerSize(unsigned int player);

	/*!
	 *  \brief Get the edit suppr size
	 *  \return the edit suppr size
	 */
	int getEditSupprSize();


	/*!
	 *  \brief Update the points
	 *  \param scroll_value the new scroll value
	 */
	void setScroll(const double scroll_value);
};

#endif	  // POINTS_VIEW_H_INCLUDED
