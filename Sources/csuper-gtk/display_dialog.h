/*!
 * \file    display_dialog.h
 * \author  Remi BERTHO
 * \date    27/08/15
 * \version 4.3.0
 */

/*
 * display_dialog.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef DISPLAY_DIALOG_H_INCLUDED
#define DISPLAY_DIALOG_H_INCLUDED

#include <gtkmm.h>

#include "csu_widget.h"

/*! \class DisplayDialog
 *   \brief This class represent the display dialog
 */
class DisplayDialog : public CsuWidget, public Gtk::Dialog
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Enum //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	enum Type
	{
		Statistics		 = 0,
		TotalPointsChart = 1,
		PointsChart		 = 2
	};

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	Gtk::Button*   ok_button_; /*!< The OK button */
	Gtk::Notebook* notebook_;  /*!< The scrolled window */

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	//
	// Constructor and Destructor
	//
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	DisplayDialog(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Show the dialog properties
	 */
	void launch(const Type type);
};


#endif	  // DISPLAY_DIALOG_H_INCLUDED
