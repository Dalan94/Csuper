/*!
 * \file    save_dialog.cpp
 * \author  Remi BERTHO
 * \date    12/01/16
 * \version 4.3.0
 */

/*
 * save_dialog.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "save_dialog.h"
#include <glibmm/i18n.h>

#include "csu_application.h"

using namespace Gtk;
using namespace Glib;
using namespace Gio;
using namespace Gdk;
using namespace std;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
SaveDialog::SaveDialog(Window& parent_window, const std::vector<Glib::ustring>& file_filter, const bool export_dialog)
{
	set_transient_for(parent_window);
	set_gravity(GRAVITY_CENTER);
	set_position(WIN_POS_CENTER_ON_PARENT);
	set_modal(true);
	set_type_hint(WINDOW_TYPE_HINT_DIALOG);
	resize(500, 100);

	if (export_dialog)
		set_title(_("Export file"));
	else
		set_title(_("Save file"));

	cancel_button_ = add_button(_("Cancel"), RESPONSE_CANCEL);
	cancel_button_->set_image_from_icon_name("gtk-cancel", ICON_SIZE_BUTTON);

	ok_button_ = add_button(_("Save"), RESPONSE_ACCEPT);
	ok_button_->set_image_from_icon_name("gtk-save", ICON_SIZE_BUTTON);

	get_content_area()->add(*(new Label(app()->pref()->directory().open())));

	main_grid_ = manage(new Grid());
	main_grid_->set_hexpand(true);
	main_grid_->set_vexpand(true);
	scrolled_window_ = manage(new ScrolledWindow());
	get_content_area()->add(*scrolled_window_);
	scrolled_window_->add(*main_grid_);
	scrolled_window_->set_min_content_height(100);
	scrolled_window_->set_min_content_width(200);

	filename_entry_ = manage(new Entry());
	filename_entry_->set_hexpand(true);
	filename_entry_->set_vexpand(true);
	main_grid_->attach(*filename_entry_, 0, 0, 1, 1);

	combobox_ = manage(new ComboBoxText());
	for (auto& text : file_filter)
		combobox_->append(text);
	combobox_->set_active(0);
	combobox_->set_valign(ALIGN_CENTER);
	main_grid_->attach(*combobox_, 1, 0, 1, 1);
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
ustring SaveDialog::launch()
{
	ustring result;


	show_all();
	switch (run())
	{
	case RESPONSE_ACCEPT:
		result		 = build_filename(app()->pref()->directory().open(), filename_entry_->get_text());
		file_filter_ = combobox_->get_active_row_number();
		break;
	case RESPONSE_CANCEL:
		break;
	default:
		break;
	}
	hide();

	return result;
}
