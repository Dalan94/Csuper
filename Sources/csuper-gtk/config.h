/*!
 * \file    config.h
 * \author  Remi BERTHO
 * \date    01/06/17
 * \version 4.3.1
 */

/*
* config.h
*
* Copyright 2014-2017
 Remi BERTHO <remi.bertho@dalan.fr>
*
* This file is part of Csuper-gtk.
*
* Csuper-gtk is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper-gtk is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
#include "config.h"
*/

#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#define G_LOG_DOMAIN "csuper-gtk"
#define G_LOG_USE_STRUCTURED


#endif	  // CONFIG_H_INCLUDED
