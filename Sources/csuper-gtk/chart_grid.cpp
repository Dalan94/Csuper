/*!
 * \file    chart_widget.cpp
 * \author  Remi BERTHO
 * \date    04/12/15
 * \version 4.3.0
 */

/*
 * chart_widget.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include "chart_grid.h"

#include "csu_application.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;
using namespace std;

//
// Constructor and destructor
//
ChartGrid::ChartGrid(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), Grid(cobject)
{
	/*refGlade->get_widget("chart_widget_grid", chart_grid_);
	refGlade->get_widget("chart_widget_x_scrollbar", x_scrollbar_);
	refGlade->get_widget("chart_widget_y_scrollbar", y_scrollbar_);
	refGlade->get_widget("chart_widget_x_scale", x_scale_);
	refGlade->get_widget("widget_chart_scale_y", y_scale_);
	refGlade->get_widget("chart_widget_drawingarea", chart_);*/
}

ChartGrid::~ChartGrid()
{
	freeSlope();
}

//
// Function
//
void ChartGrid::init()
{
	// Signals
	/*x_scrollbar_->signal_value_changed().connect(mem_fun(*this, &ChartGrid::changeScrollbar));
	y_scrollbar_->signal_value_changed().connect(mem_fun(*this, &ChartGrid::changeScrollbar));

	x_scale_->signal_value_changed().connect(mem_fun(*this, &ChartGrid::changeZoom));
	y_scale_->signal_value_changed().connect(mem_fun(*this, &ChartGrid::changeZoom));*/

	chart_->signal_draw().connect(mem_fun(*this, &ChartGrid::drawChart));
}

void ChartGrid::freeSlope()
{
	if (slope_turn != nullptr)
	{
		delete[] slope_turn;
		slope_turn = nullptr;
	}

	if (slope_points != nullptr)
	{
		for (unsigned int i = 0; i < app()->game()->nbPlayer(); i++)
			delete[] slope_points[i];
		delete[] slope_points;
		slope_points = nullptr;
	}
	if (slope_figure != nullptr)
	{
		// slope_figure_destroy(slope_figure);
		slope_figure = nullptr;
	}
	if (slope_items != nullptr)
	{
		free(slope_items);
		slope_items = nullptr;
	}
	if (slope_scale != nullptr)
	{
		// slope_scale_destroy(slope_scale);
		slope_figure = nullptr;
	}

	check_buttons_.clear();
}

void ChartGrid::update(const bool total_points)
{
	total_points_ = total_points;

	if (players_grid_ != nullptr)
	{
		freeSlope();
		delete players_grid_;
	}
	players_grid_ = manage(new Grid());

	// Chart name
	ustring title = path_get_basename(app()->filename());
	removeFileExtension(title);
	if (total_points_)
		title = _("Total points on ") + title;
	else
		title = _("Points on ") + title;


	// Create the data
	SlopeColor	 color = SLOPE_BLACK;
	unsigned int i, j;

	slope_figure = slope_figure_new();

	slope_turn	 = new double[app()->game()->maxNbTurn() + 1];
	slope_points = new double*[app()->game()->nbPlayer()];
	slope_items	 = (SlopeItem**)malloc(sizeof(SlopeItem*) * app()->game()->nbPlayer());

	slope_scale		= slope_xyscale_new_axis(_("Turns"), _("Points"), title.c_str());
	SlopeItem* axis = slope_xyscale_get_axis(SLOPE_XYSCALE(slope_scale), SLOPE_XYSCALE_AXIS_BOTTOM);
	slope_xyaxis_set_components(SLOPE_XYAXIS(axis), SLOPE_XYAXIS_DEFAULT_DOWN_GRID);
	axis = slope_xyscale_get_axis(SLOPE_XYSCALE(slope_scale), SLOPE_XYSCALE_AXIS_LEFT);
	slope_xyaxis_set_components(SLOPE_XYAXIS(axis), SLOPE_XYAXIS_DEFAULT_DOWN_GRID);
	axis = slope_xyscale_get_axis(SLOPE_XYSCALE(slope_scale), SLOPE_XYSCALE_AXIS_TOP);
	slope_xyaxis_set_components(SLOPE_XYAXIS(axis), SLOPE_XYAXIS_LINE | SLOPE_XYAXIS_TITLE);

	for (i = 0; i <= app()->game()->maxNbTurn(); i++)
		slope_turn[i] = i;

	// Create the slope chart
	for (i = 0; i < app()->game()->nbPlayer(); i++)
	{
		slope_points[i] = new double[app()->game()->nbTurn(i) + 1];
		if (!(total_points_))
		{
			for (j = 0; j <= app()->game()->nbTurn(i); j++)
				slope_points[i][j] = app()->game()->points(i, j);
		}
		else
		{
			for (j = 0; j <= app()->game()->nbTurn(i); j++)
				slope_points[i][j] = app()->game()->totalPoints(i, j);
		}
		slope_items[i] = slope_xyseries_new_filled(app()->game()->playerName(i).c_str(),
				slope_turn,
				slope_points[i],
				app()->game()->nbTurn(i) + 1,
				color,
				SLOPE_WHITE,
				static_cast<SlopeXySeriesMode>(SLOPE_SERIES_CIRCLES | SLOPE_SERIES_LINE));
		slope_scale_add_item(slope_scale, slope_items[i]);

		// Change the color
		switch (color)
		{
		case SLOPE_BLACK:
			color = SLOPE_RED;
			break;
		case SLOPE_RED:
			color = SLOPE_BLUE;
			break;
		case SLOPE_BLUE:
			color = SLOPE_GREEN;
			break;
		case SLOPE_GREEN:
			color = SLOPE_BLUEVIOLET;
			break;
		case SLOPE_BLUEVIOLET:
			color = SLOPE_HOTPINK;
			break;
		case SLOPE_HOTPINK:
			color = SLOPE_TEAL;
			break;
		case SLOPE_TEAL:
			color = SLOPE_YELLOW;
			break;
		case SLOPE_YELLOW:
			color = SLOPE_DARKMAGENTA;
			break;
		case SLOPE_DARKMAGENTA:
			color = SLOPE_MAROON;
			break;
		case SLOPE_MAROON:
			color = SLOPE_CYAN;
			break;
		case SLOPE_CYAN:
			color = SLOPE_BLACK;
			break;
		}
	}

	slope_figure_add_scale(slope_figure, slope_scale);
	SlopeItem* legend = slope_figure_get_legend(SLOPE_FIGURE(slope_figure));
	slope_legend_set_default_position(SLOPE_LEGEND(legend), SLOPE_LEGEND_BOTTOMRIGHT);


	// Players grid
	players_grid_->set_column_spacing(5);
	players_grid_->set_row_spacing(5);
	players_grid_->set_margin_end(10);
	players_grid_->set_margin_start(10);
	players_grid_->set_margin_top(10);
	players_grid_->set_margin_bottom(10);

	for (i = 0; i < app()->game()->nbPlayer(); i++)
	{
		CheckButton* tmp_button = manage(new CheckButton(app()->game()->playerName(i)));
		tmp_button->set_active(true);
		tmp_button->set_hexpand(true);
		tmp_button->set_halign(ALIGN_CENTER);
		tmp_button->signal_toggled().connect(mem_fun(*this, &ChartGrid::changePlayers));
		players_grid_->attach(*tmp_button, i % 5, i / 5, 1, 1);
		check_buttons_.push_back(tmp_button);
	}

	attach(*players_grid_, 0, 1, 1, 1);
	players_grid_->show_all();

	/*x_scale_->hide();
	y_scale_->hide();*/
}

void ChartGrid::changePlayers()
{
	for (unsigned int i = 0; i < app()->game()->nbPlayer(); i++)
		slope_item_set_is_visible(slope_items[i], check_buttons_[i]->get_active());

	chart_->queue_draw();
}

void ChartGrid::changeZoom()
{
	changeScrollbar();
}

void ChartGrid::changeScrollbar()
{
	// Set the scrollbar
	/*double zoom_x = x_scale_->get_value();
	double zoom_y = y_scale_->get_value();

	if (zoom_x == 1)
		x_scrollbar_->hide();
	else
		x_scrollbar_->show();

	x_scrollbar_->set_range(1,zoom_x+1);

	if (zoom_y == 1)
		y_scrollbar_->hide();
	else
		y_scrollbar_->show();

	y_scrollbar_->set_range(1,zoom_y+1);


	// Zoom
	double chart_width = chart_->get_allocated_width();
	double chart_height = chart_->get_allocated_height();
	double scroll_x = 0;
	if (zoom_x != 1)
		scroll_x = (x_scrollbar_->get_value()-1)/(zoom_x-1);
	double scroll_y = 0;
	if (zoom_y != 1)
		scroll_y = (y_scrollbar_->get_value()-1)/(zoom_y-1);

	double x1, y1, y2, x2;

	x1 = (chart_width - chart_width/zoom_x)*scroll_x;
	y1 = (chart_height - chart_height/zoom_y)*scroll_y;
	x2 = x1 + chart_width/zoom_x;
	y2 = y1 + chart_height/zoom_y;

	slope_figure_update(slope_chart);
	slope_figure_track_region(slope_chart,x1,y1,x2,y2);*/

	chart_->queue_draw();
}

bool ChartGrid::drawChart(const Cairo::RefPtr<Cairo::Context>& cr)
{
	// Draw
	double chart_width, chart_height;
	chart_width	 = chart_->get_allocated_width();
	chart_height = chart_->get_allocated_height();

	SlopeRect rect;
	rect.x		= 0.0;
	rect.y		= 0.0;
	rect.width	= (double)chart_width;
	rect.height = (double)chart_height;

	slope_figure_draw(slope_figure, &rect, cr->cobj());


	chart_->show();

	return true;
}
