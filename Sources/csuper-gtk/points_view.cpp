/*!
 * \file    points_view.h
 * \author  Remi BERTHO
 * \date    18/12/15
 * \version 4.3.0
 */

/*
 * points_view.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "points_view.h"

#include <glibmm/i18n.h>

#include "csu_application.h"
#include "game_information_view.h"
#include "players_names_view.h"
#include "delete_turn_dialog.h"
#include "edit_turn_dialog.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;
using namespace std;


///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
PointsView::PointsView(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), ScrolledWindow(cobject)
{
	refGlade->get_widget("main_window_points_viewport", viewport_);
	signal_draw().connect(sigc::mem_fun(*this, &PointsView::setNames));
	RefPtr<Adjustment> adjust = get_hadjustment();

	adjust->signal_value_changed().connect(sigc::mem_fun(*this, &PointsView::scrollChanged));

	app()->pref()->scoreDisplay().signalChanged().connect(mem_fun(*this, &PointsView::update));
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void PointsView::update()
{
	if (app()->game() == nullptr)
		return;

	if (main_grid_ != nullptr)
		delete main_grid_;


	unsigned int i;
	Label*		 tmp_label;


	// Set the grid
	main_grid_ = manage(new Grid());
	main_grid_->set_column_spacing(5);
	main_grid_->set_row_spacing(5);
	main_grid_->set_margin_end(10);
	main_grid_->set_margin_start(10);
	main_grid_->set_margin_top(10);
	main_grid_->set_margin_bottom(10);
	main_grid_->set_valign(ALIGN_START);


	int			 points_grid_width;
	bool		 ranking	  = app()->pref()->scoreDisplay().ranking();
	bool		 total_points = app()->pref()->scoreDisplay().totalPoints();
	bool		 edit_suppr	  = app()->pref()->scoreDisplay().editSuppr();
	unsigned int nb_player	  = app()->game()->nbPlayer();
	unsigned int max_nb_turn  = app()->game()->maxNbTurn() + 1;

	if (ranking || total_points)
		points_grid_width = 1;
	else
		points_grid_width = 5;


	// Add separator
	for (i = 0; i < nb_player + 3; i++)
	{
		if (i <= nb_player)
			main_grid_->attach(*manage(new Separator(ORIENTATION_VERTICAL)), 6 * i + 1, 1, 1, 2 * (max_nb_turn + 7));
		else if (edit_suppr)
			main_grid_->attach(*manage(new Separator(ORIENTATION_VERTICAL)), 6 * i + 1, 5, 1, 2 * (max_nb_turn - 1) + 1);
	}
	for (i = 0; i < max_nb_turn + 6; i++)
	{
		if (i < max_nb_turn + 1 && edit_suppr && i > 0 && max_nb_turn != 1)
			main_grid_->attach(*manage(new Separator(ORIENTATION_HORIZONTAL)), 0, 2 * i + 3, 6 * (nb_player + 2) + 2, 1);
		else
			main_grid_->attach(*manage(new Separator(ORIENTATION_HORIZONTAL)), 0, 2 * i + 3, 6 * nb_player + 2, 1);
	}



	// Set the legend
	// main_grid_->attach(*manage(new Separator(ORIENTATION_HORIZONTAL)),0,1,6*nb_player+2,1);
	main_grid_->attach(*(manage(new Label(_("Legend")))), 0, 2, 1, 1);
	for (i = 0; i < nb_player; i++)
	{
		tmp_label = manage(new Label(_("Points")));
		tmp_label->set_hexpand(true);
		main_grid_->attach(*tmp_label, 6 * i + 2, 2, points_grid_width, 1);
	}
	if (total_points)
	{
		for (i = 0; i < nb_player; i++)
		{
			main_grid_->attach(*manage(new Separator(ORIENTATION_VERTICAL)), 6 * i + 3, 1, 1, 2 * (max_nb_turn + 1) + 1);
			tmp_label = manage(new Label(_("Total")));
			tmp_label->set_hexpand(true);
			main_grid_->attach(*tmp_label, 6 * i + 4, 2, 1, 1);
		}
	}
	if (ranking)
	{
		for (i = 0; i < nb_player; i++)
		{
			main_grid_->attach(*manage(new Separator(ORIENTATION_VERTICAL)), 6 * i + 5, 1, 1, 2 * (max_nb_turn + 1) + 1);
			tmp_label = manage(new Label(_("Ranking")));
			tmp_label->set_hexpand(true);
			main_grid_->attach(*tmp_label, 6 * i + 6, 2, 1, 1);
		}
	}



	// Set the button delete and edit
	if (edit_suppr)
	{
		for (i = 1; i < max_nb_turn; i++)
		{
			Button* tmp_edit_button;
			Button* tmp_delete_button;

			tmp_edit_button = manage(new Button());
			tmp_edit_button->set_image_from_icon_name("gtk-edit");
			tmp_edit_button->set_tooltip_text(_("Edit the turn"));
			tmp_delete_button = manage(new Button());
			tmp_delete_button->set_image_from_icon_name("edit-delete");
			tmp_delete_button->set_tooltip_text(_("Delete the turn"));
			main_grid_->attach(*tmp_edit_button, 6 * nb_player + 2, 2 * (i + 1) + 2, 5, 1);
			main_grid_->attach(*tmp_delete_button, 6 * (nb_player + 1) + 2, 2 * (i + 1) + 2, 5, 1);
			tmp_delete_button->signal_clicked().connect(bind<-1, const unsigned int>(mem_fun(*this, &PointsView::deleteTurn), i));
			tmp_edit_button->signal_clicked().connect(bind<-1, const unsigned int>(mem_fun(*this, &PointsView::editTurn), i));
		}
	}


	// Write all the points, the total point and the ranking of the players
	for (i = 0; i < max_nb_turn; i++)
	{
		main_grid_->attach(*(manage(new Label(ustring::compose(_("Turn %1"), i)))), 0, 2 * (i + 1) + 2, 1, 1);
		for (unsigned int k = 0; k < nb_player; k++)
		{
			if (app()->game()->hasTurn(k, i))
			{
				main_grid_->attach(
						*(manage(new Label(app()->game()->pointsUstring(k, i)))), 6 * k + 2, 2 * (i + 1) + 2, points_grid_width, 1);

				if (total_points)
				{
					main_grid_->attach(*(manage(new Label(app()->game()->totalPointsUstring(k, i)))), 6 * k + 4, 2 * (i + 1) + 2, 1, 1);
				}
				if (ranking && app()->game()->config().turnBased())
				{
					main_grid_->attach(*(manage(new Label(app()->game()->rankingUstring(k, i)))), 6 * k + 6, 2 * (i + 1) + 2, 1, 1);
				}
			}
		}
	}


	// Spin button
	main_grid_->attach(*(manage(new Label(_("New points")))), 0, 2 * (max_nb_turn + 1) + 2, 1, 1);
	new_points_.clear();
	for (i = 0; i < nb_player; i++)
	{
		auto		adju			  = Adjustment::create(0, -G_MAXDOUBLE, G_MAXDOUBLE, 1, 0, 0);
		SpinButton* new_points_button = manage(new SpinButton(adju, 1, app()->game()->config().decimalPlace()));
		new_points_button->set_alignment(0.5);
		new_points_button->set_width_chars(3);
		if (!app()->game()->config().turnBased())
			new_points_button->set_activates_default(true);
		if (app()->game()->exceedMaxNumber())
			new_points_button->set_editable(false);
		main_grid_->attach(*new_points_button, 6 * i + 2, 2 * (max_nb_turn + 1) + 2, 5, 1);
		new_points_button->signal_value_changed().connect(mem_fun(*this, &PointsView::pointsChanged));
		new_points_button->signal_input().connect(bind<-1, const unsigned int>(mem_fun(*this, &PointsView::calculateSpinbuttonValue), i));
		new_points_.push_back(new_points_button);
	}


	// Blank line
	main_grid_->attach(*(manage(new Label(""))), 0, 2 * (max_nb_turn + 2) + 2, 1, 1);


	// Names
	main_grid_->attach(*(manage(new Label(_("Names")))), 0, 2 * (max_nb_turn + 3) + 2, 1, 1);
	for (i = 0; i < nb_player; i++)
	{
		tmp_label = manage(new Label());
		tmp_label->set_markup("<span size=\"large\">" + app()->game()->playerNameUstring(i) + "</span>");
		tmp_label->set_hexpand(true);
		main_grid_->attach(*tmp_label, 6 * i + 2, 2 * (max_nb_turn + 3) + 2, 5, 1);
	}


	// Total points
	main_grid_->attach(*(manage(new Label(_("Total points")))), 0, 2 * (max_nb_turn + 4) + 2, 1, 1);
	for (i = 0; i < nb_player; i++)
		main_grid_->attach(*(manage(new Label(app()->game()->totalPointsUstring(i)))), 6 * i + 2, 2 * (max_nb_turn + 4) + 2, 5, 1);


	// Ranking
	main_grid_->attach(*(manage(new Label(_("Ranking")))), 0, 2 * (max_nb_turn + 5) + 2, 1, 1);
	for (i = 0; i < nb_player; i++)
		main_grid_->attach(*(manage(new Label(app()->game()->rankingUstring(i)))), 6 * i + 2, 2 * (max_nb_turn + 5) + 2, 5, 1);

	main_grid_->show_all();
	viewport_->add(*main_grid_);

	if (app()->playersNamesView() != nullptr)
		app()->playersNamesView()->update();
}


void PointsView::setPoints(const unsigned int index_player, const double points)
{
	new_points_[index_player]->set_value(points);
}

void PointsView::pointsChanged()
{
	double total = 0;
	for (auto& it : new_points_)
		total += it->get_value();
	app()->gameInformation()->updatePoints(total);
}

int PointsView::calculateSpinbuttonValue(double* new_value, const unsigned int index)
{
	calc_.changeExpression(new_points_[index]->get_text());
	try
	{
		*new_value = calc_.calculate();
		return true;
	}
	catch (Glib::Exception& e)
	{
		g_debug("%s", e.what().c_str());
		return false;
	}
}

void PointsView::deleteTurn(const unsigned int turn)
{
	if (app()->game()->config().turnBased())
	{
		app()->game()->deleteTurn(turn);
	}
	else
	{
		app()->deleteTurnDialog()->launch(turn);
	}
}

void PointsView::editTurn(const unsigned int turn)
{
	app()->editTurnDialog()->launch(turn);
}

vector<double> PointsView::getNewPoints()
{
	vector<double> new_points;

	for (auto& it : new_points_)
		new_points.push_back(it->get_value());

	return new_points;
}

void PointsView::scrollDown()
{
	RefPtr<Adjustment> vertical_adjust = get_vadjustment();
	double			   new_upper	   = vertical_adjust->get_upper() + vertical_adjust->get_step_increment();
	vertical_adjust->set_upper(new_upper);
	vertical_adjust->set_value(new_upper - vertical_adjust->get_page_size());
}

int PointsView::getLegendSize()
{
	return main_grid_->get_child_at(0, 2)->get_allocated_width();
}

int PointsView::getPlayerSize(unsigned int player)
{
	unsigned int max_nb_turn = app()->game()->maxNbTurn() + 1;
	return main_grid_->get_child_at(6 * player + 2, 2 * (max_nb_turn + 4) + 2)->get_allocated_width();
}

int PointsView::getEditSupprSize()
{
	static int result = 0;
	if (result == 0 && app()->game()->maxNbTurn() > 0)
	{
		unsigned int nb_player = app()->game()->nbPlayer();

		Widget* tmp_widget;
		tmp_widget = main_grid_->get_child_at(6 * (nb_player + 1) + 2, 6);
		result += tmp_widget->get_allocated_width();
		tmp_widget = main_grid_->get_child_at(6 * nb_player + 2, 6);
		result += tmp_widget->get_allocated_width();
		tmp_widget = main_grid_->get_child_at(6 * nb_player + 1, 6);
		result += 2 * tmp_widget->get_allocated_width();
		result += 3 * main_grid_->get_column_spacing();
	}
	return result;
}


bool PointsView::setNames(const Cairo::RefPtr<Cairo::Context>& cr)
{
	if (app()->playersNamesView() != nullptr)
		app()->playersNamesView()->updateSize();
	return false;
}

void PointsView::scrollChanged()
{
	RefPtr<Adjustment> adjust = get_hadjustment();
	app()->playersNamesView()->setScroll(adjust->get_value());
}

void PointsView::setScroll(const double scroll_value)
{
	RefPtr<Adjustment> adjust = get_hadjustment();
	adjust->set_value(scroll_value);
}
