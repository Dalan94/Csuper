/*!
 * \file    csu_application.h
 * \author  Remi BERTHO
 * \date    30/08/16
 * \version 4.3.2
 */

/*
 * csu_application.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CSU_APPLICATION_H_INCLUDED
#define CSU_APPLICATION_H_INCLUDED

#include <gtkmm.h>

class MainWindow;
class About;
class GameConfigurationWindow;
class NewGameConfigurationDialog;
class ImportExportGameConfigurationDialog;
class MenuDisplay;
class MenuEdit;
class MenuFile;
class PreferencesWindow;
class ExportationPreferences;
class DisplayPreferences;
class HtmlPreferences;
class NewFileAssistant;
class FilePropertiesDialog;
class GameOverDialog;
class StatisticsGrid;
class RankingView;
class GameInformationView;
class ChangeDistributorDialog;
class PointsView;
class DeleteTurnDialog;
class EditTurnDialog;
class PlayersNamesView;
class ChartTotalPointsGrid;
class ChartPointsGrid;
class DisplayDialog;

#include "libcsuper/libcsuper.h"


/*! \class CsuApplication
 *   \brief This class represent the Application
 */
class CsuApplication : public Gtk::Application
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Glib::RefPtr<Gtk::Builder>	  builder_;			 /*!< The Builder */
	Glib::RefPtr<Gtk::AccelGroup> main_accel_group_; /*!< The main accel group */

	csuper::PreferencesPtr			 pref_;				 /*!< The preferences */
	csuper::ListGameConfigurationPtr list_game_config_;	 /*!< The list of game configuration */
	csuper::UndoRedoManager			 undo_redo_manager_; /*!< The undo redo manager */
	csuper::ExceptionList			 exception_list_;	 /*!< The exception list */

	csuper::GamePtr game_	  = nullptr; /*!< The game */
	Glib::ustring	filename_ = "";		 /*!< The filename */
	bool			broadway_backend_;	 /*!< The broadway back-end option is activated or not */

	Glib::Dispatcher signal_check_for_update_; /*!< The signal when the version is retrieved from
												  the internet is done */
	csuper::Version last_version_;			   /*!< The last version checked from the internet */

	MainWindow*							 main_window_;						/*!< The main window */
	About*								 about_;							/*!< The about dialog */
	GameConfigurationWindow*			 game_config_window_;				/*!< The game configuration window */
	NewGameConfigurationDialog*			 new_game_config_dialog_;			/*!< The new game configuration dialog*/
	ImportExportGameConfigurationDialog* import_export_game_config_dialog_; /*!< The import export game configuration dialog*/
	MenuFile*							 menu_file_;						/*!< The menu file */
	MenuEdit*							 menu_edit_;						/*!< The menu edit */
	MenuDisplay*						 menu_display_;						/*!< The menu display */
	PreferencesWindow*					 pref_window_;						/*!< The preferences window */
	ExportationPreferences*				 export_pref_window_;				/*!< The exportation preferences scrolled window */
	DisplayPreferences*					 display_pref_window_;				/*!< The display preferences scrolled window */
	HtmlPreferences*					 html_pref_window_;					/*!< The html preferences scrolled window */
	NewFileAssistant*					 new_file_assistant_;				/*!< The new file assistant */
	FilePropertiesDialog*				 file_properties_dialog_;			/*!< The file properties dialog */
	GameOverDialog*						 game_over_dialog_;					/*!< The game over dialog */
	StatisticsGrid*						 stat_grid_;						/*!< The statistics grid */
	RankingView*						 rank_;								/*!< The ranking box */
	GameInformationView*				 game_info_;						/*!< The game information box */
	ChangeDistributorDialog*			 change_distributor_dialog_;		/*!< The change distributor dialog */
	PointsView*							 points_view_;						/*!< The points view scrolled window */
	DeleteTurnDialog*					 delete_turn_;						/*!< The delete turn dialog */
	EditTurnDialog*						 edit_turn_;						/*!< The edit turn dialog */
	PlayersNamesView*					 players_names_;					/*!< The players names view */
	ChartTotalPointsGrid*				 chart_total_points_;				/*!< The chart total points dialog */
	ChartPointsGrid*					 chart_points_;						/*!< The chart points dialog */
	DisplayDialog*						 display_dialog_;					/*!< The display dialog */


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor
	 *  \param argc The parameter received by your main() function.
	 *  \param argv The parameter received by your main() function.
	 */
	CsuApplication(int& argc, char**& argv);


	/*!
	 *  \brief Destructor
	 */
	~CsuApplication();



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	/*!
	 *  \brief Function used on the startup signal. It create the menu
	 */
	void onStartup();


	/*!
	 *  \brief Function called when the application opened
	 *  \param files the list of files
	 *  \param hint
	 */
	void on_open(const type_vec_files& files, const Glib::ustring& hint);


	/*!
	 *  \brief Parse the command line arguments
	 *  \param options the options set to the program
	 *  \return -1
	 */
	int localOption(const Glib::RefPtr<Glib::VariantDict>& options);


	/**
	 * @brief A function that check and print the exception from the exception list
	 */
	void checkExceptionList();


	/**
	 * @brief A function called when a new version is getted from the internet
	 */
	void checkForUpdateSlot();



public:
	/*!
	 *  \brief Initialize the CsuApplication with the builder
	 *  \param builder the GTK::Builder
	 */
	void init(Glib::RefPtr<Gtk::Builder>& builder);

	/*!
	 *  \brief Launch the application
	 */
	int launch();

	/*!
	 *  \brief Quit Csuper
	 */
	void onQuit();


	/*!
	 *  \brief Check if there is an update
	 *  \param auto_check indicate if it's an auto check or a manual check
	 */
	void checkForUpdate(const bool auto_check = false);

	/*!
	 *  \brief Initialize the CsuApplication with the builder
	 *  \param game the game that may come from the undo redo manager
	 */
	void updateGame(csuper::GamePtr game = nullptr);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Getter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief return the Preferences
	 *  \return the Preferences
	 */
	inline csuper::PreferencesPtr pref()
	{
		return pref_;
	}

	/*!
	 *  \brief return the ListGameConfiguration
	 *  \return the ListGameConfiguration
	 */
	inline csuper::ListGameConfigurationPtr listGameConfig()
	{
		return list_game_config_;
	}

	/*!
	 *  \brief return the UndoRedoManager
	 *  \return the UndoRedoManager
	 */
	inline csuper::UndoRedoManager& undoRedoManager()
	{
		return undo_redo_manager_;
	}

	inline csuper::ExceptionList& exceptionList()
	{
		return exception_list_;
	}

	/*!
	 *  \brief return the main AccelGroup
	 *  \return the main AccelGroup
	 */
	inline Glib::RefPtr<Gtk::AccelGroup> mainAccelGroup()
	{
		return main_accel_group_;
	}

	/*!
	 *  \brief return the MainWindow
	 *  \return the MainWindow
	 */
	inline MainWindow* mainWindow()
	{
		return main_window_;
	}

	/*!
	 *  \brief return the About
	 *  \return the About
	 */
	inline About* about()
	{
		return about_;
	}

	/*!
	 *  \brief return the GameConfigurationWindow
	 *  \return the GameConfigurationWindow
	 */
	inline GameConfigurationWindow* gameConfigurationWindow()
	{
		return game_config_window_;
	}

	/*!
	 *  \brief return the NewGameConfigurationDialog
	 *  \return the NewGameConfigurationDialog
	 */
	inline NewGameConfigurationDialog* newGameConfigurationDialog()
	{
		return new_game_config_dialog_;
	}

	/*!
	 *  \brief return the ImportExportGameConfigurationDialog
	 *  \return the ImportExportGameConfigurationDialog
	 */
	inline ImportExportGameConfigurationDialog* importExportGameConfigurationDialog()
	{
		return import_export_game_config_dialog_;
	}

	/*!
	 *  \brief return the MenuFile
	 *  \return the MenuFile
	 */
	inline MenuFile* menuFile()
	{
		return menu_file_;
	}

	/*!
	 *  \brief return the MenuEdit
	 *  \return the MenuEdit
	 */
	inline MenuEdit* menuEdit()
	{
		return menu_edit_;
	}

	/*!
	 *  \brief return the MenuDisplay
	 *  \return the MenuDisplay
	 */
	inline MenuDisplay* menuDisplay()
	{
		return menu_display_;
	}

	/*!
	 *  \brief return the PreferencesWindow
	 *  \return the PreferencesWindow
	 */
	inline PreferencesWindow* preferencesWindow()
	{
		return pref_window_;
	}

	/*!
	 *  \brief return the ExportationPreferences
	 *  \return the ExportationPreferences
	 */
	inline ExportationPreferences* exportationPreferencesWindow()
	{
		return export_pref_window_;
	}

	/*!
	 *  \brief return the DisplayPreferences
	 *  \return the DisplayPreferences
	 */
	inline DisplayPreferences* displayPreferencesWindow()
	{
		return display_pref_window_;
	}

	/*!
	 *  \brief return the HtmlPreferences
	 *  \return the HtmlPreferences
	 */
	inline HtmlPreferences* htmlPreferencesWindow()
	{
		return html_pref_window_;
	}

	/*!
	 *  \brief return the NewFileAssistant
	 *  \return the NewFileAssistant
	 */
	inline NewFileAssistant* newFileAssistant()
	{
		return new_file_assistant_;
	}

	/*!
	 *  \brief return the FilePropertiesDialog
	 *  \return the FilePropertiesDialog
	 */
	inline FilePropertiesDialog* filePropertiesDialog()
	{
		return file_properties_dialog_;
	}

	/*!
	 *  \brief return the GameOverDialog
	 *  \return the GameOverDialog
	 */
	inline GameOverDialog* gameOverDialog()
	{
		return game_over_dialog_;
	}

	/*!
	 *  \brief return the StatisticsGrid
	 *  \return the StatisticsDialog
	 */
	inline StatisticsGrid* statisticsGrid()
	{
		return stat_grid_;
	}

	/*!
	 *  \brief return the RankingView
	 *  \return the RankingView
	 */
	inline RankingView* ranking()
	{
		return rank_;
	}

	/*!
	 *  \brief return the GameInformationView
	 *  \return the GameInformationView
	 */
	inline GameInformationView* gameInformation()
	{
		return game_info_;
	}

	/*!
	 *  \brief return the ChangeDistributorDialog
	 *  \return the ChangeDistributorDialog
	 */
	inline ChangeDistributorDialog* changeDistributorDialog()
	{
		return change_distributor_dialog_;
	}

	/*!
	 *  \brief return the PointsView
	 *  \return the PointsView
	 */
	inline PointsView* pointsView()
	{
		return points_view_;
	}

	/*!
	 *  \brief return the DeleteTurnDialog
	 *  \return the DeleteTurnDialog
	 */
	inline DeleteTurnDialog* deleteTurnDialog()
	{
		return delete_turn_;
	}

	/*!
	 *  \brief return the EditTurnDialog
	 *  \return the EditTurnDialog
	 */
	inline EditTurnDialog* editTurnDialog()
	{
		return edit_turn_;
	}

	/*!
	 *  \brief return the PlayersNamesView
	 *  \return the PlayersNamesView
	 */
	inline PlayersNamesView* playersNamesView()
	{
		return players_names_;
	}

	/*!
	 *  \brief return the ChartTotalPointsGrid
	 *  \return the ChartTotalPointsGrid
	 */
	inline ChartTotalPointsGrid* chartTotalPointsGrid()
	{
		return chart_total_points_;
	}

	/*!
	 *  \brief return the ChartPointsGrid
	 *  \return the ChartPointsGrid
	 */
	inline ChartPointsGrid* chartPointsGrid()
	{
		return chart_points_;
	}

	/*!
	 *  \brief return the DisplayDialog
	 *  \return the DisplayDialog
	 */
	inline DisplayDialog* displayDialog()
	{
		return display_dialog_;
	}




	/*!
	 *  \brief return the Game
	 *  \return the Game
	 */
	inline csuper::GamePtr game()
	{
		return game_;
	}

	/*!
	 *  \brief return the filename
	 *  \return the filename
	 */
	inline Glib::ustring filename()
	{
		return filename_;
	}

	/*!
	 *  \brief return the broadway back-end
	 *  \return the broadway back-end
	 */
	inline bool broadwayBackend()
	{
		return broadway_backend_;
	}



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Setter ////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Set a new Game
	 *  \param game the new Game
	 */
	void setGame(csuper::GamePtr game);


	/*!
	 *  \brief Set a new filename
	 *  \param filename the new filename
	 */
	void setFilename(const Glib::ustring& filename);
};

#endif	  // CSU_APPLICATION_H_INCLUDED
