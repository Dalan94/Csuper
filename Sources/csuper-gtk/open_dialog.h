/*!
 * \file    open_dialog.h
 * \author  Remi BERTHO
 * \date    11/01/16
 * \version 4.3.0
 */

/*
 * open_dialog.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef OPEN_DIALOG_H_INCLUDED
#define OPEN_DIALOG_H_INCLUDED

#include <gtkmm.h>

#include "csu_widget.h"

/*! \class OpenDialog
 *   \brief This class is a open dialog
 */
class OpenDialog : CsuWidget, public Gtk::Dialog
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::Button*		 ok_button_;	   /*!< The OK button */
	Gtk::Button*		 cancel_button_;   /*!< The cancel button */
	Gtk::ScrolledWindow* scrolled_window_; /*!< The scrolled window */
	Gtk::Grid*			 main_grid_;	   /*!< The main grid */

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor
	 */
	explicit OpenDialog(Gtk::Window& parent_window);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Show open dialog
	 *  \return the filename selected
	 */
	Glib::ustring launch();
};


#endif	  // OPEN_DIALOG_H_INCLUDED
