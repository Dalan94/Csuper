/*!
 * \file    save_dialog.h
 * \author  Remi BERTHO
 * \date    12/01/16
 * \version 4.3.0
 */

/*
 * save_dialog.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef SAVE_DIALOG_H_INCLUDED
#define SAVE_DIALOG_H_INCLUDED

#include <gtkmm.h>

#include "csu_widget.h"

/*! \class SaveDialog
 *   \brief This class is used a save dialog
 */
class SaveDialog : CsuWidget, public Gtk::Dialog
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
private:
	Gtk::Button*		 ok_button_;	   /*!< The OK button */
	Gtk::Button*		 cancel_button_;   /*!< The cancel button */
	Gtk::ScrolledWindow* scrolled_window_; /*!< The scrolled window */
	Gtk::Grid*			 main_grid_;	   /*!< The main grid */
	Gtk::Entry*			 filename_entry_;  /*!< The filename entry */
	Gtk::ComboBoxText*	 combobox_;		   /*!< The file filter combobox */

	unsigned int file_filter_ = 0; /*!< The file filter index */

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor
	 */
	SaveDialog(Gtk::Window& parent_window, const std::vector<Glib::ustring>& file_filter, const bool export_dialog = false);


	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Show save dialog
	 *  \return the filename selected
	 */
	Glib::ustring launch();

	/*!
	 *  \brief Get the file filter index
	 *  \return the file filter index
	 */
	inline unsigned int fileFilter() const
	{
		return file_filter_;
	}
};

#endif	  // SAVE_DIALOG_H_INCLUDED
