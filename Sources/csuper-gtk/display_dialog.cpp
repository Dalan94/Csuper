/*!
 * \file    display_dialog.cpp
 * \author  Remi BERTHO
 * \date    27/08/15
 * \version 4.3.0
 */

/*
 * display_dialog.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include "display_dialog.h"

#include "csu_application.h"
#include "statistics_grid.h"
#include "chart_total_points_grid.h"
#include "chart_points_grid.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;
using namespace std;


//
// Constructor and destructor
//
DisplayDialog::DisplayDialog(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), Dialog(cobject)
{
	refGlade->get_widget("display_dialog_validate_button", ok_button_);
	refGlade->get_widget("display_dialog_notebook", notebook_);

	ok_button_->set_image_from_icon_name("gtk-ok", ICON_SIZE_BUTTON);
}

//
// Function
//
void DisplayDialog::launch(const Type type)
{
	app()->statisticsGrid()->update();
	app()->chartTotalPointsGrid()->update();
	app()->chartPointsGrid()->update();

	notebook_->set_current_page(type);

	run();

	hide();
}
