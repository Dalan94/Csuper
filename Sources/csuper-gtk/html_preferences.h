/*!
 * \file    display_preferences.h
 * \author  Remi BERTHO
 * \date    06/08/17
 * \version 4.3.2
 */

/*
 * display_preferences.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef HTML_PREFERENCES_H_INCLUDED
#define HTML_PREFERENCES_H_INCLUDED

#include <gtkmm.h>
#include "../libcsuper/libcsuper.h"


#include "csu_widget.h"


/*! \class HtmlPreferences
 *   \brief This class represent the html preferences scrolled window
 */
class HtmlPreferences : public CsuWidget, public Gtk::ScrolledWindow
{
protected:
	Gtk::Grid*	   main_grid_; /*!< The main grid */
	Gtk::Viewport* viewport_;  /*!< The viewport */

	Gtk::Label*	 start_label_;		/*!< The start label */
	Gtk::Label*	 autostart_label_;	/*!< The autostart label */
	Gtk::Switch* autostart_switch_; /*!< The autostart switch */

	Gtk::Label* info_label_;	 /*!< The info label */
	Gtk::Label* ip_label_;		 /*!< The ip label */
	Gtk::Label* firewall_label_; /*!< The firewall label */


	bool last_autostart_; /*!<  The last autostart value */

	//
	// Function
	//
	/*!
	 *  \brief Get the autostart filename
	 */
	static Glib::ustring getAutostartFilename();

public:
	//
	// Constructor and Destructor
	//
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	HtmlPreferences(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);


	//
	// Function
	//
	/*!
	 *  \brief Update the view
	 */
	void update();


	/*!
	 *  \brief Update the view
	 *  \return true if the pref has changed, false otherwise
	 */
	bool hasChanged();


	/*!
	 *  \brief Apply the new preferences
	 */
	void applyChanges();
};


#endif	  // HTML_PREFERENCES_H_INCLUDED
