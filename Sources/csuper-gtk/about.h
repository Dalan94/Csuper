/*!
 * \file    about.h
 * \author  Remi BERTHO
 * \date    29/07/15
 * \version 4.3.0
 */

/*
 * about.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef ABOUT_H_INCLUDED
#define ABOUT_H_INCLUDED

#include <gtkmm.h>

#include "csu_widget.h"

/*! \class About
 *   \brief This class represent the about dialog
 */
class About : public CsuWidget, public Gtk::AboutDialog
{
protected:
public:
	//
	// Constructor and Destructor
	//
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	About(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);



	/*!
	 *  \brief Show the about dialog
	 */
	void launch();
};

#endif	  // ABOUT_H_INCLUDED
