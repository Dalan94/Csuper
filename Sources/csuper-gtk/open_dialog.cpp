/*!
 * \file    open_dialog.cpp
 * \author  Remi BERTHO
 * \date    11/01/16
 * \version 4.3.0
 */

/*
 * open_dialog.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "config.h"

#include "open_dialog.h"
#include <glibmm/i18n.h>

#include "csu_application.h"

using namespace Gtk;
using namespace Glib;
using namespace Gio;
using namespace Gdk;
using namespace std;

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Constructor ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
OpenDialog::OpenDialog(Window& parent_window)
{
	set_transient_for(parent_window);
	set_gravity(GRAVITY_CENTER);
	set_position(WIN_POS_CENTER_ON_PARENT);
	set_modal(true);
	set_type_hint(WINDOW_TYPE_HINT_DIALOG);
	resize(500, 400);
	set_title(_("Open file"));

	cancel_button_ = add_button(_("Cancel"), RESPONSE_CANCEL);
	cancel_button_->set_image_from_icon_name("gtk-cancel", ICON_SIZE_BUTTON);

	ok_button_ = add_button(_("Open"), RESPONSE_ACCEPT);
	ok_button_->set_image_from_icon_name("gtk-open", ICON_SIZE_BUTTON);

	get_content_area()->add(*(new Label(app()->pref()->directory().open())));

	main_grid_ = manage(new Grid());
	main_grid_->set_hexpand(true);
	main_grid_->set_vexpand(true);
	scrolled_window_ = manage(new ScrolledWindow());
	get_content_area()->add(*scrolled_window_);
	scrolled_window_->add(*main_grid_);
	scrolled_window_->set_min_content_height(300);
	scrolled_window_->set_min_content_width(200);
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
ustring OpenDialog::launch()
{
	ustring			result;
	vector<ustring> filenames;

	try
	{
		Dir dir(filename_from_utf8(app()->pref()->directory().open()));

		RadioButtonGroup group;
		RadioButton*	 tmp_button;
		unsigned int	 i = 0;

		DirIterator it;
		for (it = dir.begin(); it != dir.end(); it++)
		{
			ustring filename = filename_to_utf8(*it);

			RefPtr<File> file = File::create_for_path(build_filename(filename_from_utf8(app()->pref()->directory().open()), *it));

			if (file->query_file_type() == FILE_TYPE_REGULAR)
			{
				filenames.push_back(filename);
				tmp_button = manage(new RadioButton(group, filename));
				main_grid_->attach(*tmp_button, 0, i + 1, 1, 1);
				i++;
			}
		}
	}
	catch (Glib::Error& e)
	{
		g_info("%s", e.what().c_str());
		MessageDialog* error = new MessageDialog(*this, e.what(), false, MESSAGE_ERROR, BUTTONS_OK, true);
		error->run();
		error->hide();
		delete error;
		return "";
	}

	show_all();
	switch (run())
	{
	case RESPONSE_ACCEPT:
		unsigned int i;
		for (i = 0; i < filenames.size(); i++)
		{
			if (static_cast<RadioButton*>(main_grid_->get_child_at(0, i + 1))->get_active())
				break;
		}
		result = build_filename(app()->pref()->directory().open(), filenames[i]);
		break;
	case RESPONSE_CANCEL:
		break;
	default:
		break;
	}
	hide();

	return result;
}
