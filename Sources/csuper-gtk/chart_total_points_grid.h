/*!
 * \file    chart_total_points_widget.h
 * \author  Remi BERTHO
 * \date    04/12/15
 * \version 4.3.0
 */

/*
 * chart_total_points_widget.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CHART_TOTAL_POINTS_GRID_H_INCLUDED
#define CHART_TOTAL_POINTS_GRID_H_INCLUDED

#include <gtkmm.h>

#include "chart_grid.h"


/*! \class ChartTotalPointsGrid
 *   \brief This class represent the chart widget
 */
class ChartTotalPointsGrid : public ChartGrid
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	ChartTotalPointsGrid(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);

	~ChartTotalPointsGrid();

	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Show the widget properties
	 */
	void update();
};

#endif	  // CHART_TOTAL_POINTS_GRID_H_INCLUDED
