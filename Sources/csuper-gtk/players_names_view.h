/*!
 * \file    players_names_view.h
 * \author  Remi BERTHO
 * \date    26/11/15
 * \version 4.3.0
 */

/*
 * players_names_view.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef PLAYERS_NAMES_VIEW_H_INCLUDED
#define PLAYERS_NAMES_VIEW_H_INCLUDED

#include <gtkmm.h>

#include "csu_widget.h"
#include "libcsuper/libcsuper.h"

/*! \class PointsView
 *   \brief This class represent the points view
 */
class PlayersNamesView : public CsuWidget, public Gtk::ScrolledWindow
{
protected:
	Gtk::Viewport* viewport_;			 /*!< The viewport */
	Gtk::Grid*	   main_grid_ = nullptr; /*!< The main grid */



	//
	// Functions
	//
	/*!
	 *  \brief Call when the scroll changed
	 */
	void scrollChanged();

public:
	//
	// Constructor and Destructor
	//
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	PlayersNamesView(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);


	//
	// Function
	//
	/*!
	 *  \brief Update the points
	 */
	void update();


	/*!
	 *  \brief Update the points
	 */
	void updateSize();


	/*!
	 *  \brief Update the points
	 *  \param scroll_value the new scroll value
	 */
	void setScroll(const double scroll_value);
};

#endif	  // PLAYERS_NAMES_VIEW_H_INCLUDED
