/*!
 * \file    display_preferences.h
 * \author  Remi BERTHO
 * \date    06/08/17
 * \version 4.3.2
 */
/*
 * display_preferences.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include "html_preferences.h"

#include <iostream>
#include <fstream>
#include <iomanip>

#include "csu_application.h"
#include "preferences_window.h"
#include "main_window.h"

using namespace Gtk;
using namespace Glib;
using namespace Gio;
using namespace csuper;
using namespace std;

HtmlPreferences::HtmlPreferences(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), ScrolledWindow(cobject)
{
	refGlade->get_widget("preferences_html_grid", main_grid_);
	refGlade->get_widget("preferences_html_viewport", viewport_);

	refGlade->get_widget("preferences_html_start_label", start_label_);
	refGlade->get_widget("preferences_html_autostart_label", autostart_label_);
	refGlade->get_widget("preferences_html_autostart_switch", autostart_switch_);

	refGlade->get_widget("preferences_html_info_label", info_label_);
	refGlade->get_widget("preferences_html_ip_label", ip_label_);
	refGlade->get_widget("preferences_html_firewall_label", firewall_label_);

	autostart_switch_->property_active().signal_changed().connect(mem_fun(*(app()->preferencesWindow()), &PreferencesWindow::hasChange));
}

ustring HtmlPreferences::getAutostartFilename()
{
#ifdef G_OS_UNIX
	return build_filename(get_home_dir(), ".config", "autostart", "csuper-html.desktop");
#elif defined G_OS_WIN32
	return build_filename(
			get_home_dir(), "AppData", "Roaming", "Microsoft", "Windows", "Start Menu", "Programs", "Startup", "csuper-html.vbs");
#endif
}


void HtmlPreferences::update()
{
	ustring		 autostart_filename = getAutostartFilename();
	RefPtr<File> autostart_file		= File::create_for_path(filename_from_utf8(autostart_filename));

	last_autostart_ = autostart_file->query_exists();
	autostart_switch_->set_active(last_autostart_);

	// Get local IP
	auto				ip_resolver	 = Resolver::get_default();
	auto				ip_addresses = ip_resolver->lookup_by_name(g_get_host_name());
	RefPtr<InetAddress> local_ip;
	for (const auto& ip : ip_addresses)
	{
		if (ip->get_family() == SocketFamily::SOCKET_FAMILY_IPV4)
		{
			local_ip = ip;
			break;
		}
	}
	ip_label_->set_markup(ustring::compose(
			_("The HTML will be available on all your LAN (local Area Network) at the address <a href=\"http://%1:52721\">http://%1:52721</a>."),
			local_ip->to_string()));
}

bool HtmlPreferences::hasChanged()
{
	return last_autostart_ != autostart_switch_->get_active();
}

void HtmlPreferences::applyChanges()
{
	ustring autostart_filename = getAutostartFilename();

	last_autostart_ = autostart_switch_->get_active();

	if (autostart_switch_->get_active())
	{
		ofstream autostart_file;
		autostart_file.exceptions(ofstream::failbit | ofstream::badbit);
		try
		{
			autostart_file.open(locale_from_utf8(autostart_filename), ofstream::out | ofstream::app);
		}
		catch (ios_base::failure& e)
		{
			g_info("%s", e.what());
			MessageDialog* error = new MessageDialog(*app()->mainWindow(), e.what(), false, MESSAGE_ERROR, BUTTONS_OK, true);
			error->run();
			error->hide();
			delete error;
		}
#ifdef G_OS_UNIX
		autostart_file << "[Desktop Entry]" << endl
					   << "Version=1.0" << endl
					   << "Type=Application" << endl
					   << "Name=Csuper-HTML" << endl
					   << "Icon=/usr/share/csuper/Images/Logo_100.png" << endl
					   << "Exec=csuper-html-autostart" << endl
					   << "NoDisplay=false" << endl
					   << "Categories=Game;" << endl
					   << "StartupNotify=false" << endl
					   << "Terminal=true" << endl
					   << "Name[fr_FR]=Csuper";
#elif defined G_OS_WIN32
		autostart_file << "Set WshShell = CreateObject(\"WScript.Shell\")" << endl
					   << "WshShell.RUN \"\"\"C:\\Program Files\\Csuper\\csuper-html-autostart.bat\"\"\", 0, True";


#endif
		autostart_file.close();

		g_debug("Csuper-HTML autostart enable");
	}
	else
	{
		try
		{
			removeFile(autostart_filename);
		}
		catch (csuper::Exception& e)
		{
			g_info("%s", e.what().c_str());
			MessageDialog* error = new MessageDialog(*app()->mainWindow(), e.what(), false, MESSAGE_ERROR, BUTTONS_OK, true);
			error->run();
			error->hide();
			delete error;
		}
		g_debug("Csuper-HTML autostart disable");
	}
}
