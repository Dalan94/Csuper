/*!
 * \file    edit_turn_dialog.cpp
 * \author  Remi BERTHO
 * \date    09/10/15
 * \version 4.3.0
 */

/*
 * edit_turn_dialog.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include "edit_turn_dialog.h"

#include "csu_application.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;
using namespace std;


//
// Constructor and destructor
//
EditTurnDialog::EditTurnDialog(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), Dialog(cobject)
{
	refGlade->get_widget("edit_turn_button_validate", ok_button_);
	refGlade->get_widget("edit_turn_button cancel", cancel_button_);
	refGlade->get_widget("edit_turn_label", title_);
	refGlade->get_widget("edit_turn_grid", main_grid_);
	refGlade->get_widget("edit_turn_scrolledwindow", window_);
	refGlade->get_widget("edit_turn_viewport", viewport_);

	ok_button_->set_image_from_icon_name("gtk-ok", ICON_SIZE_BUTTON);
	cancel_button_->set_image_from_icon_name("gtk-cancel", ICON_SIZE_BUTTON);
}

//
// Function
//
bool EditTurnDialog::launch(unsigned int turn)
{
	bool res = false;

	Grid* grid = new Grid();
	grid->set_column_spacing(5);
	grid->set_row_spacing(5);
	grid->set_margin_end(10);
	grid->set_margin_start(10);
	grid->set_margin_top(10);
	grid->set_margin_bottom(10);
	grid->set_hexpand(true);


	unsigned int nb = 0;
	for (unsigned int i = 0; i < app()->game()->nbPlayer(); i++)
	{
		if (app()->game()->hasTurn(i, turn))
		{
			Label* tmp_label = manage(new Label(app()->game()->playerName(i)));
			grid->attach(*tmp_label, nb, 0, 1, 1);
			auto		tmp_adj	 = Adjustment::create(0, -numeric_limits<double>::max(), numeric_limits<double>::max(), 1, 0, 0);
			SpinButton* tmp_spin = manage(new SpinButton(tmp_adj, 1, app()->game()->config().decimalPlace()));
			tmp_spin->set_alignment(0.5);
			tmp_spin->set_width_chars(3);
			tmp_spin->set_value(app()->game()->points(i, turn));
			tmp_spin->set_hexpand(true);
			tmp_spin->set_halign(ALIGN_CENTER);
			grid->attach(*tmp_spin, nb, 1, 1, 1);
			nb++;
		}
	}

	viewport_->add(*grid);

	show_all();
	vector<double> new_points;
	switch (run())
	{
	case RESPONSE_ACCEPT:
		res = true;

		unsigned int i;
		for (i = 0; i < nb; i++)
		{
			SpinButton* tmp_spin_button = static_cast<SpinButton*>(grid->get_child_at(i, 1));
			new_points.push_back(tmp_spin_button->get_value());
		}

		if (app()->game()->config().turnBased())
			app()->game()->editTurn(turn, new_points);
		else
		{
			for (i = 0; i < nb; i++)
			{
				Label* tmp_label = static_cast<Label*>(grid->get_child_at(i, 0));
				app()->game()->editTurn(turn, tmp_label->get_text(), new_points[i]);
			}
		}

		break;
	case RESPONSE_CANCEL:
		break;
	default:
		break;
	}
	hide();

	delete grid;

	return res;
}
