/*!
 * \file    main.cpp
 * \brief   Begin csuper
 * \author  Remi BERTHO
 * \date    28/07/15
 * \version 4.3.0
 */

/*
 * main.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"
#include <glibmm/i18n.h>

#include <clocale>
#include <iomanip>
#include <gtkmm.h>
#include <glib/gstdio.h>
#include "libcsuper/libcsuper.h"
#include "csu_application.h"

using namespace csuper;
using namespace std;
using namespace Glib;
using namespace Gtk;

/*!
 *  Print an error message on error terminate
 *  \param log_domain the log domain of the message
 *  \param log_level the log level of the message (including the fatal and recursion flags)
 *  \param message the message to process
 *  \param user_data user data
 */
void terminateFunction(const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data)
{
	cerr << message << endl;

	MessageDialog error(message, false, MESSAGE_ERROR, BUTTONS_OK, true);
	error.run();
}



/*!
 *  \brief Function called when log occured
 *  \param log_level the log level
 *  \param fields the log fiels
 *  \param n_fields the number of fields
 *  \param user_data the user data (ustring)
 */
#ifdef GLIB_AVAILABLE_IN_2_50
GLogWriterOutput logWriterFunction(GLogLevelFlags log_level, const GLogField* fields, gsize n_fields, gpointer user_data)
#else
void logWriterFunction(const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data)
#endif
{
	string				  print_log_level = *static_cast<string*>(user_data);
	map<ustring, ustring> str_fields;
	DateTime			  current_time = DateTime::create_now_local();
	ostringstream		  log_message;

// Log to systemd
#if (defined GLIB_AVAILABLE_IN_2_50) && (!defined PORTABLE)
	g_log_writer_journald(log_level, fields, n_fields, user_data);
#endif

	// Check if log need to be written
	if (((print_log_level == "warning") && (log_level > G_LOG_LEVEL_WARNING)) ||
			((print_log_level == "info") && (log_level > G_LOG_LEVEL_INFO)))
	{
#ifdef GLIB_AVAILABLE_IN_2_50
		return G_LOG_WRITER_HANDLED;
#else
		return;
#endif
	}

// Convert to map
#ifdef GLIB_AVAILABLE_IN_2_50
	for (guint i = 0; i < n_fields; i++)
		str_fields[fields[i].key] = (char*)fields[i].value;
#else
	str_fields["MESSAGE"]	  = message;
	str_fields["GLIB_DOMAIN"] = log_domain;
#endif

	// Print date
	log_message << "[" << current_time.format("%X.") << setw(3) << setfill('0') << current_time.get_microsecond() / 1000 << "] ";

	// Print severity
	switch (log_level)
	{
	case G_LOG_FLAG_RECURSION:
#ifdef G_OS_UNIX
// log_message << "\033[31m";
#endif
		log_message << "Emergency ";
		break;
	case G_LOG_FLAG_FATAL:
#ifdef G_OS_UNIX
// log_message << "\033[31m";
#endif
		log_message << "Fatal     ";
		break;
	case G_LOG_LEVEL_ERROR:
#ifdef G_OS_UNIX
// log_message << "\033[31m";
#endif
		log_message << "Error     ";
		break;
	case G_LOG_LEVEL_CRITICAL:
#ifdef G_OS_UNIX
// log_message << "\033[31m";
#endif
		log_message << "Critical  ";
		break;
	case G_LOG_LEVEL_WARNING:
#ifdef G_OS_UNIX
// log_message << "\033[33m";
#endif
		log_message << "Warning   ";
		break;
	case G_LOG_LEVEL_MESSAGE:
#ifdef G_OS_UNIX
// log_message << "\033[36m";
#endif
		log_message << "Message   ";
		break;
	case G_LOG_LEVEL_INFO:
#ifdef G_OS_UNIX
// log_message << "\033[36m";
#endif
		log_message << "Info      ";
		break;
	case G_LOG_LEVEL_DEBUG:
#ifdef G_OS_UNIX
// log_message << "\033[32m";
#endif
		log_message << "Debug     ";
		break;
	default:
		break;
	}
#ifdef G_OS_UNIX
// Reset color
// log_message << "\033[0m";
#endif

	// Print domain
	log_message << str_fields["GLIB_DOMAIN"];

	// Print file
	if (str_fields.count("CODE_FILE") == 1)
		log_message << " - " << ustring(path_get_basename(str_fields["CODE_FILE"])) << ":" << str_fields["CODE_LINE"];

	// Print function
	if (str_fields.count("CODE_FUNC") == 1)
		log_message << " - " << str_fields["CODE_FUNC"] << " ";

	// Print message
	log_message << ": ";
#ifdef G_OS_UNIX
// log_message << "\033[35m";
#endif
	log_message << removeCharacterInUstring(str_fields["MESSAGE"], '\n');
#ifdef G_OS_UNIX
// log_message << "\033[0m";
#endif


	// Print the message to the stream
	cerr << log_message.str() << endl;

#ifdef GLIB_AVAILABLE_IN_2_50
	return G_LOG_WRITER_HANDLED;
#endif
}


/*!
 * \fn int main(int argc, char *argv[])
 *  Begin csuper.
 * \param[in] argc the number of argument.
 * \param[in] argv the array of argument.
 * \return 0 if everything is OK
 */
int main(int argc, char* argv[])
{
// Setup directory
#ifdef G_OS_WIN32
	string directory = path_get_dirname(argv[0]);
	if (path_is_absolute(directory))
		g_chdir(directory.c_str());
#endif	  // G_OS_WIN32

	// Set locals
	locale::global(locale(""));
	setlocale(LC_ALL, "");
	cout.imbue(locale(""));
	cerr.imbue(locale(""));
	cin.imbue(locale(""));
	bindtextdomain("csuper-gtk", "Locales");
	bind_textdomain_codeset("csuper-gtk", "UTF-8");
	textdomain("csuper-gtk");

	// Set name
	set_prgname("csuper-gtk");
	set_application_name(_("Csuper"));

	// Set terminate function
	string log_level = Glib::getenv("CSUPER_LOG_LEVEL");
	if (log_level == "" || ((log_level != "debug") && (log_level != "warning") && (log_level != "info")))
		log_level = "warning";
#ifdef GLIB_AVAILABLE_IN_2_50
	g_log_set_writer_func(&logWriterFunction, &log_level, nullptr);
#else
	g_log_set_handler("libcsuper",
			(GLogLevelFlags)(G_LOG_LEVEL_DEBUG | G_LOG_LEVEL_INFO | G_LOG_LEVEL_WARNING | G_LOG_LEVEL_ERROR | G_LOG_FLAG_FATAL |
							 G_LOG_FLAG_RECURSION),
			&logWriterFunction,
			&log_level);
	g_log_set_handler("csuper-gtk",
			(GLogLevelFlags)(G_LOG_LEVEL_DEBUG | G_LOG_LEVEL_INFO | G_LOG_LEVEL_WARNING | G_LOG_LEVEL_ERROR | G_LOG_FLAG_FATAL |
							 G_LOG_FLAG_RECURSION),
			&logWriterFunction,
			&log_level);
#endif
	g_log_set_handler("glibmm", (GLogLevelFlags)(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION), terminateFunction, nullptr);
	g_log_set_handler("gtkmm", (GLogLevelFlags)(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION), terminateFunction, nullptr);

	// Create the application
	RefPtr<CsuApplication> app(new CsuApplication(argc, argv));
	// Load the Glade file
	RefPtr<Builder> builder = Builder::create();
	try
	{
		builder->add_from_file("UI/csuper-gtk.glade");
	}
	catch (Glib::Exception& e)
	{
		g_error("%s", e.what().c_str());
		return EXIT_FAILURE;
	}
	app->init(builder);

	int return_value = app->launch();
	g_debug("End with value %d", return_value);
	return return_value;
}
