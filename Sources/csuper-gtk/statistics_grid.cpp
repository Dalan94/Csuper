/*!
 * \file    statistics_grid.cpp
 * \author  Remi BERTHO
 * \date    27/08/15
 * \version 4.3.0
 */

/*
 * statistics_grid.cpp
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "config.h"

#include "statistics_grid.h"

#include <glibmm/i18n.h>
#include "csu_application.h"

using namespace Gtk;
using namespace Glib;
using namespace csuper;
using namespace std;


//
// Constructor and destructor
//
StatisticsGrid::StatisticsGrid(BaseObjectType* cobject, const RefPtr<Builder>& refGlade) : CsuWidget(), Grid(cobject)
{
	refGlade->get_widget("display_dialog_statistics_scrolledwindow", scrolled_window_);
	refGlade->get_widget("display_dialog_statistics_viewport", viewport_);
	refGlade->get_widget("display_dialog_statistics_label", stat_label_);
}

//
// Function
//
void StatisticsGrid::update()
{
	if (stat_grid_ != nullptr)
		delete stat_grid_;
	stat_grid_ = manage(new Grid());

	stat_grid_->set_column_spacing(5);
	stat_grid_->set_row_spacing(5);
	stat_grid_->set_margin_end(10);
	stat_grid_->set_margin_start(10);
	stat_grid_->set_margin_top(10);
	stat_grid_->set_margin_bottom(10);
	stat_grid_->set_hexpand(true);
	stat_grid_->set_vexpand(true);

	unsigned int nb_row;
	if (app()->game()->config().turnBased())
		nb_row = 7;
	else
		nb_row = 3;

	for (unsigned int i = 0; i < nb_row + 1; i++)
		stat_grid_->attach(*(manage(new Separator(ORIENTATION_HORIZONTAL))), 0, 2 * i, 2 * app()->game()->nbPlayer() + 3, 1);

	stat_grid_->attach(*(manage(new Label(_("Name")))), 1, 1, 1, 1);
	stat_grid_->attach(*(manage(new Label(_("Mean points")))), 1, 3, 1, 1);
	stat_grid_->attach(*(manage(new Label(_("Number of turn")))), 1, 5, 1, 1);
	if (app()->game()->config().turnBased())
	{
		stat_grid_->attach(*(manage(new Label(_("Number of turn with the best score")))), 1, 7, 1, 1);
		stat_grid_->attach(*(manage(new Label(_("Number of turn with the worst score")))), 1, 9, 1, 1);
		stat_grid_->attach(*(manage(new Label(_("Number of turn first")))), 1, 11, 1, 1);
		stat_grid_->attach(*(manage(new Label(_("Number of turn last")))), 1, 13, 1, 1);
	}
	stat_grid_->attach(*(manage(new Separator(ORIENTATION_VERTICAL))), 0, 0, 1, 2 * nb_row + 1);
	stat_grid_->attach(*(manage(new Separator(ORIENTATION_VERTICAL))), 2, 0, 1, 2 * nb_row + 1);

	for (unsigned int i = 0; i < app()->game()->nbPlayer(); i++)
	{
		Label* tmp_label;

		tmp_label = manage(new Label(app()->game()->playerName(i)));
		tmp_label->set_hexpand(true);
		tmp_label->set_hexpand(true);
		stat_grid_->attach(*tmp_label, 2 * (i + 1) + 1, 1, 1, 1);

		tmp_label = manage(new Label(app()->game()->meanPointsUstring(i)));
		tmp_label->set_hexpand(true);
		tmp_label->set_hexpand(true);
		stat_grid_->attach(*tmp_label, 2 * (i + 1) + 1, 3, 1, 1);

		tmp_label = manage(new Label(app()->game()->nbTurnUstring(i)));
		tmp_label->set_hexpand(true);
		tmp_label->set_hexpand(true);
		stat_grid_->attach(*tmp_label, 2 * (i + 1) + 1, 5, 1, 1);

		if (app()->game()->config().turnBased())
		{
			tmp_label = manage(new Label(app()->game()->nbTurnBestUstring(i)));
			tmp_label->set_hexpand(true);
			tmp_label->set_hexpand(true);
			stat_grid_->attach(*tmp_label, 2 * (i + 1) + 1, 7, 1, 1);

			tmp_label = manage(new Label(app()->game()->nbTurnWorstUstring(i)));
			tmp_label->set_hexpand(true);
			tmp_label->set_hexpand(true);
			stat_grid_->attach(*tmp_label, 2 * (i + 1) + 1, 9, 1, 1);

			tmp_label = manage(new Label(app()->game()->nbTurnFirstUstring(i)));
			tmp_label->set_hexpand(true);
			tmp_label->set_hexpand(true);
			stat_grid_->attach(*tmp_label, 2 * (i + 1) + 1, 11, 1, 1);

			tmp_label = manage(new Label(app()->game()->nbTurnLastUstring(i)));
			tmp_label->set_hexpand(true);
			tmp_label->set_hexpand(true);
			stat_grid_->attach(*tmp_label, 2 * (i + 1) + 1, 13, 1, 1);
		}

		stat_grid_->attach(*(manage(new Separator(ORIENTATION_VERTICAL))), 2 * (2 + i), 0, 1, 2 * nb_row + 1);
	}

	viewport_->add(*stat_grid_);
	show_all();
}
