/*!
 * \file    game_information_view.h
 * \author  Remi BERTHO
 * \date    18/12/15
 * \version 4.3.0
 */

/*
 * game_information_view.h
 *
 * Copyright 2014-2017 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This file is part of Csuper-gtk.
 *
 * Csuper-gtk is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Csuper-gtk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef GAME_INFORMATION_VIEW_H_INCLUDED
#define GAME_INFORMATION_VIEW_H_INCLUDED

#include <gtkmm.h>

#include "csu_widget.h"

/*! \class GameInformationView
 *   \brief This class represent the game information view
 */
class GameInformationView : public CsuWidget, public Gtk::Box
{
	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Attributes ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
protected:
	Gtk::Separator* sep_;				 /*!< The separator */
	Gtk::Label*		distributor_;		 /*!< The distributor label */
	Gtk::Label*		game_info_label_;	 /*!< The game information label */
	Gtk::Button*	change_distributor_; /*!< The change distributor button */
	Gtk::Label*		total_poins_;		 /*!< The total points label */



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Constructor ///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Constructor with builder
	 *  \param cobject the C object
	 *  \param refGlade the builder
	 */
	GameInformationView(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);



	///////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Function //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
public:
	/*!
	 *  \brief Update distributor
	 */
	void updateDistributor();


	/*!
	 *  \brief Update points
	 */
	void updatePoints(const double points);


protected:
	/*!
	 *  \brief Change distributor
	 */
	void changeDistributor();

	/*!
	 *  \brief Show or hide the information based on the preferences
	 */
	void onPreferencesChanged();
};

#endif	  // GAME_INFORMATION_VIEW_H_INCLUDED
