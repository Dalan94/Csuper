TODO
====

4.3.2
-----

* [x] Add temporary folder for game creation
* [x] Add informations about automatic save
* [x] Merge statistics and charts windows
* [x] Upgrade slope
* [x] Choose default pref (UI) depending on user environnment
* [x] No error message when looking for new version when launching
* [x] Improve HTML version
* [x] Add logs


4.5.x
-----

* [ ] C++17 variant instead of Glib::Variant
* [ ] C++17 filesystem
* [ ] flatpak/snap
* [ ] Gtkmm4