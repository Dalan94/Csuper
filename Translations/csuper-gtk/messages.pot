# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-18 23:33+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Sources/csuper-gtk/chart_grid.cpp:127
msgid "Total points on "
msgstr ""

#: Sources/csuper-gtk/chart_grid.cpp:129
msgid "Points on "
msgstr ""

#: Sources/csuper-gtk/chart_grid.cpp:142 Sources/csuper-gtk/points_view.cpp:128
#: Sources/csuper-gtk/ranking_view.cpp:88
msgid "Points"
msgstr ""

#: Sources/csuper-gtk/chart_grid.cpp:142
msgid "Turns"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:108
msgid ""
"Fatal error during initialization, please check your preferences an game "
"configurations files."
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:234
#: Sources/csuper-gtk/main_window.cpp:92 UI/csuper-gtk.glade:2254
msgid "Preferences"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:235
#: Sources/csuper-gtk/main_window.cpp:91 UI/csuper-gtk.glade:1246
#: UI/csuper-gtk.glade:1895
msgid "Game configuration"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:236
#: Sources/csuper-gtk/main_window.cpp:93
msgid "About"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:237
#: Sources/csuper-gtk/main_window.cpp:94
msgid "Check for update"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:238
#: Sources/csuper-gtk/main_window.cpp:95
msgid "Quit"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:260
msgid "Opened"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:261
msgid "Csuper-HTML is available at http://%1:52721"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:301
msgid ""
"Do you really want to quit Csuper HTML ?\n"
"You will need to restart Csuper-HTML before running it again."
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:321
msgid "Closed"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:322
msgid "Csuper-HTML is closed"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:358
msgid ""
"A update is available: you use the version %1 of Csuper whereas the version "
"%2 is available.\n"
"You can download the new version on this website: https://www.dalan.fr/"
msgstr ""

#: Sources/csuper-gtk/csu_application.cpp:365
msgid "You use the version %1 of Csuper which is the latest version."
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:139
#: Sources/csuper-gtk/main_window.cpp:172
msgid "Edit"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:140
#: UI/csuper-gtk.glade:1730
msgid "Properties"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:141
#: UI/csuper-gtk.glade:1693 UI/csuper-gtk.glade:1763
msgid "Delete"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:151
msgid "View the details of the game configuration"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:152
msgid "Delete the game configuration"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:153
msgid "Edit the game configuration"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:235
#: Sources/csuper-gtk/main_window.cpp:251 Sources/csuper-gtk/menu_file.cpp:169
#: Sources/csuper-gtk/menu_file.cpp:194
msgid "All file"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:242
#: Sources/csuper-gtk/save_dialog.cpp:59
msgid "Export file"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:243
#: Sources/csuper-gtk/game_configuration_window.cpp:292
msgid "Accept"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:244
#: Sources/csuper-gtk/game_configuration_window.cpp:293
#: Sources/csuper-gtk/import_export_game_configuration_dialog.cpp:54
#: Sources/csuper-gtk/main_window.cpp:240 Sources/csuper-gtk/menu_file.cpp:182
#: Sources/csuper-gtk/menu_file.cpp:257 Sources/csuper-gtk/open_dialog.cpp:60
#: Sources/csuper-gtk/save_dialog.cpp:63 UI/csuper-gtk.glade:440
#: UI/csuper-gtk.glade:1913
msgid "Cancel"
msgstr ""

#: Sources/csuper-gtk/game_configuration_window.cpp:291
msgid "Import file"
msgstr ""

#: Sources/csuper-gtk/game_information_view.cpp:73
msgid "The distributor is %1."
msgstr ""

#: Sources/csuper-gtk/game_information_view.cpp:75
msgid ""
"\n"
"It is his %1th turn."
msgstr ""

#: Sources/csuper-gtk/game_information_view.cpp:81
msgid "There is no distributor in this game"
msgstr ""

#: Sources/csuper-gtk/game_information_view.cpp:94
msgid "There is %1 points in the game"
msgstr ""

#: Sources/csuper-gtk/html_preferences.cpp:99
msgid ""
"The HTML will be available on all your LAN (local Area Network) at the "
"address <a href=\"http://%1:52721\">http://%1:52721</a>."
msgstr ""

#: Sources/csuper-gtk/import_export_game_configuration_dialog.cpp:55
#: UI/csuper-gtk.glade:455 UI/csuper-gtk.glade:728 UI/csuper-gtk.glade:1181
#: UI/csuper-gtk.glade:1453 UI/csuper-gtk.glade:1928 UI/csuper-gtk.glade:3017
msgid "Validate"
msgstr ""

#: Sources/csuper-gtk/import_export_game_configuration_dialog.cpp:89
#: UI/csuper-gtk.glade:1320
msgid "Import game configuration"
msgstr ""

#: Sources/csuper-gtk/import_export_game_configuration_dialog.cpp:91
#: UI/csuper-gtk.glade:1334
msgid "Export game configuration"
msgstr ""

#: Sources/csuper-gtk/import_export_game_configuration_dialog.cpp:125
msgid "Select all"
msgstr ""

#: Sources/csuper-gtk/import_export_game_configuration_dialog.cpp:131
msgid "Deselect all"
msgstr ""

#: Sources/csuper-gtk/main.cpp:230 Sources/csuper-gtk/main_window.cpp:74
#: Sources/csuper-gtk/main_window.cpp:82 Sources/csuper-gtk/main_window.cpp:88
#: Sources/csuper-gtk/main_window.cpp:318 UI/csuper-gtk.glade:63
msgid "Csuper"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:125
msgid "New"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:129
msgid "Create a new CSU file"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:136
#: Sources/csuper-gtk/main_window.cpp:239 Sources/csuper-gtk/open_dialog.cpp:63
msgid "Open"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:140
msgid "Open a existing CSU file"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:148
msgid "Recent"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:149
msgid "Open a recent CSU file"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:164 UI/csuper-gtk.glade:709
#: UI/csuper-gtk.glade:2836
msgid "Display"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:180
msgid "File"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:238 Sources/csuper-gtk/open_dialog.cpp:58
msgid "Open file"
msgstr ""

#: Sources/csuper-gtk/main_window.cpp:248 Sources/csuper-gtk/menu_file.cpp:170
#: Sources/csuper-gtk/menu_file.cpp:191
msgid "CSU file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:128
msgid "The game configuration already exist in your list."
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:138
msgid "Do you really want to delete the current file?"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:180 UI/csuper-gtk.glade:1706
msgid "Save as"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:181 Sources/csuper-gtk/save_dialog.cpp:66
msgid "Save"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:241 Sources/csuper-gtk/menu_file.cpp:265
msgid "PDF file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:242 Sources/csuper-gtk/menu_file.cpp:270
msgid "CSV file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:243 Sources/csuper-gtk/menu_file.cpp:282
msgid "Gnuplot file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:244 Sources/csuper-gtk/menu_file.cpp:289
msgid "Octave/Matlab file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:245 Sources/csuper-gtk/menu_file.cpp:294
msgid "SVG file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:246 Sources/csuper-gtk/menu_file.cpp:299
msgid "PNG file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:255 Sources/csuper-gtk/menu_file.cpp:256
#: UI/csuper-gtk.glade:1330 UI/csuper-gtk.glade:1715
msgid "Export"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:275
msgid "Markdown file"
msgstr ""

#: Sources/csuper-gtk/menu_file.cpp:380
msgid "The file was well exported."
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:63
msgid "New csu file assistant"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:77
msgid "General information"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:88
msgid "Player's names"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:96
msgid "Distributor and validation"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:125
msgid "Add a new game configuration"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:126
msgid "Use an other game configuration"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:173
#: Sources/csuper-gtk/new_file_assistant.cpp:176
msgid "Name of the new CSU file"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:182
#: Sources/csuper-gtk/new_file_assistant.cpp:185 UI/csuper-gtk.glade:1784
msgid "Folder where the new csu file will be saved"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:191
#: Sources/csuper-gtk/new_file_assistant.cpp:194 UI/csuper-gtk.glade:1822
msgid "Your game configuration"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:218
#: Sources/csuper-gtk/new_file_assistant.cpp:221
#: Sources/csuper-gtk/new_file_assistant.cpp:235
#: Sources/csuper-gtk/new_file_assistant.cpp:238
#: Sources/csuper-gtk/new_file_assistant_page_2.cpp:69
msgid "Name of the %1th player"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant.cpp:259
#: Sources/csuper-gtk/new_file_assistant.cpp:264
#: Sources/csuper-gtk/new_file_assistant_page_3.cpp:55
msgid "Select the distributor"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant_page_2.cpp:72
msgid "Type here the name of the %1th player"
msgstr ""

#: Sources/csuper-gtk/new_file_assistant_page_3.cpp:62
msgid ""
"<span font_weight=\"bold\">Please notice that the file will be automatically "
"saved during any action.</span>"
msgstr ""

#: Sources/csuper-gtk/new_game_configuration_dialog.cpp:169
#: Sources/csuper-gtk/new_game_configuration_dialog.cpp:174
#: UI/csuper-gtk.glade:1964
msgid "Name of the game configuration"
msgstr ""

#: Sources/csuper-gtk/players_names_view.cpp:89
#: Sources/csuper-gtk/ranking_view.cpp:87
#: Sources/csuper-gtk/statistics_grid.cpp:83
msgid "Name"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:125
msgid "Legend"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:137
msgid "Total"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:147
#: Sources/csuper-gtk/points_view.cpp:243
msgid "Ranking"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:165
msgid "Edit the turn"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:168
msgid "Delete the turn"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:180
msgid "Turn %1"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:202
msgid "New points"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:226
msgid "Names"
msgstr ""

#: Sources/csuper-gtk/points_view.cpp:237
msgid "Total points"
msgstr ""

#: Sources/csuper-gtk/ranking_view.cpp:55 UI/csuper-gtk.glade:175
#: UI/csuper-gtk.glade:207
msgid "No csu file loaded"
msgstr ""

#: Sources/csuper-gtk/ranking_view.cpp:86
msgid "Position"
msgstr ""

#: Sources/csuper-gtk/ranking_view.cpp:90
msgid "Diff cons"
msgstr ""

#: Sources/csuper-gtk/ranking_view.cpp:92
msgid "Diff first"
msgstr ""

#: Sources/csuper-gtk/ranking_view.cpp:94
msgid "Diff last"
msgstr ""

#: Sources/csuper-gtk/save_dialog.cpp:61
msgid "Save file"
msgstr ""

#: Sources/csuper-gtk/statistics_grid.cpp:84
msgid "Mean points"
msgstr ""

#: Sources/csuper-gtk/statistics_grid.cpp:85
msgid "Number of turn"
msgstr ""

#: Sources/csuper-gtk/statistics_grid.cpp:88
msgid "Number of turn with the best score"
msgstr ""

#: Sources/csuper-gtk/statistics_grid.cpp:89
msgid "Number of turn with the worst score"
msgstr ""

#: Sources/csuper-gtk/statistics_grid.cpp:90
msgid "Number of turn first"
msgstr ""

#: Sources/csuper-gtk/statistics_grid.cpp:91
msgid "Number of turn last"
msgstr ""

#: UI/csuper-gtk.glade:98
msgid "<span size=\"x-large\">Ranking</span>"
msgstr ""

#: UI/csuper-gtk.glade:161
msgid "<span size=\"x-large\">Game informations</span>"
msgstr ""

#: UI/csuper-gtk.glade:187 UI/csuper-gtk.glade:424
msgid "Change distributor"
msgstr ""

#: UI/csuper-gtk.glade:192
msgid "Change the distributor"
msgstr ""

#: UI/csuper-gtk.glade:238
msgid "End of turn"
msgstr ""

#: UI/csuper-gtk.glade:247
msgid "Save the new points"
msgstr ""

#: UI/csuper-gtk.glade:369
msgid "About Csuper"
msgstr ""

#: UI/csuper-gtk.glade:379
msgid "Copyright © 2014-2018 Rémi BERTHO <remi.bertho@dalan.fr>"
msgstr ""

#: UI/csuper-gtk.glade:380
msgid "Universal points counter allowing a dispense with reflection"
msgstr ""

#: UI/csuper-gtk.glade:382
msgid "Visit the website of Csuper"
msgstr ""

#: UI/csuper-gtk.glade:385
msgid ""
"Rémi BERTHO\n"
"Nicolas DANKAR"
msgstr ""

#: UI/csuper-gtk.glade:444
msgid "Cancel and return to the main window"
msgstr ""

#: UI/csuper-gtk.glade:459 UI/csuper-gtk.glade:732 UI/csuper-gtk.glade:1185
#: UI/csuper-gtk.glade:1457
msgid "Validate and return to the main window"
msgstr ""

#: UI/csuper-gtk.glade:520
msgid "<span size=\"x-large\">Please choose the new distributor</span>"
msgstr ""

#: UI/csuper-gtk.glade:538
msgid "Set here the number of turn already passed as a distributor"
msgstr ""

#: UI/csuper-gtk.glade:586
msgid "Delete turn"
msgstr ""

#: UI/csuper-gtk.glade:678
msgid ""
"<span size=\"large\">Please choose the player(s) that \n"
"you want to delete the turn</span>"
msgstr ""

#: UI/csuper-gtk.glade:768
msgid "<span font_weight=\"bold\" size=\"xx-large\">Statistics</span>"
msgstr ""

#: UI/csuper-gtk.glade:806
msgid "Statistics"
msgstr ""

#: UI/csuper-gtk.glade:912
msgid "Total points chart"
msgstr ""

#: UI/csuper-gtk.glade:1019
msgid "Points chart"
msgstr ""

#: UI/csuper-gtk.glade:1044
msgid "Edit turn"
msgstr ""

#: UI/csuper-gtk.glade:1134
msgid "<span size=\"large\">Please choose the new points</span>"
msgstr ""

#: UI/csuper-gtk.glade:1162
msgid "File's properties"
msgstr ""

#: UI/csuper-gtk.glade:1301
msgid "Add"
msgstr ""

#: UI/csuper-gtk.glade:1305
msgid "Add a game configuration"
msgstr ""

#: UI/csuper-gtk.glade:1316
msgid "Import"
msgstr ""

#: UI/csuper-gtk.glade:1344 UI/csuper-gtk.glade:3032
msgid "Close"
msgstr ""

#: UI/csuper-gtk.glade:1348 UI/csuper-gtk.glade:3036
msgid "Close the game preferences window"
msgstr ""

#: UI/csuper-gtk.glade:1371
msgid ""
"<span font_weight=\"bold\" size=\"large\">List of all your game "
"configurations</span>"
msgstr ""

#: UI/csuper-gtk.glade:1385
msgid "<span size=\"large\">You have no game configuration selected.</span>"
msgstr ""

#: UI/csuper-gtk.glade:1402
msgid ""
"<span font_weight=\"bold\" size=\"large\">The selected game configurations</"
"span>"
msgstr ""

#: UI/csuper-gtk.glade:1433
msgid "Game over"
msgstr ""

#: UI/csuper-gtk.glade:1485
msgid "The game is over, congratulation to the winner !"
msgstr ""

#: UI/csuper-gtk.glade:1511
msgid "Display the podium"
msgstr ""

#: UI/csuper-gtk.glade:1512
msgid "Display podium"
msgstr ""

#: UI/csuper-gtk.glade:1520
msgid "Display the statistics"
msgstr ""

#: UI/csuper-gtk.glade:1521
msgid "Display statistics"
msgstr ""

#: UI/csuper-gtk.glade:1529
msgid "Display the chart of the total points"
msgstr ""

#: UI/csuper-gtk.glade:1530
msgid "Display the total points chart"
msgstr ""

#: UI/csuper-gtk.glade:1538
msgid "Display the chart of the points"
msgstr ""

#: UI/csuper-gtk.glade:1539
msgid "Display the points chart"
msgstr ""

#: UI/csuper-gtk.glade:1553
msgid ""
"Display the points differences between two consecutive player in the ranking"
msgstr ""

#: UI/csuper-gtk.glade:1554
msgid "Display the difference between two player consecutive"
msgstr ""

#: UI/csuper-gtk.glade:1562
msgid ""
"Display the points differences between a player and the first one in the "
"ranking"
msgstr ""

#: UI/csuper-gtk.glade:1563
msgid "Display the difference with the first"
msgstr ""

#: UI/csuper-gtk.glade:1571
msgid ""
"Display the points differences between a player and the last one in the "
"ranking"
msgstr ""

#: UI/csuper-gtk.glade:1572
msgid "Display the difference with the last"
msgstr ""

#: UI/csuper-gtk.glade:1586
msgid "Display the total points in each turn in the points grid"
msgstr ""

#: UI/csuper-gtk.glade:1587
msgid "Display the total points in each turn"
msgstr ""

#: UI/csuper-gtk.glade:1595
msgid "Display the ranking in each turn in the points grid"
msgstr ""

#: UI/csuper-gtk.glade:1596
msgid "Display the ranking in each turn"
msgstr ""

#: UI/csuper-gtk.glade:1604
msgid "Display the button which permit to edit and suppress a turn"
msgstr ""

#: UI/csuper-gtk.glade:1605
msgid "Display the edit and suppr button"
msgstr ""

#: UI/csuper-gtk.glade:1619
msgid "Display the ranking in the left side of the main window"
msgstr ""

#: UI/csuper-gtk.glade:1620
msgid "Display the ranking"
msgstr ""

#: UI/csuper-gtk.glade:1628
msgid "Display the game infonrmations in the left side of the main window"
msgstr ""

#: UI/csuper-gtk.glade:1629
msgid "Display the game informations"
msgstr ""

#: UI/csuper-gtk.glade:1641
msgid "Undo the last \"End of turn\""
msgstr ""

#: UI/csuper-gtk.glade:1642
msgid "Undo"
msgstr ""

#: UI/csuper-gtk.glade:1650
msgid "Redo the last \"End of turn\""
msgstr ""

#: UI/csuper-gtk.glade:1651
msgid "Redo"
msgstr ""

#: UI/csuper-gtk.glade:1665
msgid "Cut the current text"
msgstr ""

#: UI/csuper-gtk.glade:1666
msgid "Cut"
msgstr ""

#: UI/csuper-gtk.glade:1674
msgid "Copy the current text"
msgstr ""

#: UI/csuper-gtk.glade:1675
msgid "Copy"
msgstr ""

#: UI/csuper-gtk.glade:1683 UI/csuper-gtk.glade:1684
msgid "Paste"
msgstr ""

#: UI/csuper-gtk.glade:1692
msgid "Delete the current text"
msgstr ""

#: UI/csuper-gtk.glade:1705
msgid "Save as a CSU file"
msgstr ""

#: UI/csuper-gtk.glade:1714
msgid "Export the current game"
msgstr ""

#: UI/csuper-gtk.glade:1729
msgid "Properties of the file"
msgstr ""

#: UI/csuper-gtk.glade:1738
msgid "Edit the game configuration of the current file"
msgstr ""

#: UI/csuper-gtk.glade:1739
msgid "Edit game configuration"
msgstr ""

#: UI/csuper-gtk.glade:1747
msgid "Import the game configuration of the current game"
msgstr ""

#: UI/csuper-gtk.glade:1748
msgid "Import the game configuration"
msgstr ""

#: UI/csuper-gtk.glade:1762
msgid "Delete the current csu file"
msgstr ""

#: UI/csuper-gtk.glade:1795
msgid "Number of players on the game"
msgstr ""

#: UI/csuper-gtk.glade:1806
msgid "1"
msgstr ""

#: UI/csuper-gtk.glade:1845
msgid "Name of the new csu file"
msgstr ""

#: UI/csuper-gtk.glade:1857
msgid "Type here the name of the new csu file"
msgstr ""

#: UI/csuper-gtk.glade:1917
msgid "Cancel and go back to the game configuration preferences"
msgstr ""

#: UI/csuper-gtk.glade:1932
msgid "Validate the new game configuration"
msgstr ""

#: UI/csuper-gtk.glade:1977
msgid "Use a maximum or minimum score or neither"
msgstr ""

#: UI/csuper-gtk.glade:1990
msgid "Initial score"
msgstr ""

#: UI/csuper-gtk.glade:2003
msgid "The winner is the player with the highest score"
msgstr ""

#: UI/csuper-gtk.glade:2016
msgid "The points will be attributed in turn-based"
msgstr ""

#: UI/csuper-gtk.glade:2029
msgid "Use of a distributor"
msgstr ""

#: UI/csuper-gtk.glade:2042
msgid "Maximal or minimal score which a player can reach in the game"
msgstr ""

#: UI/csuper-gtk.glade:2057
msgid "Type here the name of the game configuration"
msgstr ""

#: UI/csuper-gtk.glade:2097
msgid "Maximum"
msgstr ""

#: UI/csuper-gtk.glade:2098
msgid "Minimum"
msgstr ""

#: UI/csuper-gtk.glade:2099
msgid "Neither"
msgstr ""

#: UI/csuper-gtk.glade:2111 UI/csuper-gtk.glade:2128 UI/csuper-gtk.glade:2171
msgid "0,000"
msgstr ""

#: UI/csuper-gtk.glade:2159
msgid "Number of decimals displayed"
msgstr ""

#: UI/csuper-gtk.glade:2187 UI/csuper-gtk.glade:2390 UI/csuper-gtk.glade:2424
#: UI/csuper-gtk.glade:2551 UI/csuper-gtk.glade:2570
msgid "0"
msgstr ""

#: UI/csuper-gtk.glade:2204
msgid "Number of turn before the distributor change"
msgstr ""

#: UI/csuper-gtk.glade:2305
msgid "<span font_weight=\"bold\" size=\"large\">PDF exportation</span>"
msgstr ""

#: UI/csuper-gtk.glade:2318
msgid ""
"The UTF-8 character set permit to display all character but can have problem "
"with some fonts."
msgstr ""

#: UI/csuper-gtk.glade:2320
msgid "Use the UTF-8 character set"
msgstr ""

#: UI/csuper-gtk.glade:2346
msgid "Size of the pdf document"
msgstr ""

#: UI/csuper-gtk.glade:2361
msgid "A5"
msgstr ""

#: UI/csuper-gtk.glade:2362
msgid "A4"
msgstr ""

#: UI/csuper-gtk.glade:2363
msgid "A3"
msgstr ""

#: UI/csuper-gtk.glade:2376
msgid "Margin of the pdf document"
msgstr ""

#: UI/csuper-gtk.glade:2410
msgid "Font size of the pdf document"
msgstr ""

#: UI/csuper-gtk.glade:2444
msgid "Show the total points in each turn"
msgstr ""

#: UI/csuper-gtk.glade:2471
msgid "Show the ranking in each turn"
msgstr ""

#: UI/csuper-gtk.glade:2498
msgid "<span font_weight=\"bold\" size=\"large\">Chart exportation</span>"
msgstr ""

#: UI/csuper-gtk.glade:2512
msgid "Width of the chart"
msgstr ""

#: UI/csuper-gtk.glade:2524
msgid "Height of the chart"
msgstr ""

#: UI/csuper-gtk.glade:2535
msgid ""
"Show the total points in the chart. If it is turn off, it will show the "
"points."
msgstr ""

#: UI/csuper-gtk.glade:2537
msgid "Show the total points"
msgstr ""

#: UI/csuper-gtk.glade:2599
msgid ""
"If it's not set, the size of the chart on the pdf will be those of the chart "
"exportation."
msgstr ""

#: UI/csuper-gtk.glade:2603
msgid "Use the pdf size for the chart"
msgstr ""

#: UI/csuper-gtk.glade:2628
msgid "Direction of the pdf document"
msgstr ""

#: UI/csuper-gtk.glade:2643
msgid "Portrait"
msgstr ""

#: UI/csuper-gtk.glade:2644
msgid "Landscape"
msgstr ""

#: UI/csuper-gtk.glade:2669
msgid ""
"Please type the font name which will be use in the PDF\n"
"Be careful, if the font does'nt exist the PDF file will be unreadable."
msgstr ""

#: UI/csuper-gtk.glade:2672
msgid "Font name"
msgstr ""

#: UI/csuper-gtk.glade:2684
msgid ""
"If the font is embedded in the PDF file, any user will can read the file "
"even if he has not the font installed. But the file will be bigger."
msgstr ""

#: UI/csuper-gtk.glade:2686
msgid "Embedded font"
msgstr ""

#: UI/csuper-gtk.glade:2720
msgid "Exportation"
msgstr ""

#: UI/csuper-gtk.glade:2760
msgid "<span font_weight=\"bold\" size=\"large\">Title bar</span>"
msgstr ""

#: UI/csuper-gtk.glade:2773
msgid ""
"Disable the main window decoration will be useful when using GNOME desktop "
"manager."
msgstr ""

#: UI/csuper-gtk.glade:2775
msgid "Disable the main window decoration "
msgstr ""

#: UI/csuper-gtk.glade:2787
msgid ""
"Add the title in the title bar will be useful when using GNOME desktop "
"manager."
msgstr ""

#: UI/csuper-gtk.glade:2789
msgid "Add title"
msgstr ""

#: UI/csuper-gtk.glade:2875
msgid "<span font_weight=\"bold\" size=\"large\">Informations</span>"
msgstr ""

#: UI/csuper-gtk.glade:2893
msgid "<span font_weight=\"bold\" size=\"large\">Start</span>"
msgstr ""

#: UI/csuper-gtk.glade:2906
msgid ""
"It will allow you to connect to csuper with any device on your local network "
"(for example tablet) when your computer is up.\n"
"Please DO NOT USE it on public network."
msgstr ""

#: UI/csuper-gtk.glade:2909
msgid "Start Csuper HTML with the computer for the current user"
msgstr ""

#: UI/csuper-gtk.glade:2949
msgid ""
"If you can't acces to the HTML version from another devic, it may be your "
"firewall that block it.\n"
"You can tell it to unblock the Csuper port (52721)."
msgstr ""

#: UI/csuper-gtk.glade:2978
msgid "HTML version"
msgstr ""

#: UI/csuper-gtk.glade:3002
msgid "Apply"
msgstr ""

#: UI/csuper-gtk.glade:3006 UI/csuper-gtk.glade:3021
msgid "Apply the new preferences for the new toolbar button displayed"
msgstr ""
